# README #

This README would normally document whatever steps are necessary to get the TourG application up and running.

### Introduction ###

* #### TourG ####
* TourG is an android mobile application for making your trips easier.
* It helps to plan the trips in an efficient way by sorting the places with its smartest sorting algorithm.
* Supported platform: Android.
* IDE to build and run the project: Android Studio.

### Deployment setups and other configurations ###

* Get the source code from the repository by downloading the repository or clone the repository.
* If you don't have Android Studio IDE, download and install it and finish related configurations.
* You can use your android mobile to install the application or can use emulator.
* If you are going to use emulator setup an emulator in Android Studio.
* To use android mobile, enable developer options and turn on USB debugging and then connect with your local machine.
* Make sure that your local machine has connected to the internet and, open the project using Android Studio.
* After opening the project select the target device, which can be your android mobile phone or emulator.
* Then run the project. Android Studio will build and install the application in the targeted device. 
* After the installation, allow the location permission to the application in the settings.
* Make sure that your mobile device has connected to internet and location is turned on in the device.
* Now open TourG app and get great experience with TourG. 

### Developers contacts ###

* [Ramakrishnan Ravichandran](https://www.linkedin.com/in/ramakrishnan-ravichandran-5185a6134)
* [Sangeevan Siventhirarajah](https://www.linkedin.com/in/sangeevan)
* [Sujithra Gunaratnasamy](https://www.linkedin.com/in/sujithra-gunaratnasamy-441663149)
* [Sharmila Pararajasingam](https://www.linkedin.com/in/sharmila-pararajasingam-620074148/)