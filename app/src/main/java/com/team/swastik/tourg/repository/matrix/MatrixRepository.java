package com.team.swastik.tourg.repository.matrix;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import io.reactivex.Observable;

public interface MatrixRepository {
    Observable<JsonObject> getMatrix(JSONObject postparams);
}
