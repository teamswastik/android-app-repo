package com.team.swastik.tourg.ui.fragment.tripBasicDetails;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.team.swastik.tourg.base.BaseViewModel;

import javax.inject.Inject;

public class TripBasicDetailsViewModel extends BaseViewModel {

    private MutableLiveData<String> mText;

    @Inject
    public TripBasicDetailsViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is Trip Basic Details fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}