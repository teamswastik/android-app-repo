package com.team.swastik.tourg.repository.matrix;

import com.google.gson.JsonObject;
import com.team.swastik.tourg.di.ORSClient;
import com.team.swastik.tourg.retrofit.interfaces.MatrixServiceORS;

import org.json.JSONArray;
import org.json.JSONObject;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class MatrixRepositoryImpl implements MatrixRepository {

    private Retrofit mRetrofitClient;

    @Inject
    public MatrixRepositoryImpl(@ORSClient Retrofit mRetrofitClient){
        this.mRetrofitClient = mRetrofitClient;
    }

    @Override
    public Observable<JsonObject> getMatrix(JSONObject postparams) {
        MatrixServiceORS matrixService = mRetrofitClient.create(MatrixServiceORS.class);
        Observable<JsonObject> matrix = matrixService.getMatrix(postparams);
        return matrix.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }
}
