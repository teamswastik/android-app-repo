package com.team.swastik.tourg.ui.activity;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.team.swastik.tourg.base.BaseViewModel;
import com.team.swastik.tourg.data.database.AppDatabase;
import com.team.swastik.tourg.data.database.entity.TripWithPlaces;
import com.team.swastik.tourg.data.pref.WishListPreference;
import com.team.swastik.tourg.model.Place;
import com.team.swastik.tourg.model.Trip;
import com.team.swastik.tourg.model.enums.District;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class StartTripViewModel extends BaseViewModel {

    private AppDatabase appDatabase;
    private MutableLiveData<Place> currentPlaceLiveData = new MutableLiveData<>();
    private MutableLiveData<Place> nextPlaceLiveData = new MutableLiveData<>();
    private MutableLiveData<TripWithPlaces> currentTripWithPlaces = new MutableLiveData<>();
    private MutableLiveData<Boolean> noNextPlace = new MutableLiveData<>();
    private List<Place> currentSortedList ;
    private WishListPreference wishListPreference;
    private double Lat;
    private double Lng;

    @Inject
    public StartTripViewModel(AppDatabase appDatabase, WishListPreference wishListPreference) {
        this.appDatabase = appDatabase;
        this.wishListPreference = wishListPreference;
        Lat = wishListPreference.getLat();
        Lng = wishListPreference.getLng();
    }

    public void getTripWithId(Long id){
        getCompositeDisposable().add(
                appDatabase.tripDao().getTripWithPlaceWithId(id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                (tripWithPlaces) ->{
                                    Timber.e("Trip With Place");
                                    currentTripWithPlaces.setValue(tripWithPlaces);

                                },
                                throwable -> {
                                    Timber.e("Error in Getting Trip");
                                    throwable.printStackTrace();
                                }
                        )
        );

    }

    public Long getCurrentTripId(){
        return wishListPreference.getCurrentTripId();
    }

    public void setCurrentSortedList(List<Place> places){
        currentSortedList = places;
        currentPlaceLiveData.setValue(currentSortedList.get(0));
        if (currentSortedList.size()>1){
            nextPlaceLiveData.setValue(currentSortedList.get(1));
        }else noNextPlace.setValue(true);
    }


    public LiveData<List<Place>> getAccommodationLiveData(District district ,boolean touristPlace){
        return appDatabase.placeDao().getPlacesByDistrictAndTouristPlace(district,touristPlace);
    }

    public LiveData<Place> getCurrentPlaceLiveData() {
        return currentPlaceLiveData;
    }

    public LiveData<Place> getNextPlaceLiveData() {
        return nextPlaceLiveData;
    }

    public LiveData<Boolean> getNoNextPlace() {
        return noNextPlace;
    }

    public double getLat() {
        return Lat;
    }

    public double getLng() {
        return Lng;
    }

    public void endTrip(){
        wishListPreference.setTripOnProcess(false);
    }
    public void setCurrentTripWithPlaces(TripWithPlaces currentTripWithPlaces) {
        this.currentTripWithPlaces.setValue(currentTripWithPlaces);
    }

    public LiveData<TripWithPlaces> getCurrentTripWithPlaces() {
        return currentTripWithPlaces;
    }

    public Completable updateTrip(Trip trip){
        return appDatabase.tripDao().update(trip);
    }

}
