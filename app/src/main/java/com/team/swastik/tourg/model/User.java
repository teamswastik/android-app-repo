package com.team.swastik.tourg.model;


import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class User {

    @PrimaryKey
    private int id;
    private String name;
    private String username;
    private String email;
    private String phoneNo;
    private String firebaseUid;
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirebaseUid() {
        return firebaseUid;
    }

    public void setFirebaseUid(String firebaseUid) {
        this.firebaseUid = firebaseUid;
    }

    public String getPhoneno() {
        return phoneNo;
    }

    public void setPhoneno(String phoneno) {
        this.phoneNo = phoneno;
    }

    @Override
    public String  toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", phoneno='" + phoneNo + '\'' +
                ", firebaseUid='" + firebaseUid + '\'' +
                '}';
    }
}
