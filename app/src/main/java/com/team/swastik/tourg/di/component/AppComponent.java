package com.team.swastik.tourg.di.component;

import com.team.swastik.tourg.TourgApp;
import com.team.swastik.tourg.di.TourgAppScope;
import com.team.swastik.tourg.di.module.ActivityModule;
import com.team.swastik.tourg.di.module.ApplicationModule;
import com.team.swastik.tourg.di.module.FragmentModule;
import com.team.swastik.tourg.di.module.NetworkModule;
import com.team.swastik.tourg.di.module.RetrofitModule;
import com.team.swastik.tourg.di.module.ViewModelModule;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@TourgAppScope
@Component(modules = {
        AndroidInjectionModule.class,
        ActivityModule.class,
        FragmentModule.class,
        ViewModelModule.class,
        RetrofitModule.class,
        ApplicationModule.class,
        NetworkModule.class
})
public interface AppComponent {

    void inject(TourgApp app);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(TourgApp app);

        AppComponent build();
    }
}
