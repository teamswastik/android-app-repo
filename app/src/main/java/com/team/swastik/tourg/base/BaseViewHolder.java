package com.team.swastik.tourg.base;

import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

public abstract class BaseViewHolder<T extends ViewDataBinding> extends RecyclerView.ViewHolder {
    private T mBinding;
    public BaseViewHolder(T binding) {
        super(binding.getRoot());
        this.mBinding= binding;
    }

    public T getBinding() {
        return mBinding;
    }
}
