package com.team.swastik.tourg.di.module;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.room.Room;

import com.team.swastik.tourg.TourgApp;
import com.team.swastik.tourg.data.database.AppDatabase;
import com.team.swastik.tourg.di.DbInfo;
import com.team.swastik.tourg.di.PrefInfo;
import com.team.swastik.tourg.di.TourgAppScope;
import com.team.swastik.tourg.utils.AppConstants;
import com.team.swastik.tourg.utils.DateTimeUtils;

import java.text.SimpleDateFormat;
import java.util.Locale;

import dagger.Module;
import dagger.Provides;

@Module(includes = {
        PreferenceModule.class,
        RepositoryModule.class
})
public abstract class ApplicationModule {

    @Provides
    static Context provideContext(TourgApp app) {
        return app.getApplicationContext();
    }


    @Provides
    static SharedPreferences provideTourGSharedPref(Context context, @PrefInfo String prefFileName) {
        return context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
    }

    @Provides
    static DateTimeUtils provideDateTimeUtils(SimpleDateFormat dateFormatter, Context context){
        return new DateTimeUtils(dateFormatter,context);
    };

    @Provides
    @PrefInfo
    static String providePrefName() {
        return AppConstants.APP_PREF_NAME;
    }

    @Provides
    @DbInfo
    static String provideDbName() {
        return AppConstants.APP_DB_NAME;
    }

    @Provides
    static SimpleDateFormat provideSimpleDateFormatter(){
        return new SimpleDateFormat(AppConstants.DATE_FORMAT, Locale.ENGLISH);
    }

    @Provides
    @TourgAppScope
    static AppDatabase provideAppDb(Context context, @DbInfo String dbName) {
        return Room.databaseBuilder(
                context,
                AppDatabase.class,
                dbName
        ).build();
    }


}
