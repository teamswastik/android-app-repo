package com.team.swastik.tourg.model;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = "wishlist" ,
        foreignKeys = {
                @ForeignKey(
                        entity = Place.class,
                        parentColumns = "id",
                        childColumns = "touristplace_id",
                        onDelete = CASCADE
                )
        })
public class WishlistPlace {
    @PrimaryKey(autoGenerate = false)
    private String id;
    @ColumnInfo(name = "touristplace_id")
    private String touristplaceId;


    public WishlistPlace() {
         }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
         @Override
    public String toString() {
        return "WishlistPlace{" +
                "id='" + id +'\'' +
                '}';
         }

}
