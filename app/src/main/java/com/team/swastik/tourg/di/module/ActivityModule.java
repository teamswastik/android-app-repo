package com.team.swastik.tourg.di.module;

import com.team.swastik.tourg.ui.activity.HomeActivity;
import com.team.swastik.tourg.ui.activity.StartTrip;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract HomeActivity bindHomeActivity();

    @ContributesAndroidInjector
    abstract StartTrip bindStartTrip();
}
