package com.team.swastik.tourg.repository;

import com.team.swastik.tourg.repository.place.PlaceRepository;
import com.team.swastik.tourg.repository.user.UserRepository;

public interface AppRepository extends UserRepository, PlaceRepository {
}
