package com.team.swastik.tourg.ui.fragment.home;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.team.swastik.tourg.R;
import com.team.swastik.tourg.base.BaseFragment;
import com.team.swastik.tourg.databinding.FragmentHomeBinding;
import com.team.swastik.tourg.databinding.ItemHomePlaceSliderBinding;
import com.team.swastik.tourg.di.ViewModelFactory;
import com.team.swastik.tourg.model.Place;
import com.team.swastik.tourg.model.enums.District;
import com.team.swastik.tourg.service.SortTripService;
import com.team.swastik.tourg.ui.activity.MainViewModel;
import com.team.swastik.tourg.utils.AppConstants;
import com.team.swastik.tourg.utils.DateTimeUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;
import timber.log.Timber;

public class HomeFragment extends BaseFragment<FragmentHomeBinding> implements IMainAdapterListener {

    @Inject
    ViewModelFactory viewModelFactory;
    @Inject
    DateTimeUtils dateTimeUtils;
    private MainViewModel mMainViewModel;
    private HomeViewModel mHomeViewModel;

    PlaceAdapter<ItemHomePlaceSliderBinding> placeAdapter;
    private MainAdapter mMainAdapter;

    private BroadcastReceiver bReceiver = new BroadcastReceiver(){

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(AppConstants.INTENT_FILTER_SORT)){
                Timber.e("Received Brodcast intent From : "+intent.getAction());
                Timber.e(intent.getExtras().getBoolean("success")+"");
            }
        }

    };

    @Override
    public int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        initDaggerAndViewModel();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mMainViewModel.getFavouritesPlacesFromPref();
        mHomeViewModel.getPlacesFromFirebase();
        setListeners(view);
        setObservers();
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getBaseActivity()).registerReceiver(bReceiver, new IntentFilter(AppConstants.INTENT_FILTER_SORT));
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getBaseActivity()).unregisterReceiver(bReceiver);
    }

    private void setListeners(View view) {
        final NavController navController = Navigation.findNavController(view);
        getBinding().fab.setOnClickListener(v -> {

            navController.navigate(R.id.action_navigation_home_to_selectPlanFragment);
        });

        getBinding().imgViewUser.setOnClickListener(v -> {

        });

    }

    private void setObservers() {
        /*mHomeViewModel.getLiveDataPlace().observe(this.getViewLifecycleOwner(), places -> {
            if (places != null) {
              //  initRecyclerView(places);
            }
        });
        mHomeViewModel.getHomeModelLiveData().observe(this.getViewLifecycleOwner(), this::initRecyclerView);*/

        mMainViewModel.getCurrentLocation().observe(this.getViewLifecycleOwner(), location -> {
            if (location!=null){
                getBinding().txtViewLocation.setText(location.getLatitude()+" , "+location.getLongitude());

               /* if (mHomeViewModel.getTouristPlaceLiveData().getValue()!=null){
                    Intent intent = new Intent(getBaseActivity(),SortTripService.class);
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList(
                            AppConstants.SORT_SERVICE_KEY_PLACES,
                            new ArrayList<>(getPlacesByDistrict(mHomeViewModel.getTouristPlaceLiveData().getValue(),District.COLOMBO))
                    );
                    bundle.putDouble(AppConstants.SORT_SERVICE_KEY_LAT,location.getLatitude());
                    bundle.putDouble(AppConstants.SORT_SERVICE_KEY_LNG,location.getLongitude());
                    intent.putExtras(bundle);
                    getBaseActivity().startService(intent);
                }*/


            }
        });

        mHomeViewModel.getTouristPlaceLiveData().observe(this.getViewLifecycleOwner(), places -> {
            if (places!=null && places.size()>0){
                initRecyclerView(getHomeModels(places));
            }

        });

        mMainViewModel.getWishListChangedMutableLiveData().observe(this.getViewLifecycleOwner(), wishListChanged -> {
            if (wishListChanged!=null){
                Timber.e("wishlist changed : " + wishListChanged.getAction());
                onWishListChanged(wishListChanged);

            }

            /*List<Place> dataSet = placeAdapter.getDataSet();
            for (int i = 0; i < dataSet.size(); i++) {
                if (dataSet.get(i).getId().equals(wishListChanged.getChangedId())) {
                    if (wishListChanged.getAction().equals(WishListChanged.Action.ADDED)) {
                        dataSet.get(i).setOnWishList(true);
                    } else {
                        dataSet.get(i).setOnWishList(false);
                    }
                    placeAdapter.notifyItemChanged(i);
                }
            }*/
        });

    }

    private List<HomeModel>  getHomeModels(List<Place> places){
        List<HomeModel> homeModels = new ArrayList<>();
        homeModels.add(
                new HomeModel(
                        "Top Rated",
                        getPlacesByRating(places,3.5),
                        LayoutType.LIST_HORIZONTAL
                )
        );

        homeModels.add(
                new HomeModel(
                        "Recommended",
                        getPlacesByDistrict(places,District.COLOMBO),
                        LayoutType.LIST_HORIZONTAL
                )
        );

        return homeModels;
    }

    private List<Place> getPlacesByDistrict(List<Place> places,District district){
        List<Place> placesByDistrict = new ArrayList<>();
        for (Place place : places){
            if (place.getAddress().getDistrict().equals(district) &&  place.isTouristPlace()){
                placesByDistrict.add(place);
            }
        }

        return placesByDistrict;
    }

    private List<Place> getPlacesByRating(List<Place> places,double rating){
        List<Place> placesByDistrict = new ArrayList<>();
        for (Place place : places){
            if (place.getRating()>rating && place.isTouristPlace()){
                placesByDistrict.add(place);
            }
        }

        return placesByDistrict;
    }

    private void onWishListChanged(WishListChanged wishListChanged){
        if (wishListChanged.getMainAdapterPosition()!=null && wishListChanged.getPlaceAdapterPosition()!=null){
            if (wishListChanged.getMainAdapterPosition()==-1 || wishListChanged.getPlaceAdapterPosition()==-1)
                return;
            mMainAdapter.notifyAdapterItemChanged(
                    wishListChanged.getChangedId(),
                    wishListChanged.getMainAdapterPosition(),
                    wishListChanged.getPlaceAdapterPosition(),
                    mMainViewModel.getFavouritePlacesLiveData().getValue()
            );
        }
    }

    @Override
    public void onDestroyView() {
        mMainViewModel.setWishListChangedMutableLiveData(null);
        super.onDestroyView();

    }

    private void checkAndChangeWishListChangeInMainAdapter(){

    }

    private void initRecyclerView(List<HomeModel> homeModels) {
        RecyclerView recyclerView = getBinding().recyclerViewMain;
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity(), LinearLayoutManager.VERTICAL, false));
        mMainAdapter = new MainAdapter(getBaseActivity(),homeModels, this);
        mMainAdapter.setWishLists(mMainViewModel.getFavouritePlacesLiveData().getValue());
        recyclerView.setAdapter(mMainAdapter);
    }

    private void initDaggerAndViewModel() {
        AndroidSupportInjection.inject(this);
        // ViewModelProvider viewModelProvider = new ViewModelProvider(this, viewModelFactory);
        mHomeViewModel = ViewModelProviders.of(this, viewModelFactory).get(HomeViewModel.class);
        mMainViewModel = ViewModelProviders.of(getBaseActivity(), viewModelFactory).get(MainViewModel.class);
    }


    @Override
    public void onPlaceLikeClicked(String placeId, boolean currentState, int placeAdapterPosition, int mainAdapterPosition) {
        if (currentState) {
            mMainViewModel.removeFromWishList(placeId,placeAdapterPosition,mainAdapterPosition);
        } else {
            mMainViewModel.addToWishList(placeId,placeAdapterPosition,mainAdapterPosition);
        }

    }

    @Override
    public void onPlaceClicked(String placeId) {
        HomeFragmentDirections.ActionNavigationHomeToPlaceFragment action = HomeFragmentDirections.actionNavigationHomeToPlaceFragment(placeId);
        Navigation.findNavController(getBinding().getRoot()).navigate(action);
    }

}