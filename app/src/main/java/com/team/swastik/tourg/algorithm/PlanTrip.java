package com.team.swastik.tourg.algorithm;

import com.team.swastik.tourg.model.Marks;
import com.team.swastik.tourg.model.Place;
import java.util.ArrayList;
import java.util.List;

public class PlanTrip {

     int distance =25;
     int closingTime = 25;
     int openingTime = 20;
     int onWishlist = 15;
     int avgViewTime = 10;
     int rating = 5;

     List<Place> tripPlan = new ArrayList<Place>();

     List<Marks> distanceMarks = new ArrayList<Marks>();
     List<Marks> closingTimeMarks = new ArrayList<Marks>();
     List<Marks> openingTimeMarks = new ArrayList<Marks>();
     List<Marks> onWishlistMarks = new ArrayList<Marks>();
     List<Marks> avgViewTimeMarks = new ArrayList<Marks>();
     List<Marks> ratingMarks = new ArrayList<Marks>();
     List<Marks> finalMarks = new ArrayList<Marks>();

    public  List<Place> makePlan(List<Place> selectedPlaces, double currentLng, double currentLat, double[][] distanceMatrix){

        int count = selectedPlaces.size();

        calculateDistanceMarks(selectedPlaces,currentLng,currentLat,distanceMatrix);
        calculateClosingTimeMarks(selectedPlaces);
        calculateOpeningTimeMarks(selectedPlaces);
        calculateOnWishlistMarks(selectedPlaces);
        calculateAvgViewTimeMarks(selectedPlaces);
        calculateRatingMarks(selectedPlaces);

        for(int i=0; i<count; i++ ){
            String selectedId = selectedPlaces.get(i).getId();
            double totalMarks = 0;
            for(int j=0; j<count; j++){
                if((distanceMarks.get(j).getPlaceId()).equals(selectedId)){
                    totalMarks = totalMarks+distanceMarks.get(j).getMarks();
                }
                if((closingTimeMarks.get(j).getPlaceId()).equals(selectedId)){
                    totalMarks = totalMarks+closingTimeMarks.get(j).getMarks();
                }
                if((openingTimeMarks.get(j).getPlaceId()).equals(selectedId)){
                    totalMarks = totalMarks+openingTimeMarks.get(j).getMarks();
                }
                if((onWishlistMarks.get(j).getPlaceId()).equals(selectedId)){
                    totalMarks = totalMarks+onWishlistMarks.get(j).getMarks();
                }
                if((avgViewTimeMarks.get(j).getPlaceId()).equals(selectedId)){
                    totalMarks = totalMarks+avgViewTimeMarks.get(j).getMarks();
                }
                if((ratingMarks.get(j).getPlaceId()).equals(selectedId)){
                    totalMarks = totalMarks+ratingMarks.get(j).getMarks();
                }
            }

            double results = totalMarks / (distance+closingTime+openingTime+onWishlist+avgViewTime+rating);

            Marks calculatedMarks = new Marks(selectedId, results);

            finalMarks.add(i, calculatedMarks);

        }

        for(int i=0; i<count; i++){
            for(int j=i+1; j<count; j++){
                if(finalMarks.get(i).getMarks()<finalMarks.get(j).getMarks()){
                    Marks temp = finalMarks.get(i);
                    finalMarks.set(i, finalMarks.get(j));
                    finalMarks.set(j,temp);
                }
            }
        }

        for(int i=0; i<count; i++){
            String sortedId = finalMarks.get(i).getPlaceId();
            for(int j=0; j<count; j++){
                if((selectedPlaces.get(j).getId()).equals(sortedId)){
                    tripPlan.add(i, selectedPlaces.get(j));
                }
            }
        }

        return tripPlan;
    }

    public  void calculateDistanceMarks(List<Place> selectedPlaces, double currentLng, double currentLat, double[][] distanceMatrix){

        List<Place> places = new ArrayList<Place>(selectedPlaces);
        List<Place> placesList = new ArrayList<Place>();
        int count = selectedPlaces.size();

        placesList = distanceSort(places ,currentLng, currentLat, distanceMatrix);

        for(int i=0; i<count; i++){
            String placeId = placesList.get(i).getId();
            double marks = (count-i)*distance;
            Marks calculatedMarks = new Marks(placeId, marks);
            distanceMarks.add(i, calculatedMarks);
        }

    }

    public  List<Place> distanceSort(List<Place> selectedPlaces, double currentLng, double currentLat, double[][] distanceMatrix){

        int count = selectedPlaces.size();
        List<Place> places = new ArrayList<Place>(selectedPlaces);
        List<Place> orderedPlaces = new ArrayList<Place>();

        double[][] distance = new double[count+1][count+1];

        distance = distanceMatrix;

        int i=0;
        int placePos=0;
        Place sortPlace = new Place();
        int[] selected = new int[count+1];
        selected[0]=0;

        for(int k=1; k<count+1; k++){
            double shortDis = Integer.MAX_VALUE;
            for(int j=0; j<count+1; j++){

                boolean notSelected = true;

                for(int m : selected){
                    if(m==j){
                        notSelected = false;
                        break;
                    }
                }

                if(notSelected){
                    if(distance[i][j]<shortDis){
                        shortDis = distance[i][j];
                        sortPlace = places.get(j-1);
                        placePos = j;
                    }
                }

            }

            orderedPlaces.add(k-1, sortPlace);
            selected[k] = placePos;
            i = placePos;

        }

        return orderedPlaces;

    }

    public  void calculateClosingTimeMarks(List<Place> selectedPlaces){

        int count = selectedPlaces.size();
        List<Place> places = new ArrayList<Place>(selectedPlaces);

        for(int i=0; i<count; i++){
            for(int j=i+1; j<count; j++){
                if(places.get(i).getClosingTime()>places.get(j).getClosingTime()){
                    Place temp = places.get(i);
                    places.set(i, places.get(j));
                    places.set(j, temp);
                }
            }
        }

        for(int i=0; i<count; i++){
            String placeId = places.get(i).getId();
            double marks = (count-i)*closingTime;
            Marks calculatedMarks = new Marks(placeId, marks);
            closingTimeMarks.add(i, calculatedMarks);
        }

    }

    public  void calculateOpeningTimeMarks(List<Place> selectedPlaces){

        int count = selectedPlaces.size();
        List<Place> places = new ArrayList<Place>(selectedPlaces);

        for(int i=0; i<count; i++){
            for(int j=i+1; j<count; j++){
                if(places.get(i).getOpeningTime()>places.get(j).getOpeningTime()){
                    Place temp = places.get(i);
                    places.set(i, places.get(j));
                    places.set(j, temp);
                }
            }
        }

        for(int i=0; i<count; i++){
            String placeId = places.get(i).getId();
            double marks = (count-i)*openingTime;
            Marks calculatedMarks = new Marks(placeId, marks);
            openingTimeMarks.add(i, calculatedMarks);
        }

    }

    public  void calculateOnWishlistMarks(List<Place> selectedPlaces){

        int count = selectedPlaces.size();
        List<Place> places = new ArrayList<Place>(selectedPlaces);

        for(int i=0; i<count; i++){
            double marks = 0;
            String placeId = places.get(i).getId();
            if(places.get(i).isOnWishList()){
                marks = onWishlist;
            }
            Marks calculatedMarks = new Marks(placeId, marks);
            onWishlistMarks.add(i, calculatedMarks);
        }

    }

    public  void calculateAvgViewTimeMarks(List<Place> selectedPlaces){

        int count = selectedPlaces.size();
        List<Place> places = new ArrayList<Place>(selectedPlaces);

        for(int i=0; i<count; i++){
            for(int j=i+1; j<count; j++){
                if(places.get(i).getAvgViewTime()>places.get(j).getAvgViewTime()){
                    Place temp = places.get(i);
                    places.set(i, places.get(j));
                    places.set(j, temp);
                }
            }
        }

        for(int i=0; i<count; i++){
            String placeId = places.get(i).getId();
            double marks = (count-i)*avgViewTime;
            Marks calculatedMarks = new Marks(placeId, marks);
            avgViewTimeMarks.add(i, calculatedMarks);
        }

    }

    public  void calculateRatingMarks(List<Place> selectedPlaces){

        int count = selectedPlaces.size();
        List<Place> places = new ArrayList<Place>(selectedPlaces);

        for(int i=0; i<count; i++){
            for(int j=i+1; j<count; j++){
                if(places.get(i).getRating()<places.get(j).getRating()){
                    Place temp = places.get(i);
                    places.set(i, places.get(j));
                    places.set(j, temp);
                }
            }
        }

        for(int i=0; i<count; i++){
            String placeId = places.get(i).getId();
            double marks = (count-i)*avgViewTime;
            Marks calculatedMarks = new Marks(placeId, marks);
            ratingMarks.add(i, calculatedMarks);
        }

    }

}