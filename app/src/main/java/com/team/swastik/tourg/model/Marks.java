package com.team.swastik.tourg.model;

public class Marks {

    private String placeId;
    private Double marks;

    public Marks(String placeId, Double marks) {
        this.setPlaceId(placeId);
        this.setMarks(marks);
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public Double getMarks() {
        return marks;
    }

    public void setMarks(Double marks) {
        this.marks = marks;
    }

}

