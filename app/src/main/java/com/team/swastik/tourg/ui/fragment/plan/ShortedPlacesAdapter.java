package com.team.swastik.tourg.ui.fragment.plan;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.team.swastik.tourg.R;
import com.team.swastik.tourg.base.BaseRecyclerViewAdapter;
import com.team.swastik.tourg.databinding.ItemPlaceByDistrictBinding;
import com.team.swastik.tourg.databinding.ItemShortedPlacesBinding;
import com.team.swastik.tourg.model.Place;
import com.team.swastik.tourg.ui.fragment.selectPlaces.PlaceByDistrictViewHolder;

import java.util.List;

public class ShortedPlacesAdapter extends BaseRecyclerViewAdapter<Place, PlaceByDistrictViewHolder> {

    public ShortedPlacesAdapter(Context context, List<Place> dataSet) {
        super(context, dataSet);
    }

    @Override
    public void bindData(PlaceByDistrictViewHolder holder, Place model, int position) {
        holder.setPlaceName(model.getName());
        Glide.with(getmContext())
                .load(model.getImageUrl())
                .dontAnimate()
                .centerCrop()
                .into(holder.getBinding().imgViewPlace);
    }

    @Override
    public PlaceByDistrictViewHolder createViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        ItemPlaceByDistrictBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_place_by_district, parent, false);
        binding.checkbox.setVisibility(View.GONE);
        return new PlaceByDistrictViewHolder(binding);
    }

    @Override
    public void onItemClick(Place model, int position) {

    }
}
