package com.team.swastik.tourg.ui.fragment.addplace;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.team.swastik.tourg.R;
import com.team.swastik.tourg.base.BaseFragment;
import com.team.swastik.tourg.databinding.FragmentAddPlaceBinding;
import com.team.swastik.tourg.di.ViewModelFactory;
import com.team.swastik.tourg.model.Address;
import com.team.swastik.tourg.model.Place;
import com.team.swastik.tourg.model.enums.AvailableType;
import com.team.swastik.tourg.model.enums.District;
import com.team.swastik.tourg.model.enums.PlaceCategory;
import com.team.swastik.tourg.ui.activity.MainViewModel;
import com.team.swastik.tourg.ui.fragment.home.HomeViewModel;
import com.team.swastik.tourg.utils.AppConstants;
import com.team.swastik.tourg.utils.DateTimeUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

public class AddPlaceFragment extends BaseFragment<FragmentAddPlaceBinding> {
    @Inject
    ViewModelFactory viewModelFactory;
    @Inject
    DateTimeUtils dateTimeUtils;

    private MainViewModel mMainViewModel;

    private ProgressDialog dialog;


    public AddPlaceFragment() {
        // Required empty public constructor
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_add_place;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        initDaggerAndViewModel();
        dialog= new ProgressDialog(getBaseActivity());
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setListeners();
        setSpinnerAdapters();
    }

    private void setListeners() {
        getBinding().btnAddPlace.setOnClickListener(v -> {
            dialog.setMessage("Uploading..");
            dialog.show();
            addPlaceToFb();
        });
    }

    private void setSpinnerAdapters(){
        getBinding().spinnerDistrict.setAdapter(new ArrayAdapter<District>(getBaseActivity(), android.R.layout.simple_spinner_item, District.values()));
      //  getBinding().spinnerAvailableType.setAdapter(new ArrayAdapter<AvailableType>(getBaseActivity(), android.R.layout.simple_spinner_item, AvailableType.values()));
        getBinding().spinnerPlaceCategory.setAdapter(new ArrayAdapter<PlaceCategory>(getBaseActivity(), android.R.layout.simple_spinner_item, PlaceCategory.values()));
    }

    private void addPlaceToFb() {
        DatabaseReference touristPlaceRef = FirebaseDatabase.getInstance().getReference(AppConstants.FB_PATH_PLACE);
        DatabaseReference placeRef = touristPlaceRef.child(AppConstants.FB_PATH_TOURIST_PLACE);
        String key = placeRef.push().getKey();
        if (key==null){
            Toast.makeText(getBaseActivity(), "Error Adding generating key", Toast.LENGTH_SHORT).show();
            return;
        }
        placeRef.child(key).setValue(createPlace(key)).addOnCompleteListener(task -> {
            if (!task.isSuccessful()){
                dialog.dismiss();
                Toast.makeText(getBaseActivity(), "Failed ", Toast.LENGTH_SHORT).show();
            }else {
                dialog.dismiss();
                clearFields();
                Toast.makeText(getBaseActivity(), "Successful!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private Place createPlace(String id){
        FragmentAddPlaceBinding binding = getBinding();
        Place place = new Place(id);
        place.setName(binding.editTxtName.getText().toString());
        place.setDesc(binding.editTxtDesc.getText().toString());
        place.setLatitude(getDoubleFromEditText(binding.editTxtLat));
        place.setLongitude(getDoubleFromEditText(binding.editTxtLng));
        Address address = new Address(
                getIntFromEditText(binding.editTxtAddressDoorNo),
                binding.editTxtAddressStreet.getText().toString(),
                binding.editTxtAddressCity.getText().toString(),
                District.getDistrictById(binding.spinnerDistrict.getSelectedItemPosition())
        );
        place.setAddress(address);
        place.setActive(binding.checkBoxActive.isChecked());
        place.setPlaceCategory(PlaceCategory.getCategoryById(binding.spinnerPlaceCategory.getSelectedItemPosition()));
        place.setTouristPlace(binding.checkBoxTourist.isChecked());
        place.setImageUrl(binding.editTxtImgUrl.getText().toString());
        place.setAvgViewTime(getIntFromEditText(binding.editTxtAverageViewTime));
        place.setRating(binding.ratingBarPlaceRating.getRating());
        place.setCreatedAt(dateTimeUtils.getCurrentDateTimeAsString());
        place.setOpeningTime(getDoubleFromEditText(binding.editTxtOpenTime));
        place.setClosingTime(getDoubleFromEditText(binding.editTxtCloseTime));
        List<String> gallery = new ArrayList<>();
        gallery.add(binding.editTxtImgUrl.getText().toString());
        gallery.add(binding.editTxtImgUrl1.getText().toString());
        gallery.add(binding.editTxtImgUrl2.getText().toString());
        place.setGallery(gallery);
        return place;

    }

    private int getIntFromEditText(EditText editText) {
        String value = editText.getText().toString();
        if (value.length()==0)
            return -1;
        return Integer.parseInt(value);
    }

    private float getFloatFromEditText(EditText editText) {
        return Float.parseFloat(editText.getText().toString());
    }

    private double getDoubleFromEditText(EditText editText) {
        return Double.parseDouble(editText.getText().toString());
    }


    private void initDaggerAndViewModel() {
        AndroidSupportInjection.inject(this);
        // ViewModelProvider viewModelProvider = new ViewModelProvider(this, viewModelFactory);
        mMainViewModel = ViewModelProviders.of(getBaseActivity(), viewModelFactory).get(MainViewModel.class);
    }

    private void clearFields() {
        getBinding().editTxtName.getText().clear();
        getBinding().editTxtDesc.getText().clear();
        getBinding().editTxtAddressCity.getText().clear();
        getBinding().editTxtAddressDoorNo.getText().clear();
        getBinding().editTxtAddressStreet.getText().clear();
        getBinding().editTxtAverageViewTime.getText().clear();
        getBinding().editTxtImgUrl.getText().clear();
        getBinding().editTxtLat.getText().clear();
        getBinding().editTxtLng.getText().clear();
        getBinding().editTxtOpenTime.getText().clear();
        getBinding().editTxtCloseTime.getText().clear();
        getBinding().editTxtImgUrl1.getText().clear();
        getBinding().editTxtImgUrl2.getText().clear();
    }


}