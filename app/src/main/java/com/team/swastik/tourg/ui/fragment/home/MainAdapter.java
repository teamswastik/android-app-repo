package com.team.swastik.tourg.ui.fragment.home;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bumptech.glide.Glide;
import com.team.swastik.tourg.R;
import com.team.swastik.tourg.base.BaseActivity;
import com.team.swastik.tourg.base.BaseRecyclerViewAdapter;
import com.team.swastik.tourg.base.listeners.IPlaceListener;
import com.team.swastik.tourg.databinding.ItemHomeMainRecyclerBinding;
import com.team.swastik.tourg.databinding.ItemHomePlaceSliderBinding;
import com.team.swastik.tourg.model.Place;
import com.team.swastik.tourg.ui.fragment.home.viewholder.PlaceViewHolder;
import com.team.swastik.tourg.ui.fragment.home.viewholder.RecyclerViewHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import timber.log.Timber;

public class MainAdapter extends BaseRecyclerViewAdapter<HomeModel, RecyclerViewHolder> implements IPlaceListener {
    private IMainAdapterListener mainAdapterListener;
    private ArrayList<BaseRecyclerViewAdapter> mAdapters = new ArrayList<>();

    private Set<String> wishLists;


    public MainAdapter(Context context, List<HomeModel> dataSet, IMainAdapterListener listener) {
        super(context, dataSet);
        this.mainAdapterListener = listener;
    }

    @Override
    public void bindData(RecyclerViewHolder holder, HomeModel model, int position) {
        holder.setTitle(model.getTitle());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getmContext(), model.getLayoutType().getScroll(), false);
        PlaceAdapter<ItemHomePlaceSliderBinding> placeAdapter = createPlaceAdapter(model.getItems(), position);
        mAdapters.add(position, placeAdapter);
        holder.initRecyclerView(linearLayoutManager, placeAdapter);
    }

    @Override
    public RecyclerViewHolder createViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        ItemHomeMainRecyclerBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_home_main_recycler, parent, false);
        return new RecyclerViewHolder(binding);
    }

    @Override
    public void onItemClick(HomeModel model, int position) {

    }

    @Override
    public int getItemViewType(int position) {
        return getDataSet().get(position).getLayoutType().getValue();
    }

    public Set<String> getWishLists() {
        return wishLists;
    }

    public void setWishLists(Set<String> wishLists) {
        this.wishLists = wishLists;
    }

    public PlaceAdapter<ItemHomePlaceSliderBinding> createPlaceAdapter(List<Place> places, int mainAdapterPosition) {
        PlaceAdapter<ItemHomePlaceSliderBinding> placeAdapter = new PlaceAdapter<ItemHomePlaceSliderBinding>(getmContext(), places) {
            @Override
            public void bindData(PlaceViewHolder<ItemHomePlaceSliderBinding> holder, Place model, int position) {
                ItemHomePlaceSliderBinding binding = holder.getBinding();
                binding.txtViewPlaceName.setText(model.getName());
                binding.txtViewPlaceDesc.setText(model.getDesc());
                binding.ratingBarPlaceRating.setRating((float) model.getRating());

                if (wishLists != null && wishLists.contains(model.getId())) {
                    binding.imgViewAddToWishList.setImageResource(R.drawable.ic_heart_pink_fill);
                    model.setOnWishList(true);
                } else {
                    binding.imgViewAddToWishList.setImageResource(R.drawable.ic_heart_black_border);
                    model.setOnWishList(false);
                }

                binding.imgViewAddToWishList.setOnClickListener(v -> {
                    Timber.e("clicked");
                    getListener().likeClicked(model.getId(), model.isOnWishList(), position);
                });

                binding.getRoot().setOnClickListener(v -> {
                    getListener().placeClicked(model.getId());
                });

                Glide.with(getmContext())
                        .load(model.getImageUrl())

                        .dontAnimate()
                        .centerCrop()
                        .into(binding.imgViewPlace);
            }

            @Override
            public PlaceViewHolder<ItemHomePlaceSliderBinding> createViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
                ItemHomePlaceSliderBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_home_place_slider, parent, false);
                ViewGroup.LayoutParams layoutParams = binding.getRoot().getLayoutParams();
                DisplayMetrics displayMetrics = new DisplayMetrics();
                ((BaseActivity) getmContext()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                layoutParams.width = (int) (displayMetrics.widthPixels * 0.8);
                binding.getRoot().setLayoutParams(layoutParams);
                return new PlaceViewHolder<>(binding);
            }

            @Override
            public void onItemClick(Place model, int position) {

            }
        };
        placeAdapter.setListener(new IPlaceListener() {
            @Override
            public void likeClicked(String placeId, boolean current, int position) {
                 mainAdapterListener.onPlaceLikeClicked(placeId, current, position, mainAdapterPosition);
            }

            @Override
            public void placeClicked(String placeId) {
                mainAdapterListener.onPlaceClicked(placeId);
            }
        });
        return placeAdapter;
    }

    @Override
    public void likeClicked(String placeId, boolean current, int position) {

    }

    @Override
    public void placeClicked(String placeId) {

    }

    public void notifyAdapterItemChanged(String changedId, int mainAdapterPosition, int placeAdapterPosition, Set<String> wishLists) {
        setWishLists(wishLists);
        if (mAdapters != null) {
            BaseRecyclerViewAdapter adapter = mAdapters.get(mainAdapterPosition);
            adapter.notifyItemChanged(placeAdapterPosition);
        }
        checkAndNotifyPlaceAdapters(changedId, mainAdapterPosition);

    }

    public void checkAndNotifyPlaceAdapters(String placeId, int mainAdapterPosition) {
        List<HomeModel> dataSet = getDataSet();
        for (int i = 0; i < dataSet.size(); i++) {
            if (i == mainAdapterPosition) continue;
            List<Place> places = dataSet.get(i).getItems();
            int placePosition = checkIdInPlaceList(places, placeId);
            if (mAdapters != null && placePosition > -1)
                mAdapters.get(i).notifyItemChanged(placePosition);

        }
    }

    private int checkIdInPlaceList(List<Place> places, String idToCheck) {
        for (int i = 0; i < places.size(); i++) {
            if (places.get(i).getId().equals(idToCheck))
                return i;
        }
        return -1;
    }
}
