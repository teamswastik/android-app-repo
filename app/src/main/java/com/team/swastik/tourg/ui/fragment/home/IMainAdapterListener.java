package com.team.swastik.tourg.ui.fragment.home;

import com.team.swastik.tourg.base.listeners.IPlaceListener;

public interface IMainAdapterListener {
    void onPlaceLikeClicked(String placeId,boolean currentState,int placeAdapterPosition,int mainAdapterPosition);
    void onPlaceClicked(String placeId);
}
