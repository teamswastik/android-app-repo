package com.team.swastik.tourg.di.module;

import com.team.swastik.tourg.di.ORSClient;

import com.team.swastik.tourg.di.TestClient;
import com.team.swastik.tourg.di.TourgAppScope;
import com.team.swastik.tourg.utils.AppConstants;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public abstract class RetrofitModule {

    @Provides
    @TourgAppScope
    @ORSClient
    static Retrofit provideRetrofitClient(OkHttpClient okHttpClient){
        return new retrofit2.Retrofit.Builder()
                .baseUrl(AppConstants.OPEN_ROUTE_SERVICE_BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @Provides
    @TestClient
    public static Retrofit provideTestClient( OkHttpClient okHttpClient){
        return new retrofit2.Retrofit.Builder()
                .baseUrl(AppConstants.JSON_TEST_API_BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }


}
