package com.team.swastik.tourg.retrofit.interfaces;

import com.team.swastik.tourg.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface UserService {

    @GET("/users")
    Call<List<User>> getAllUsers();
}
