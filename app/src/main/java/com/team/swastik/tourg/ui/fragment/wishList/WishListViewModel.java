package com.team.swastik.tourg.ui.fragment.wishList;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.team.swastik.tourg.R;
import com.team.swastik.tourg.base.BaseViewModel;
import com.team.swastik.tourg.data.database.AppDatabase;
import com.team.swastik.tourg.model.Place;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class WishListViewModel extends BaseViewModel {

    private MutableLiveData<List<Place>> wishLists = new MutableLiveData<>();
    private AppDatabase appDatabase;
    @Inject
    public WishListViewModel(AppDatabase appDatabase) {
        this.appDatabase = appDatabase;
    }



    public LiveData<List<Place>> getWishLists(List<String> ids) {
        return appDatabase.placeDao().getPlaceByIdList(ids);
    }
}