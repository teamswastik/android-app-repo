package com.team.swastik.tourg.data.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.team.swastik.tourg.data.database.entity.TripPlaceCrossRef;
import com.team.swastik.tourg.data.database.entity.TripWithPlaces;
import com.team.swastik.tourg.model.Place;
import com.team.swastik.tourg.model.Trip;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

@Dao
public interface TripDao {
    @Transaction
    @Query("SELECT * FROM Trip")
    LiveData<List<TripWithPlaces>> getTripWithPlaces();

    @Transaction
    @Query("SELECT * FROM Trip WHERE id = :id")
    Single<TripWithPlaces> getTripWithPlaceWithId(Long id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Single<Long> insert(Trip trip);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    Completable update(Trip trip);

    @Insert
    Completable insertPlaceIntoTrip(TripPlaceCrossRef tripPlaceCrossRef);

    @Insert
    Completable insertPlaceListIntoTrip(List<TripPlaceCrossRef> crossRefList);


}
