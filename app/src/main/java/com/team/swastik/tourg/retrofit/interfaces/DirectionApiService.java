package com.team.swastik.tourg.retrofit.interfaces;

import com.google.gson.JsonObject;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface DirectionApiService {


    @GET("/maps/api/directions/json")
    Call<JsonObject> getAllDirectionStatus(@Query("origin") String orgin,
                                           @Query("destination") String destination,
                                           @Query("key") String key);
}
