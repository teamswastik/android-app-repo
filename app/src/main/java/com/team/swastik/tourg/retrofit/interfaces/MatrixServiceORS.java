package com.team.swastik.tourg.retrofit.interfaces;

import com.google.gson.JsonObject;
import com.team.swastik.tourg.model.User;
import com.team.swastik.tourg.utils.AppConstants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MatrixServiceORS {

    @Headers({
            "Accept: application/json",
            "Content-Type: application/json",
            "Authorization: " + AppConstants.ORS_API_KEY
    })
    @POST("/v2/matrix/"+ AppConstants.OPEN_ROUTE_SERVICE_PROFILE_CAR)
    Observable<JsonObject> getMatrix(@Body JSONObject postparams);

    @GET("/maps/api/directions/json")
    Call<JsonObject> getAllDirectionStatus(@Query("origin") String orgin,
                                           @Query("destination") String destination,
                                           @Query("key") String key);
}
