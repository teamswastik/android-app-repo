package com.team.swastik.tourg.base;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;

import dagger.android.support.AndroidSupportInjection;

public abstract class BaseFragment<T extends ViewDataBinding> extends Fragment {

    private BaseActivity mBaseActivity;
    private T mBinding;
    private View mRootView;



    /**
     * @return layout resource id
     */
    public abstract
    @LayoutRes
    int getLayoutId();


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof BaseActivity)
            this.mBaseActivity = (BaseActivity) context;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false);
        mBinding.setLifecycleOwner(this);
        mRootView = mBinding.getRoot();
        return mRootView;
    }

    @Override
    public void onDetach() {
        mBaseActivity = null;
        super.onDetach();
    }

    public BaseActivity getBaseActivity() {
        return mBaseActivity;
    }

    public T getBinding() {
        return mBinding;
    }

    private void performDependencyInjection() {
        AndroidSupportInjection.inject(this);
    }
}
