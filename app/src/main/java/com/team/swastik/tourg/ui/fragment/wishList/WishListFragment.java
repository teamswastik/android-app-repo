package com.team.swastik.tourg.ui.fragment.wishList;

import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.team.swastik.tourg.R;
import com.team.swastik.tourg.base.BaseActivity;
import com.team.swastik.tourg.base.BaseFragment;
import com.team.swastik.tourg.base.listeners.IPlaceListener;
import com.team.swastik.tourg.databinding.FragmentWishlistBinding;
import com.team.swastik.tourg.databinding.ItemHomePlaceSliderBinding;
import com.team.swastik.tourg.di.ViewModelFactory;
import com.team.swastik.tourg.model.Place;
import com.team.swastik.tourg.ui.activity.MainViewModel;
import com.team.swastik.tourg.ui.fragment.home.HomeFragmentDirections;
import com.team.swastik.tourg.ui.fragment.home.PlaceAdapter;
import com.team.swastik.tourg.ui.fragment.home.viewholder.PlaceViewHolder;
import com.team.swastik.tourg.ui.fragment.plan.ShortedPlacesAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;
import timber.log.Timber;

public class WishListFragment extends BaseFragment<FragmentWishlistBinding> implements IPlaceListener {


    public WishListFragment() {
    }

    @Inject
    ViewModelFactory viewModelFactory;

    private WishListViewModel wishListViewModel;
    private MainViewModel mMainViewModel;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_wishlist;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        initDaggerAndViewModel();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if ( mMainViewModel.getFavouritePlacesLiveData().getValue()!=null && mMainViewModel.getFavouritePlacesLiveData().getValue().size()>0){
            Set<String> value = mMainViewModel.getFavouritePlacesLiveData().getValue();
            List<String> placeIds = new ArrayList<>(value);
            wishListViewModel.getWishLists(placeIds).observe(this.getViewLifecycleOwner(), places -> {
                if (places!=null){
                    initRecyclerView(places);
                }
            });
        }else {
            getBinding().txtViewNoWishlist.setVisibility(View.VISIBLE);
        }

        setObservers();

    }

    private void  setObservers(){


    }

    private void initRecyclerView(List<Place> places) {
        RecyclerView recyclerView = getBinding().recylcerWishlist;
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity(),LinearLayoutManager.VERTICAL,false));
        PlaceAdapter<ItemHomePlaceSliderBinding> adapter = createAdapter(getBaseActivity(), places);
        recyclerView.setAdapter(adapter);
    }

    private PlaceAdapter<ItemHomePlaceSliderBinding>  createAdapter(Context context ,List<Place> places){
        PlaceAdapter<ItemHomePlaceSliderBinding> placeAdapter = new PlaceAdapter<ItemHomePlaceSliderBinding>(context, places) {
            @Override
            public void bindData(PlaceViewHolder<ItemHomePlaceSliderBinding> holder, Place model, int position) {
                ItemHomePlaceSliderBinding binding = holder.getBinding();
                binding.txtViewPlaceName.setText(model.getName());
                binding.txtViewPlaceDesc.setText(model.getDesc());
                binding.ratingBarPlaceRating.setRating((float) model.getRating());


                binding.getRoot().setOnClickListener(v -> {
                    getListener().placeClicked(model.getId());
                });

                Glide.with(getmContext())
                        .load(model.getImageUrl())
                        .dontAnimate()
                        .centerCrop()
                        .into(binding.imgViewPlace);
            }

            @Override
            public PlaceViewHolder<ItemHomePlaceSliderBinding> createViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
                ItemHomePlaceSliderBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_home_place_slider, parent, false);
                binding.imgViewAddToWishList.setVisibility(View.GONE);
                return new PlaceViewHolder<>(binding);
            }

            @Override
            public void onItemClick(Place model, int position) {

            }
        };
        placeAdapter.setListener(this);
        return placeAdapter;
    }


    private void initDaggerAndViewModel() {
        AndroidSupportInjection.inject(this);
        // ViewModelProvider viewModelProvider = new ViewModelProvider(this, viewModelFactory);
        wishListViewModel = ViewModelProviders.of(this, viewModelFactory).get(WishListViewModel.class);
        mMainViewModel = ViewModelProviders.of(getBaseActivity(), viewModelFactory).get(MainViewModel.class);


    }

    @Override
    public void likeClicked(String placeId, boolean current, int position) {

    }

    @Override
    public void placeClicked(String placeId) {
        WishListFragmentDirections.ActionNavigationWishlistToPlaceFragment action = WishListFragmentDirections.actionNavigationWishlistToPlaceFragment(placeId);
        Navigation.findNavController(getBinding().getRoot()).navigate(action);
    }
}
