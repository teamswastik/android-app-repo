package com.team.swastik.tourg.ui.fragment.selectPlaces;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.team.swastik.tourg.R;
import com.team.swastik.tourg.base.BaseRecyclerViewAdapter;
import com.team.swastik.tourg.databinding.ItemDistrictBinding;
import com.team.swastik.tourg.databinding.ItemPlaceByDistrictBinding;
import com.team.swastik.tourg.model.Place;
import com.team.swastik.tourg.ui.fragment.selectPlan.DistrictViewHolder;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import timber.log.Timber;

public class PlaceByDistrictAdapter extends BaseRecyclerViewAdapter<Place, PlaceByDistrictViewHolder> {

    private Set<Place> selectedList = new HashSet<>();

    public PlaceByDistrictAdapter(Context context, List<Place> dataSet) {
        super(context, dataSet);
    }

    @Override
    public void bindData(PlaceByDistrictViewHolder holder, Place model, int position) {
        holder.setPlaceName(model.getName());
        if (selectedList.contains(model))
            holder.getBinding().checkbox.setChecked(true);
        else holder.getBinding().checkbox.setChecked(false);

        holder.getBinding().checkbox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked){
                selectedList.add(model);
                Timber.e("Added");
            }else{
                selectedList.remove(model);
                Timber.e("Removed");
            }
        });
        Glide.with(getmContext())
                .load(model.getImageUrl())
                .dontAnimate()
                .centerCrop()
                .into(holder.getBinding().imgViewPlace);
    }

    @Override
    public PlaceByDistrictViewHolder createViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        ItemPlaceByDistrictBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_place_by_district, parent, false);
        return new PlaceByDistrictViewHolder(binding);
    }

    @Override
    public void onItemClick(Place model, int position) {

    }

    public Set<Place> getSelectedList() {
        return selectedList;
    }
}
