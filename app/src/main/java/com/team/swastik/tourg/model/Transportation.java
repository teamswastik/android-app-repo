package com.team.swastik.tourg.model;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class Transportation {
    private String medium;
    private String routeNumber;
    private float cost;
    private String departure;
    private String destination;
    @Ignore
    public Transportation(String medium, String routeNumber, float cost, String departure, String destination) {
        this.medium = medium;
        this.routeNumber = routeNumber;
        this.cost = cost;
        this.departure = departure;
        this.destination = destination;
    }

    public Transportation() {
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getRouteNumber() {
        return routeNumber;
    }

    public void setRouteNumber(String routeNumber) {
        this.routeNumber = routeNumber;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    @Override
    public String toString() {
        return "Transportation{" +
                "medium='" + medium + '\'' +
                ", routeNumber='" + routeNumber + '\'' +
                ", cost=" + cost +
                ", departure='" + departure + '\'' +
                ", destination='" + destination + '\'' +
                '}';
    }
}
