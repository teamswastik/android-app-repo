package com.team.swastik.tourg.ui.fragment.search;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.team.swastik.tourg.base.BaseViewModel;

import javax.inject.Inject;

public class SearchViewModel extends BaseViewModel {

    private MutableLiveData<String> mText;

    @Inject
    public SearchViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is search fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
