package com.team.swastik.tourg.base.listeners;

public interface IPlaceListener {
    void likeClicked(String placeId,boolean current,int position);
    void placeClicked(String placeId);
}
