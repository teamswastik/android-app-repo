package com.team.swastik.tourg.ui.fragment.plan;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.JsonObject;
import com.team.swastik.tourg.base.BaseViewModel;
import com.team.swastik.tourg.data.database.AppDatabase;
import com.team.swastik.tourg.data.database.dao.PlaceDao;
import com.team.swastik.tourg.data.database.dao.TripDao;
import com.team.swastik.tourg.data.database.entity.TripPlaceCrossRef;
import com.team.swastik.tourg.model.Place;
import com.team.swastik.tourg.model.Trip;
import com.team.swastik.tourg.repository.matrix.MatrixRepository;
import com.team.swastik.tourg.repository.place.PlaceRepository;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class PlanViewModel extends BaseViewModel {
    private MutableLiveData<List<Place>> sortedList = new MutableLiveData<>();
    private AppDatabase appDatabase;
    private PlaceDao placeDao;
    private TripDao tripDao;
    private MutableLiveData<Boolean> creationSuccessful = new MutableLiveData<>();

    @Inject
    public PlanViewModel(AppDatabase appDatabase) {
        this.appDatabase = appDatabase;
        this.placeDao = appDatabase.placeDao();
        this.tripDao = appDatabase.tripDao();
    }

    public void createTrip(Trip trip,List<Place> selectedList) {
        getCompositeDisposable().add(
                tripDao.insert(trip)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                (id) ->{
                                    Timber.e("Insert Trip Success");
                                    Timber.e("id: "+id);

                                    insertPlacesIntoTrip(id,selectedList);

                                },
                                throwable -> {
                                    Timber.e("Error in insert Trip");
                                    throwable.printStackTrace();
                                }
                        )
        );


    }

    public void insertPlacesIntoTrip(long tripId,List<Place> places) {
        getCompositeDisposable().add(
                tripDao.insertPlaceListIntoTrip(generateCrossRef(tripId,places))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                () -> {
                                    Timber.e("Insert TripCrossRef Completed");
                                    creationSuccessful.setValue(true);
                                },
                                throwable -> {
                                    Timber.e("Error Inserting");
                                    throwable.printStackTrace();
                                }
                        )
        );

    }

    private List<TripPlaceCrossRef> generateCrossRef(long tripId,List<Place> places){
        List<TripPlaceCrossRef> list = new ArrayList<>();
        for (Place place : places){
            list.add(new TripPlaceCrossRef(tripId,place.getId()));
        }

        return list;
    }

    public LiveData<List<Place>> getSortedListLiveData() {
        return sortedList;
    }

    public void setSortedList(List<Place> sortedList) {
        this.sortedList.setValue(sortedList);
    }

    public LiveData<Boolean> getCreationSuccessful() {
        return creationSuccessful;
    }
}