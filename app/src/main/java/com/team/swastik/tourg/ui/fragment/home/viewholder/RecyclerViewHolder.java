package com.team.swastik.tourg.ui.fragment.home.viewholder;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.team.swastik.tourg.base.BaseViewHolder;
import com.team.swastik.tourg.databinding.ItemHomeMainRecyclerBinding;


public class RecyclerViewHolder extends BaseViewHolder<ItemHomeMainRecyclerBinding> {

    public RecyclerViewHolder(ItemHomeMainRecyclerBinding binding) {
        super(binding);
    }

    public void setTitle(String title){
        if (title!=null)
            getBinding().txtViewTitle.setText(title);
        else getBinding().txtViewTitle.setVisibility(View.GONE);
    }

    public void initRecyclerView(RecyclerView.LayoutManager layoutManager,RecyclerView.Adapter adapter){
        RecyclerView recyclerView = getBinding().recyclerView;
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

    }


}
