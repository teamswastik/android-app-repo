package com.team.swastik.tourg.ui.activity;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.team.swastik.tourg.R;
import com.team.swastik.tourg.databinding.ItemAccommodationBinding;
import com.team.swastik.tourg.model.Place;

import java.util.List;

public class AccommodationAdapter extends RecyclerView.Adapter<AccommodationAdapter.ViewHolder>  {
    private List<Place> mDataSet;
    StartTrip startTrip;

    public AccommodationAdapter(List<Place> mDataSet, StartTrip startTrip) {
        this.mDataSet = mDataSet;
        this.startTrip = startTrip;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ItemAccommodationBinding mBinding = DataBindingUtil.inflate(inflater, R.layout.item_accommodation, parent, false);
        return new ViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ItemAccommodationBinding binding = holder.mBinding;
        Place place = mDataSet.get(position);
        binding.txtViewAccommodationName.setText(place.getName());
        Glide.with(startTrip)
                .load(place.getImageUrl())
                .dontAnimate()
                .centerCrop()
                .into(binding.imgViewAccommodation);
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private ItemAccommodationBinding mBinding;

        public ViewHolder(ItemAccommodationBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        public ItemAccommodationBinding getBinding() {
            return mBinding;
        }

    }
}
