package com.team.swastik.tourg.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.team.swastik.tourg.R;
import com.team.swastik.tourg.ui.activity.HomeActivity;
import com.team.swastik.tourg.ui.activity.MainViewModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import timber.log.Timber;

public class ShortService extends IntentService {

    public String[] places;
    double[][] locations;
    public int[] avgViewTime;
    public double[][] allDistance = null;
    public double[][] distance = null;
    public String[] place = null;
    public String[] ShortedList = null;
    int tripDays = 0;

    public ShortService(){
        super("Short Service");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        System.out.println("shorting service has started");

        this.allDistance = null;
        this.distance = null;
        this.place = null;
        this.ShortedList = null;
        this.tripDays=0;

    }

    @Override
    protected void onHandleIntent (Intent intent) {

        this.places = intent.getStringArrayExtra("allPlaces");
        this.locations = (double[][]) intent.getSerializableExtra("allLocations");
        this.avgViewTime = intent.getIntArrayExtra("allAvgViewTime");

        GetMatrix(this.locations);
        if(this.allDistance == null){
            while (this.allDistance == null) {
                System.out.println("Waiting for data");
            }
        }
        String type = intent.getStringExtra("type");
        if(type.equals("PlanTripForMe")){
            String strTripDays = intent.getStringExtra("tripDays");
            this.tripDays = Integer.parseInt(strTripDays);
            SelectPlaces(locations,places,avgViewTime);
        }
        if(type.equals("PlanTrip")){
            this.distance = this.allDistance;
            this.place = places;
            Short(this.distance,this.place);
        }
        if(this.ShortedList == null){
            while (this.ShortedList == null) {
                System.out.println("Waiting for data");
            }
        }

        ResultReceiver receiver = intent.getParcelableExtra("receiver");
        Bundle bundle = new Bundle();
        bundle.putStringArray("ShortedList", this.ShortedList);
        receiver.send(001, bundle);
        System.out.println("shorting finished");
        stopSelf();

    }

    public void Short(double[][] distances, String[] places){

        if(this.distance == null){
            while (this.distance == null) {
                System.out.println("Waiting for data");
            }
        }

        String[] placeList = places;
        double[][] distance = distances;

        String shortPlace = "Current Location";
        String[] ShortedList = new String[placeList.length];
        int[] SelectedPlace = new int[placeList.length];
        int i = 0;
        int place = 0;

        ShortedList[0] = placeList[0];
        SelectedPlace[0] = 0;

        for (int k = 1; k < placeList.length; k++) {

            double shortdis = 1000000000;

            for (int j = 0; j < placeList.length; j++) {

                boolean selected = true;

                for (int l : SelectedPlace) {
                    if (l == j) {
                        selected = false;
                        break;
                    }
                }

                if (selected) {

                    if (distance[i][j] < shortdis) {
                        shortdis = distance[i][j];
                        shortPlace = placeList[j];
                        place = j;
                    }

                }

            }

            ShortedList[k] = shortPlace;
            SelectedPlace[k] = place;
            i = place;

        }

        this.ShortedList = ShortedList;

    }

    public void GetMatrix(double[][] locations){

        final RequestQueue requestQueue = Volley.newRequestQueue(this);

        String matrix_url = getString(R.string.ORS_MATRIX_URL);

        JSONObject postparams = new JSONObject();
        try {
            postparams.put("locations",new JSONArray(locations));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Timber.e(postparams.toString());

        JsonObjectRequest objectRequest = new JsonObjectRequest(
                Request.Method.POST,
                matrix_url,
                postparams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONObject responseObject = response;
                        JSONArray responseArray = null;
                        JSONArray responseArrayList = null;
                        try {
                            responseArray = responseObject.getJSONArray("durations");
                            if (responseArray == null) {
                                System.out.println("json is empty");
                            }
                            else
                            {
                                int responseArrayLength = responseArray.length();
                                double responseOutput[][] = new double[responseArrayLength][responseArrayLength];
                                for (int i=0;i<responseArrayLength;i++){
                                    responseArrayList = (JSONArray) responseArray.get(i);
                                    int responseArrayListLength = responseArrayList.length();
                                    for (int j=0;j<responseArrayListLength;j++){
                                        responseOutput[i][j] =(double) responseArrayList.get(j);
                                    }
                                }
                                allDistance = responseOutput;
                                System.out.println("distance is ready to short");
                                Timber.e("Distnace sorted");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Timber.e("on Error Response");
                    }
                }
        )
        {
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Accept", "application/json");
                headers.put("Authorization", getString(R.string.ORS_API_KEY));
                return headers;
            }
        };

        requestQueue.add(objectRequest);

    }

    public void SelectPlaces(double[][] locations, String[] places, int[] avgViewTime){

        double dayCountSec = 0;
        double tripDaysSec = this.tripDays*60*60*8;
        double viewTimeSec;

        int placeCount = 1;

        if(this.allDistance == null){
            while (this.allDistance == null) {
                System.out.println("Waiting for data");
            }
        }

        String[] placeList = places;
        double[][] allDistance = this.allDistance;

        for (int i=0; i<placeList.length-1; i++){
            int j = i+1;
            viewTimeSec = avgViewTime[j]*60*60;
            dayCountSec = dayCountSec + allDistance[i][j] + viewTimeSec;
            if (dayCountSec<tripDaysSec) {
                placeCount = placeCount + 1;
            }
        }

        double[][] selectedLocations = new double[placeCount][];
        String[] selectedPlaces = new String[placeCount];

        for (int k=0; k<placeCount; k++){
            selectedPlaces[k]=places[k];
            selectedLocations[k]=locations[k];
        }

        this.allDistance = null;
        this.distance = null;

        if(placeCount==1){
            String[] mShortedList = new String[1];
            mShortedList[0] = "Can't visit any places within this days. Please Change number of days or trip location";
            this.ShortedList=mShortedList;
            System.out.println("Can't visit any places within this days");
        }

        else{
            GetMatrix(selectedLocations);

            if(this.allDistance == null){
                while (this.allDistance == null) {
                    System.out.println("Waiting for data");
                }
            }

            this.distance = this.allDistance;
            this.place = selectedPlaces;

            Short(this.distance,this.place);
        }

    }

}
