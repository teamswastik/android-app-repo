package com.team.swastik.tourg.model.enums;

public enum TripStatus {
    TRIP_PLANNED,
    CURRENTLY_ON_TRIP,
    TRIP_COMPLETED,
    TRIP_ENDED,
    TRIP_PAUSEPONDED,
    TRIP_CANCELED
}
