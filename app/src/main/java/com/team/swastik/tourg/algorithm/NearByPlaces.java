package com.team.swastik.tourg.algorithm;

import com.team.swastik.tourg.model.Place;
import java.util.ArrayList;
import java.util.List;

public class NearByPlaces {

    public static List<Place> SelectNearByPlaces(double disLimit, double currentLat, double currentLng, List<Place> placesList){
        List<Place> selectedNearByPlaces = new ArrayList<Place>();

        for(int i=0; i<placesList.size(); i++){
            double calculatedDis = distance(currentLat, currentLng, placesList.get(i).getLatitude(), placesList.get(i).getLongitude());
            if(calculatedDis<disLimit){
                selectedNearByPlaces.add(placesList.get(i));
            }
        }

        return selectedNearByPlaces;
    }

    public static double distance(double lat1, double lon1, double lat2, double lon2)
    {

        lon1 = Math.toRadians(lon1);
        lon2 = Math.toRadians(lon2);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        double dlon = lon2 - lon1;
        double dlat = lat2 - lat1;
        double a = Math.pow(Math.sin(dlat / 2), 2)
                + Math.cos(lat1) * Math.cos(lat2)
                * Math.pow(Math.sin(dlon / 2),2);

        double c = 2 * Math.asin(Math.sqrt(a));

        double r = 6371;

        return(c * r);
    }

}
