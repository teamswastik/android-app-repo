package com.team.swastik.tourg.ui.fragment.selectPlaces;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.team.swastik.tourg.base.BaseViewModel;
import com.team.swastik.tourg.data.database.AppDatabase;
import com.team.swastik.tourg.data.database.dao.PlaceDao;
import com.team.swastik.tourg.model.Place;
import com.team.swastik.tourg.model.enums.District;
import com.team.swastik.tourg.repository.place.PlaceRepository;

import java.util.List;

import javax.inject.Inject;

public class SelectPlacesViewModel extends BaseViewModel {
    private PlaceRepository mPlaceRepository;
   // private MutableLiveData<List<Place>> placeByDistrictLiveData = new MutableLiveData<>() ;
    private AppDatabase appDatabase;
    private PlaceDao placeDao;

    @Inject
    public SelectPlacesViewModel(PlaceRepository placeRepository, AppDatabase appDatabase) {
        this.mPlaceRepository= placeRepository;
        this.appDatabase= appDatabase;
        this.placeDao = appDatabase.placeDao();
    }

    public void getPlaceByDistrict(District district){
        List<Place> places = mPlaceRepository.getPlaceByDistrict(district).getValue();
       // this.placeByDistrictLiveData.setValue(places);
    }

    public LiveData<List<Place>> getPlaceByDistrictLiveData(District district) {
        return mPlaceRepository.getPlaceByDistrict(district);
    }

    public LiveData<List<Place>> getPlacesByDistrictAndTouristPlace(District district,boolean isTouristPlace){
        return placeDao.getPlacesByDistrictAndTouristPlace(district,isTouristPlace);
    }
}