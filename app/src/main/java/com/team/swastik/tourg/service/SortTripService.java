package com.team.swastik.tourg.service;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.team.swastik.tourg.R;
import com.team.swastik.tourg.algorithm.PlanTrip;
import com.team.swastik.tourg.model.Place;
import com.team.swastik.tourg.utils.AppConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

public class SortTripService extends IntentService {
    public static final String SORT_TRIP_RESULT = "SORT_TRIP_RESULT";
    public static final String KEY_SORT_SUCCESS = "KEY_SORT_SUCCESS";
    public static final String KEY_SORT_PLACE_LIST = "KEY_SORT_PLACE_LIST";


    public SortTripService() {
        super("SortTripService");
        Timber.e("SortTrip Service");
    }



    @SuppressLint("CheckResult")
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        if (intent!=null && intent.getExtras()!=null){
            Bundle bundle = intent.getExtras();
            List<Place> parcelableArrayList = bundle.getParcelableArrayList(AppConstants.SORT_SERVICE_KEY_PLACES);
            double lat = bundle.getDouble(AppConstants.SORT_SERVICE_KEY_LAT);
            double lng = bundle.getDouble(AppConstants.SORT_SERVICE_KEY_LNG);

            if (parcelableArrayList!=null){
                double[][] sendingMatrix = distanceSort(parcelableArrayList, lng, lat);
                getMatrix(sendingMatrix,parcelableArrayList,lng,lat);

            }else Timber.e("List of Place is null");

        }




    }

    public double[][] distanceSort(List<Place> selectedPlaces, double currentLocationLng, double currentLocationLat){

        int count = selectedPlaces.size();
        List<Place> places = new ArrayList<Place>(selectedPlaces);

        Timber.e(selectedPlaces.toString());

        double[][] locations = new double[count+1][2];

        locations[0][0] = currentLocationLng;
        locations[0][1] = currentLocationLat;
        for(int i=1; i<count+1; i++){
            locations[i][0] = places.get(i-1).getLongitude();
            locations[i][1] = places.get(i-1).getLatitude();
        }

        return locations;

    }

    public void getMatrix(double[][] locationMatrix,List<Place> selectedPlaces,double currentLng,double currentLat){

        final RequestQueue requestQueue = Volley.newRequestQueue(this);

        String matrix_url = getString(R.string.ORS_MATRIX_URL);

        JSONObject postparams = new JSONObject();
        try {
            postparams.put("locations",new JSONArray(locationMatrix));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Timber.e(postparams.toString());

        JsonObjectRequest objectRequest = new JsonObjectRequest(
                Request.Method.POST,
                matrix_url,
                postparams,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONObject responseObject = response;
                        System.out.println("Problem"+responseObject.toString());
                        JSONArray responseArray = null;
                        JSONArray responseArrayList = null;
                        try {
                            responseArray = responseObject.getJSONArray("durations");
                            if (responseArray == null) {
                                System.out.println("json is empty");
                            }
                            else
                            {
                                int responseArrayLength = responseArray.length();
                                double responseOutput[][] = new double[responseArrayLength][responseArrayLength];
                                for (int i=0;i<responseArrayLength;i++){
                                    responseArrayList = (JSONArray) responseArray.get(i);
                                    int responseArrayListLength = responseArrayList.length();
                                    for (int j=0;j<responseArrayListLength;j++){
                                        responseOutput[i][j] =(double) responseArrayList.get(j);
                                    }
                                }
                                Timber.e("Got Matrix from backend");
                                PlanTrip planTrip = new PlanTrip();
                                List<Place> places = planTrip.makePlan(selectedPlaces, currentLng, currentLat, responseOutput);
                                sendBroadcast(places,true);
                                Timber.e(places.toString());
                                Timber.e("Places Sorted");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        sendBroadcast(false);
                        Timber.e("Getting Distance Matrix On Error Response");
                        error.printStackTrace();
                    }
                }
        )
        {
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Accept", "application/json");
                headers.put("Authorization", getString(R.string.ORS_API_KEY));
                return headers;
            }
        };

        requestQueue.add(objectRequest);

    }

    private void sendBroadcast(boolean success){
        sendBroadcast(null,success);
    }

    private void sendBroadcast (List<Place> sortedList,boolean success){
        Intent intent = new Intent (SORT_TRIP_RESULT); //put the same message as in the filter you used in the activity when registering the receiver
        Bundle bundle = new Bundle();
        bundle.putBoolean(KEY_SORT_SUCCESS,success);
        if (sortedList!=null){
            bundle.putParcelableArrayList(
                    KEY_SORT_PLACE_LIST,
                    new ArrayList<>(sortedList)
            );
        }
        intent.putExtras(bundle);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

}
