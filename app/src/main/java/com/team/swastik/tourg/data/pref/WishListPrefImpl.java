package com.team.swastik.tourg.data.pref;

import android.content.SharedPreferences;

import com.team.swastik.tourg.base.BasePreference;

import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;

public class WishListPrefImpl extends BasePreference implements WishListPreference {

    @Inject
    public WishListPrefImpl(SharedPreferences mSharedPref) {
        super(mSharedPref);
    }

    @Override
    public void setWishList(Set<String> wishList) {
        getSharedPref().edit().putStringSet(PREF_KEY_WISH_LIST,wishList).apply();
    }

    @Override
    public Set<String> getWishList() {
        return getSharedPref().getStringSet(PREF_KEY_WISH_LIST, new HashSet<>());
    }

    @Override
    public boolean addToWishList(String placeId) {
        Set<String> wishList = new HashSet<>(getWishList()); //Creating a copy of wishList because we cannot directly alter the StringSet
        boolean added = wishList.add(placeId); //Adding placeId to the Set and getting added boolean
        setWishList(wishList); // Setting new wishList to pref
        return added;
    }

    @Override
    public boolean removeFromWishList(String placeId) {
        Set<String> wishList = new HashSet<>(getWishList()); //Creating a copy of wishList because we cannot directly alter the StringSet
        boolean removed = wishList.remove(placeId); //Removing placeId from the Set and getting removed boolean
        setWishList(wishList); // Setting new wishList to pref
        return removed;
    }

    @Override
    public void setCurrentTripId(Long tripId) {
        getSharedPref().edit().putLong(PREF_KEY_TRIP_ID,tripId).apply();
    }

    @Override
    public Long getCurrentTripId() {
        return getSharedPref().getLong(PREF_KEY_TRIP_ID,0);
    }

    @Override
    public void setTripOnProcess(boolean onProcess) {
        getSharedPref().edit().putBoolean(PREF_KEY_TRIP_PROCESS,onProcess).apply();
    }

    @Override
    public boolean getTripOnProcess() {
        return getSharedPref().getBoolean(PREF_KEY_TRIP_PROCESS,false);
    }

    @Override
    public void setLat(double lat) {
        getSharedPref().edit().putFloat(PREF_KEY_LAT,(float) lat).apply();
    }

    @Override
    public double getLat() {
        return getSharedPref().getFloat(PREF_KEY_LAT,(float) 6.9630527);
    }

    @Override
    public void setLng(double lng) {
        getSharedPref().edit().putFloat(PREF_KEY_LNG,(float) lng).apply();
    }

    @Override
    public double getLng() {
        return getSharedPref().getFloat(PREF_KEY_LNG,(float) 79.9213102);
    }
}
