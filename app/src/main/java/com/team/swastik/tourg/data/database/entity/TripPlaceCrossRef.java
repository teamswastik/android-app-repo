package com.team.swastik.tourg.data.database.entity;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;

import org.jetbrains.annotations.NotNull;

@Entity(primaryKeys = {"tripId","placeId"},indices = {@Index(value = "placeId")})
public class TripPlaceCrossRef {
    @NonNull
    private Long tripId;
    @NonNull
    private String placeId;

    public TripPlaceCrossRef() {
    }

    @Ignore
    public TripPlaceCrossRef(@NonNull Long tripId, @NonNull String placeId) {
        this.tripId = tripId;
        this.placeId = placeId;
    }

    @NotNull
    public Long getTripId() {
        return tripId;
    }

    public void setTripId(@NotNull Long tripId) {
        this.tripId = tripId;
    }

    @NotNull
    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(@NotNull String placeId) {
        this.placeId = placeId;
    }
}
