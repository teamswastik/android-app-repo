package com.team.swastik.tourg.ui.fragment.selectPlan;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.team.swastik.tourg.R;
import com.team.swastik.tourg.model.enums.District;

public class ChoosePlanDialog extends DialogFragment {

    private IPlanChoiceListener listener;
    private final District district;

    public ChoosePlanDialog(IPlanChoiceListener listener, District district) {
        this.listener = listener;
        this.district = district;
    }

    public interface IPlanChoiceListener{
        void onAutoPlan(District district);
        void onManualPlan(District district);
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("How to plan?" )
                .setPositiveButton("Auto Plan", (DialogInterface.OnClickListener) (dialog, id) -> {
                    dismiss();
                    listener.onAutoPlan(district);
                })
                .setNegativeButton("Let me plan", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dismiss();
                        listener.onManualPlan(district);

                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}
