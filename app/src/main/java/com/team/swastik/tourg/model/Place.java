package com.team.swastik.tourg.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.team.swastik.tourg.model.enums.AvailableType;
import com.team.swastik.tourg.model.enums.District;
import com.team.swastik.tourg.model.enums.PlaceCategory;

import java.util.List;

@Entity(
        tableName = "place"
)
public class Place implements Parcelable {
    @PrimaryKey
    @NonNull
    private String id;
    private String name;
    private String desc;
    private double latitude;
    private double longitude;
    @Embedded
    private Address address;
    private boolean isActive;
    @Ignore
    private PlaceCategory placeCategory;
    @Ignore
    private boolean onWishList;
    private boolean touristPlace;
    private String imageUrl;
    private String createdAt;
    private String updatedAt;
    @Ignore
    private AvailableType availableType;
    private int popularity;
    private int avgViewTime;
    private double rating;
    private double closingTime;
    private double openingTime;

    @Ignore
    private List<String> gallery;

    public Place() {
    }

    @Ignore
    public Place(@NonNull String id) {
        this.id = id;
    }

    public Place(Parcel in) {
        id = in.readString();
        name = in.readString();
        desc = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
        address = in.readParcelable(Address.class.getClassLoader());
        isActive = in.readByte() != 0;
        placeCategory = (PlaceCategory) in.readSerializable();
        onWishList = in.readByte() != 0;
        touristPlace = in.readByte() != 0;
        imageUrl = in.readString();
        createdAt = in.readString();
        updatedAt = in.readString();
        availableType = (AvailableType) in.readSerializable();
        popularity = in.readInt();
        avgViewTime = in.readInt();
        rating = in.readDouble();
        closingTime = in.readDouble();
        openingTime = in.readDouble();
    }

    @Ignore
    public Place(@NonNull String id, String name, String desc, double longitude, double latitude, int avgViewTime, double rating) {
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.longitude = longitude;
        this.latitude = latitude;
        this.avgViewTime = avgViewTime;
        this.rating = rating;
    }

    @Ignore
    public Place(@NonNull String id, String name, String desc, double longitude, double latitude, int avgViewTime, double rating, String imgUrl) {
        this(id, name, desc, longitude, latitude, avgViewTime, rating);
        this.imageUrl = imgUrl;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public PlaceCategory getPlaceCategory() {
        return placeCategory;
    }

    public void setPlaceCategory(PlaceCategory placeCategory) {
        this.placeCategory = placeCategory;
    }

    public boolean isTouristPlace() {
        return touristPlace;
    }

    public void setTouristPlace(boolean touristPlace) {
        this.touristPlace = touristPlace;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public AvailableType getAvailableType() {
        return availableType;
    }

    public void setAvailableType(AvailableType availableType) {
        this.availableType = availableType;
    }

    public int getAvgViewTime() {
        return avgViewTime;
    }

    public void setAvgViewTime(int avgViewTime) {
        this.avgViewTime = avgViewTime;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getPopularity() {
        return popularity;
    }

    public void setPopularity(int popularity) {
        this.popularity = popularity;
    }

    public boolean isOnWishList() {
        return onWishList;
    }

    public void setOnWishList(boolean onWishList) {
        this.onWishList = onWishList;
    }

    public double getClosingTime() {
        return closingTime;
    }

    public void setClosingTime(double closingTime) {
        this.closingTime = closingTime;
    }

    public double getOpeningTime() {
        return openingTime;
    }

    public void setOpeningTime(double openingTime) {
        this.openingTime = openingTime;
    }


    public List<String> getGallery() {
        return gallery;
    }

    public void setGallery(List<String> gallery) {
        this.gallery = gallery;
    }

    @Override
    public String toString() {
        return "Place{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(desc);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeParcelable(address, flags);
        dest.writeByte((byte) (isActive ? 1 : 0));
        dest.writeSerializable(placeCategory);
        dest.writeByte((byte) (onWishList ? 1 : 0));
        dest.writeByte((byte) (touristPlace ? 1 : 0));
        dest.writeString(imageUrl);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
        dest.writeSerializable(availableType);
        dest.writeInt(popularity);
        dest.writeInt(avgViewTime);
        dest.writeDouble(rating);
        dest.writeDouble(closingTime);
        dest.writeDouble(openingTime);
    }

    public static final Parcelable.Creator<Place> CREATOR = new Parcelable.Creator<Place>() {
        public Place createFromParcel(Parcel in) {
            return new Place(in);
        }

        public Place[] newArray(int size) {
            return new Place[size];
        }
    };
}
