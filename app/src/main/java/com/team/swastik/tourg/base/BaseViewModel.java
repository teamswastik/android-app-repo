package com.team.swastik.tourg.base;

import androidx.lifecycle.ViewModel;

import io.reactivex.disposables.CompositeDisposable;
import timber.log.Timber;

public abstract class BaseViewModel extends ViewModel {
    private CompositeDisposable mCompositeDisposable;


    public BaseViewModel() {
        mCompositeDisposable= new CompositeDisposable();
    }

    @Override
    protected void onCleared() {
        mCompositeDisposable.dispose();
        Timber.e("Composite Disposable cleared!");
        super.onCleared();
    }

    public CompositeDisposable getCompositeDisposable() {
        return mCompositeDisposable;
    }
}
