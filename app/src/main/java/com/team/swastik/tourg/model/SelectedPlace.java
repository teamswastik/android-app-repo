package com.team.swastik.tourg.model;

public class SelectedPlace {
    private final Place place;
    private int priority;
    private int openHrsPriority;


    public SelectedPlace(Place place) {
        this.place = place;
    }

    public Place getPlace() {
        return place;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getOpenHrsPriority() {
        return openHrsPriority;
    }

    public void setOpenHrsPriority(int openHrsPriority) {
        this.openHrsPriority = openHrsPriority;
    }
}
