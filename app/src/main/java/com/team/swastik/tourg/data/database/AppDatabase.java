package com.team.swastik.tourg.data.database;


import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.team.swastik.tourg.data.database.dao.PlaceDao;
import com.team.swastik.tourg.data.database.dao.TripDao;
import com.team.swastik.tourg.data.database.dao.UserDao;
import com.team.swastik.tourg.data.database.entity.TripPlaceCrossRef;
import com.team.swastik.tourg.data.database.entity.TripWithPlaces;
import com.team.swastik.tourg.data.database.typeconverter.MainConverter;
import com.team.swastik.tourg.model.Place;
import com.team.swastik.tourg.model.Trip;

@Database(
        entities = {
                Place.class,
                Trip.class,
                TripPlaceCrossRef.class
        },
        version = 1,
        exportSchema = true
)
@TypeConverters({MainConverter.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract UserDao userDao();
    public abstract PlaceDao placeDao();
    public abstract TripDao tripDao();
}