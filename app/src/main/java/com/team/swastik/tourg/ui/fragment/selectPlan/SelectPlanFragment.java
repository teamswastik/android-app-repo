package com.team.swastik.tourg.ui.fragment.selectPlan;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.team.swastik.tourg.R;
import com.team.swastik.tourg.base.BaseActivity;
import com.team.swastik.tourg.base.BaseFragment;
import com.team.swastik.tourg.databinding.FragmentSelectPlanBinding;
import com.team.swastik.tourg.databinding.ItemHomePlaceSliderBinding;
import com.team.swastik.tourg.di.ViewModelFactory;
import com.team.swastik.tourg.model.Place;
import com.team.swastik.tourg.model.enums.District;
import com.team.swastik.tourg.ui.activity.MainViewModel;
import com.team.swastik.tourg.ui.fragment.home.PlaceAdapter;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

public class SelectPlanFragment extends BaseFragment<FragmentSelectPlanBinding> implements ChoosePlanDialog.IPlanChoiceListener {

    @Inject
    ViewModelFactory viewModelFactory;
    private SelectPlanViewModel mSelectPlanViewModel;
    private BaseActivity mActivityContext;
    private MainViewModel mMainViewModel;

    @Override
    public int getLayoutId() {
        return R.layout.fragment_select_plan;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mActivityContext = (BaseActivity) getActivity();
        initDaggerAndViewModel();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setListeners(view);
        setObservers();
        initDistrictRecycler(Arrays.asList(District.values()));
    }

    private void setListeners(View view) {
        getBinding().imgViewBack.setOnClickListener(v -> {
            Navigation.findNavController(getBinding().getRoot()).navigateUp();
        });
    }

    private void setObservers() {
    }


    private void initDistrictRecycler(List<District> districts){
        RecyclerView recyclerView = getBinding().recyclerViewDistrict;
        recyclerView.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(gridLayoutManager);
        DistrictAdapter districtAdapter = new DistrictAdapter(getContext(),districts);
        districtAdapter.setListener((district, position) -> {
            /*ChoosePlanDialog dialog = new ChoosePlanDialog(this,district);
            dialog.showNow(getChildFragmentManager(),"Choose plan");*/
            SelectPlanFragmentDirections.ActionSelectPlanFragmentToSelectPlacesFragment action = SelectPlanFragmentDirections.actionSelectPlanFragmentToSelectPlacesFragment(district.getId());
            Navigation.findNavController(getBinding().getRoot()).navigate(action);
        });
        recyclerView.setAdapter(districtAdapter);

    }

    public void onNothingSelected(AdapterView<?> parent) {
    }

    private void setPlacesData() {

        //Get all places data for selected district from database, Change after implement database code
        List<Place> allTouristPlaces = GetDataFromDataBaseAllTouristPlaces(mMainViewModel.getSelectedDistrictId());

        mMainViewModel.setAllTouristPlaces(allTouristPlaces);
    }

    private void initDaggerAndViewModel() {
        AndroidSupportInjection.inject(this);
        // ViewModelProvider viewModelProvider = new ViewModelProvider(this, viewModelFactory);
        mSelectPlanViewModel = ViewModelProviders.of(this, viewModelFactory).get(SelectPlanViewModel.class);
        mMainViewModel = ViewModelProviders.of(mActivityContext, viewModelFactory).get(MainViewModel.class);
    }

    public List<Place> GetDataFromDataBaseAllTouristPlaces(int selectedDistrictId){
        List<Place> allPlaces = new ArrayList<Place>();
        switch (selectedDistrictId){
            case 0:
                allPlaces.add(new Place("place001", "Jaffna Fort", "desc Jaffna Fort", 80.0062365, 9.662036,3, 70));
                allPlaces.add(new Place("place002", "Kankesanthurai Beach", "desc Kankesanthurai Beach", 80.0452713, 9.8159948,4, 80));
                allPlaces.add(new Place("place003", "Jaffna Library", "desc Jaffna Library", 80.0097038, 9.6620852,2, 60));
                break;
            case 8:
                allPlaces.add(new Place("place001", "Galle Face", "desc Galle Face", 79.8420486, 6.9261046,4, 75));
                allPlaces.add(new Place("place002", "Majestic City", "desc Majestic City", 79.8525224, 6.8939599,6, 70));
                allPlaces.add(new Place("place003", "Colombo City Centre", "desc Colombo City Centre", 79.8532242, 6.9176167,6, 80));
                allPlaces.add(new Place("place004", "Gangaramaya Park", "desc Gangaramaya Park", 79.8511183, 6.9176742,3, 60));
                allPlaces.add(new Place("place005", "Mount Laviniya Beach", "desc Mount Laviniya Beach", 79.8537397, 6.8406715,3, 65));
                allPlaces.add(new Place("place006", "One Galle Face Mall", "desc One Galle Face Mall", 79.8518387, 6.9199384,6, 85));
                break;
            default:
                allPlaces.add(new Place("place001", "Colombo", "desc Colombo", 79.8732959, 6.8936223,10, 75));
                allPlaces.add(new Place("place002", "Jaffna", "desc Jaffna", 80.010917, 9.6699994,6, 50));
                allPlaces.add(new Place("place003", "Kandy", "desc Kandy", 80.5907617, 7.2946291,16, 80));
                allPlaces.add(new Place("place004", "Galle", "desc Galle", 80.1769773, 6.0559758,14, 70));
                allPlaces.add(new Place("place005", "Trincomalee", "desc Trincomalee", 81.1781969, 8.5832926,12, 65));
                allPlaces.add(new Place("place006", "Nuwara Eliya", "desc Nuwara Eliya", 80.7651007, 6.9718651,20, 90));
                allPlaces.add(new Place("place007", "Anuradhapura", "desc Anuradhapura", 80.4086475, 8.3444248,8, 50));
                allPlaces.add(new Place("place008", "Mannar", "desc Mannar", 79.8885017, 8.974539,6, 40));
                allPlaces.add(new Place("place009", "Batticaloe", "desc Batticaloe", 81.5296891, 7.687641,12,60));
        }
        return allPlaces;
    }

    @Override
    public void onAutoPlan(District district) {

    }

    @Override
    public void onManualPlan(District district) {
        SelectPlanFragmentDirections.ActionSelectPlanFragmentToSelectPlacesFragment action = SelectPlanFragmentDirections.actionSelectPlanFragmentToSelectPlacesFragment(district.getId());
        Navigation.findNavController(getBinding().getRoot()).navigate(action);
    }
}