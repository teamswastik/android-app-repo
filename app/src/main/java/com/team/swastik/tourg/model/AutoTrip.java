package com.team.swastik.tourg.model;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Ignore;

import com.team.swastik.tourg.model.enums.District;

@Entity
public class AutoTrip extends Trip {
    private int tourDays;
    private float budget;
    private boolean accommodate;
    private String transportMedium;

    @Embedded
    private Transportation transportation;
    private Accommodation accommodation;
    @Ignore
    public AutoTrip(int tourDays, float budget, boolean accommodate, String transportMedium, Transportation transportation, Accommodation accommodation) {
        this.tourDays = tourDays;
        this.budget = budget;
        this.accommodate = accommodate;
        this.transportMedium = transportMedium;
        this.transportation = transportation;
        this.accommodation = accommodation;
    }


    public AutoTrip() {
    }

    public int getTourDays() {
        return tourDays;
    }

    public void setTourDays(int tourDays) {
        this.tourDays = tourDays;
    }

    public float getBudget() {
        return budget;
    }

    public void setBudget(float budget) {
        this.budget = budget;
    }

    public boolean isAccommodate() {
        return accommodate;
    }

    public void setAccommodate(boolean accommodate) {
        this.accommodate = accommodate;
    }

    public String getTransportMedium() {
        return transportMedium;
    }

    public void setTransportMedium(String transportMedium) {
        this.transportMedium = transportMedium;
    }

    public Transportation getTransportation() {
        return transportation;
    }

    public void setTransportation(Transportation transportation) {
        this.transportation = transportation;
    }

    public Accommodation getAccommodation() {
        return accommodation;
    }

    public void setAccommodation(Accommodation accommodation) {
        this.accommodation = accommodation;
    }

    @Override
    public String toString() {
        return "AutoTrip{" +
                "tourDays=" + tourDays +
                ", budget=" + budget +
                ", accommodate=" + accommodate +
                ", transportMedium='" + transportMedium + '\'' +
                ", transportation=" + transportation +
                ", accommodation=" + accommodation +
                '}';
    }
}
