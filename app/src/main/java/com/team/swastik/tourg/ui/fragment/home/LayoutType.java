package com.team.swastik.tourg.ui.fragment.home;

import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.HashMap;
import java.util.Map;

public enum LayoutType {
    LIST_HORIZONTAL(1, LinearLayoutManager.HORIZONTAL),
    LIST_VERTICAL(2,LinearLayoutManager.VERTICAL),
    GRID_HORIZONTAL(3,LinearLayoutManager.HORIZONTAL),
    GRID_VERTICAL(4,LinearLayoutManager.VERTICAL);
    private final int mValue;
    private final int mScroll;
    private static Map map = new HashMap<>();

    private LayoutType(int value,int scroll) {
        this.mValue = value;
        this.mScroll = scroll;
    }

    static {
        for (LayoutType layoutType : LayoutType.values()) {
            map.put(layoutType.getValue(), layoutType);
        }
    }

    public static LayoutType valueOf(int pageType) {
        return (LayoutType) map.get(pageType);
    }

    public int getValue() {
        return mValue;
    }

    public int getScroll() {
        return mScroll;
    }

    @Override
    public String toString() {
        return "LayoutType{" +
                "mValue=" + mValue +
                ", mScroll=" + mScroll +
                '}';
    }
}
