package com.team.swastik.tourg.base;

import android.content.SharedPreferences;

public abstract class BasePreference {
    private final SharedPreferences mSharedPref;

    public BasePreference(SharedPreferences mSharedPref) {
        this.mSharedPref = mSharedPref;
    }

    public SharedPreferences getSharedPref() {
        return mSharedPref;
    }
}
