package com.team.swastik.tourg.ui.fragment.selectPlaces;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.team.swastik.tourg.R;
import com.team.swastik.tourg.base.BaseFragment;
import com.team.swastik.tourg.databinding.FragmentSelectPlacesBinding;
import com.team.swastik.tourg.di.ViewModelFactory;
import com.team.swastik.tourg.model.Place;
import com.team.swastik.tourg.model.enums.District;
import com.team.swastik.tourg.service.OutputReceiver;
import com.team.swastik.tourg.ui.activity.MainViewModel;
import com.team.swastik.tourg.ui.fragment.places.PlacesFragmentArgs;
import com.team.swastik.tourg.ui.fragment.plan.PlanFragment;

import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;
import timber.log.Timber;

public class SelectPlacesFragment extends BaseFragment<FragmentSelectPlacesBinding> {

    @Inject
    ViewModelFactory viewModelFactory;
    private  MainViewModel mMainViewModel;
    private SelectPlacesViewModel viewModel;
    private District mDistrict;

    private PlaceByDistrictAdapter mAdapter;


    @Override
    public int getLayoutId() {
        return R.layout.fragment_select_places;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        initDaggerAndViewModel();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments()!=null){
            int districtId = SelectPlacesFragmentArgs.fromBundle(getArguments()).getDistrictId();
            mDistrict = District.getDistrictById(districtId);
            //viewModel.getPlaceByDistrict(districtById);
            getBinding().txtViewTitle.setText(mDistrict.getName());
        }
        setListeners();
        setObservers();
    }

    private void setListeners(){
        getBinding().imgViewBack.setOnClickListener(v -> {
            Navigation.findNavController(getBinding().getRoot()).navigateUp();
        });

        getBinding().btnMakePlan.setOnClickListener(v -> {
            if (mAdapter==null){
                Toast.makeText(getBaseActivity(), "No adapter attached", Toast.LENGTH_SHORT).show();
                return;
            }
            Set<Place> selectedList = mAdapter.getSelectedList();
            Timber.e("Selected list : ");
            Timber.e(selectedList.toString());
            if (selectedList.size()==0){
                Toast.makeText(getBaseActivity(), "Please select at least one place to make trip", Toast.LENGTH_SHORT).show();
                return;
            }

            mMainViewModel.setSelectedList(selectedList);
            Timber.e("Total Selected : " +mMainViewModel.getSelectedList().size());
            // navigate confirm page
            Navigation.findNavController(getBinding().getRoot()).navigate(SelectPlacesFragmentDirections.actionSelectPlacesFragmentToPlanFragment());
        });

    }

    private void setObservers(){
       /* viewModel.getPlaceByDistrictLiveData(mDistrict).observe(this.getViewLifecycleOwner(), places -> {
            if (places!=null){
                initRecyclerView(places);
            }

        });*/

        viewModel.getPlacesByDistrictAndTouristPlace(mDistrict,true).observe(this.getViewLifecycleOwner(), places -> {
            if (places!=null){
                initRecyclerView(places);
            }
        });

    }


    private void initRecyclerView(List<Place> places) {
        RecyclerView recyclerView = getBinding().recyclerViewPlaces;
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity(),LinearLayoutManager.VERTICAL,false));
        mAdapter = new PlaceByDistrictAdapter(getBaseActivity(), places);
        recyclerView.setAdapter(mAdapter);
    }


/*    public void StartShortService(){
        OutputReceiver receiver = new OutputReceiver(new PlanFragment.OutputList());
        Intent intent = new Intent(getActivity(), com.team.swastik.tourg.service.ShortService.class);
        intent.putExtra("allPlaces", selectedPlacesId());
        intent.putExtra("allLocations", selectedPlacesLocation());
        intent.putExtra("allAvgViewTime", selectedPlacesAvgViewTime());
        intent.putExtra("receiver", receiver);
        intent.putExtra("type", "PlanTrip");
        getActivity().startService(intent);
    }*/

    public String[] selectedPlacesId(){
        List<Place> selectedTouristPlaces = mMainViewModel.getSelectedTouristPlaces();
        String[] selectedPlacesId = new String[selectedTouristPlaces.size()+1];
        selectedPlacesId[0] = String.valueOf(mMainViewModel.getSelectedDistrictId());
        for(int i=1; i<selectedPlacesId.length; i++){
            selectedPlacesId[i]=selectedTouristPlaces.get(i-1).getId();
        }
        return selectedPlacesId;
    }

    public double[][] selectedPlacesLocation(){
        List<Place> selectedTouristPlaces = mMainViewModel.getSelectedTouristPlaces();
        double[][] selectedPlacesLocation = new double[selectedTouristPlaces.size()+1][2];
        selectedPlacesLocation[0][0] = mMainViewModel.getSelectedDistrictlng();
        selectedPlacesLocation[0][1] = mMainViewModel.getSelectedDistrictlat();
        for(int i=1; i<selectedPlacesLocation.length; i++){
            selectedPlacesLocation[i][0] = selectedTouristPlaces.get(i-1).getLongitude();
            selectedPlacesLocation[i][1] = selectedTouristPlaces.get(i-1).getLatitude();
        }
        return selectedPlacesLocation;
    }

    public int[] selectedPlacesAvgViewTime(){
        List<Place> selectedTouristPlaces = mMainViewModel.getSelectedTouristPlaces();
        int[] selectedPlacesAvgViewTime = new int[selectedTouristPlaces.size()+1];
        selectedPlacesAvgViewTime[0] = 0;
        for(int i=1; i<selectedPlacesAvgViewTime.length; i++){
            selectedPlacesAvgViewTime[i]=selectedTouristPlaces.get(i-1).getAvgViewTime();
        }
        return selectedPlacesAvgViewTime;
    }

    private void initDaggerAndViewModel() {
        AndroidSupportInjection.inject(this);
        // ViewModelProvider viewModelProvider = new ViewModelProvider(this, viewModelFactory);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(SelectPlacesViewModel.class);
        mMainViewModel = ViewModelProviders.of(getBaseActivity(), viewModelFactory).get(MainViewModel.class);
    }
}