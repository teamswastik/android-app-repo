package com.team.swastik.tourg.di.module;


import androidx.lifecycle.ViewModel;

import com.team.swastik.tourg.di.ViewModelKey;
import com.team.swastik.tourg.ui.activity.MainViewModel;
import com.team.swastik.tourg.ui.activity.StartTrip;
import com.team.swastik.tourg.ui.activity.StartTripViewModel;
import com.team.swastik.tourg.ui.fragment.home.HomeViewModel;
import com.team.swastik.tourg.ui.fragment.myTrip.MyTripViewModel;
import com.team.swastik.tourg.ui.fragment.places.PlacesViewModel;
import com.team.swastik.tourg.ui.fragment.plan.PlanViewModel;
import com.team.swastik.tourg.ui.fragment.search.SearchViewModel;
import com.team.swastik.tourg.ui.fragment.selectPlaces.SelectPlacesViewModel;
import com.team.swastik.tourg.ui.fragment.selectPlan.SelectPlanViewModel;
import com.team.swastik.tourg.ui.fragment.tripBasicDetails.TripBasicDetailsViewModel;
import com.team.swastik.tourg.ui.fragment.wishList.WishListViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @IntoMap
    @Binds
    @ViewModelKey(MainViewModel.class)
    abstract ViewModel bindMainViewModel(MainViewModel viewModel);

    @IntoMap
    @Binds
    @ViewModelKey(HomeViewModel.class)
    abstract ViewModel bindHomeViewModel(HomeViewModel viewModel);

    @IntoMap
    @Binds
    @ViewModelKey(MyTripViewModel.class)
    abstract ViewModel bindMyTripViewModel(MyTripViewModel viewModel);

    @IntoMap
    @Binds
    @ViewModelKey(PlacesViewModel.class)
    abstract ViewModel bindPlacesViewModel(PlacesViewModel viewModel);

    @IntoMap
    @Binds
    @ViewModelKey(PlanViewModel.class)
    abstract ViewModel bindPlanViewModel(PlanViewModel viewModel);

    @IntoMap
    @Binds
    @ViewModelKey(SearchViewModel.class)
    abstract ViewModel bindSearchViewModel(SearchViewModel viewModel);

    @IntoMap
    @Binds
    @ViewModelKey(SelectPlacesViewModel.class)
    abstract ViewModel bindSelectPlacesViewModel(SelectPlacesViewModel viewModel);

    @IntoMap
    @Binds
    @ViewModelKey(SelectPlanViewModel.class)
    abstract ViewModel bindSelectPlanViewModel(SelectPlanViewModel viewModel);

    @IntoMap
    @Binds
    @ViewModelKey(TripBasicDetailsViewModel.class)
    abstract ViewModel bindTripBasicDetailsViewModel(TripBasicDetailsViewModel viewModel);

    @IntoMap
    @Binds
    @ViewModelKey(WishListViewModel.class)
    abstract ViewModel bindWishListViewModel(WishListViewModel viewModel);

    @IntoMap
    @Binds
    @ViewModelKey(StartTripViewModel.class)
    abstract ViewModel bindStartTripViewModel(StartTripViewModel viewModel);

}
