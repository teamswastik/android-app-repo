package com.team.swastik.tourg.repository.user;

import com.team.swastik.tourg.base.BaseRepository;
import com.team.swastik.tourg.data.database.AppDatabase;
import com.team.swastik.tourg.data.pref.PreferenceHelper;

import javax.inject.Inject;

public class UserRepositoryImpl extends BaseRepository implements UserRepository {

    @Inject
    public UserRepositoryImpl(AppDatabase database, PreferenceHelper preferenceHelper) {
        super(database,preferenceHelper);
    }

    @Override
    public boolean isUserAvailable() {
        return getPrefHelper().isUserAvailable() ;
    }

    @Override
    public void setUserAvailable(boolean available) {
        getPrefHelper().setUserAvailable(available);
    }
}
