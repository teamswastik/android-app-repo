package com.team.swastik.tourg.base;

import com.team.swastik.tourg.data.database.AppDatabase;
import com.team.swastik.tourg.data.pref.PreferenceHelper;

import retrofit2.Retrofit;

public abstract class BaseRepository {
    private final AppDatabase mAppDatabase;
    private final PreferenceHelper mPrefHelper;

    public BaseRepository(AppDatabase appDatabase,PreferenceHelper preferenceHelper) {
        this.mAppDatabase = appDatabase;
        this.mPrefHelper=  preferenceHelper;
    }

    public AppDatabase getAppDatabase() {
        return mAppDatabase;
    }

    public PreferenceHelper getPrefHelper() {
        return mPrefHelper;
    }
}
