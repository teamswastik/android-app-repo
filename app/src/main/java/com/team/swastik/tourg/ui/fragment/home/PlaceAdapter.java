package com.team.swastik.tourg.ui.fragment.home;

import android.content.Context;

import androidx.databinding.ViewDataBinding;

import com.team.swastik.tourg.base.BaseRecyclerViewAdapter;
import com.team.swastik.tourg.base.listeners.IPlaceListener;
import com.team.swastik.tourg.model.Place;
import com.team.swastik.tourg.ui.fragment.home.viewholder.PlaceViewHolder;

import java.util.List;

public abstract class PlaceAdapter<D extends ViewDataBinding> extends BaseRecyclerViewAdapter<Place,PlaceViewHolder<D>> {
    private IPlaceListener listener;

    public PlaceAdapter(Context context, List<Place> dataSet) {
        super(context, dataSet);
    }

    public void setListener(IPlaceListener listener) {
        this.listener = listener;
    }

    public IPlaceListener getListener() {
        return listener;
    }
}
