package com.team.swastik.tourg.model.enums;

import androidx.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

public enum District {
    JAFFNA(0,"Jaffna",80.0097799,9.6668384),
    KILINOCHCHI(1,"Kilinochchi",80.0097799,9.6668384),
    MULLAITIVU(2,"Mullaitivu",80.0097799,9.6668384),
    VAVUNIYA(3,"Vavuniya",80.0097799,9.6668384),
    MANNAR(4,"Mannar",80.0097799,9.6668384),
    PUTTALAM(5,"Puttalam",80.0097799,9.6668384),
    KURUNEGALA(6,"Kurunegala",80.0097799,9.6668384),
    GAMPAHA(7,"Gampaha",80.0097799,9.6668384),
    COLOMBO(8,"Colombo",79.8512083,6.9332484),
    KALUTARA(9,"Kalutara",80.0097799,9.6668384),
    ANURADHAPURA(10,"Anuradhapura",80.0097799,9.6668384),
    POLANNARUWA(11,"Polonnaruwa",80.0097799,9.6668384),
    MATALE(12,"Matale",80.0097799,9.6668384),
    KANDY(13,"Kandy",80.0097799,9.6668384),
    NUWARAELIYA(14,"Nuwara Eliya",80.0097799,9.6668384),
    KEGALLE(15,"Kegalle",80.0097799,9.6668384),
    RATNAPURA(16,"Ratnapura",80.0097799,9.6668384),
    TRINCOMALEE(17,"Trincomalee",80.0097799,9.6668384),
    BATTICALOA(18,"Batticaloa",80.0097799,9.6668384),
    AMPARA(19,"Ampara",80.0097799,9.6668384),
    BADULLA(20,"Badulla",80.0097799,9.6668384),
    MONARAGALA(21,"Monaragala",80.0097799,9.6668384),
    HAMBANTOTA(22,"Hambantota",80.0097799,9.6668384),
    MATARA(23,"Matara",80.0097799,9.6668384),
    GALLE(24,"Galle",80.0097799,9.6668384);

    private final int id;
    private final String name;
    private final double lng;
    private final double lat;

    private static Map nameMap = new HashMap<>();
    private static Map lngMap = new HashMap<>();
    private static Map latMap = new HashMap<>();
    private static Map districtMap = new HashMap<>();

    District(int id, String name, double lng, double lat) {
        this.id = id;
        this.name = name;
        this.lat = lat;
        this.lng = lng;
    }

    static {
        for (District district : District.values()) {
            nameMap.put(district.getId(),district.getName());
            lngMap.put(district.getId(),district.getLng());
            latMap.put(district.getId(),district.getLat());
            districtMap.put(district.getId(),district);
        }
    }

    public static String getNameByDistrictId(int districtId) {
        return (String) lngMap.get(districtId);
    }
    public static double getLngByDistrictId(int districtId) {
        return (double)lngMap.get(districtId);
    }
    public static double getLatByDistrictId(int districtId) {
        return (double)latMap.get(districtId);
    }
    public static District getDistrictById(int districtId) {
        return (District)districtMap.get(districtId);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getLng() {
        return lng;
    }

    public double getLat() {
        return lat;
    }

}
