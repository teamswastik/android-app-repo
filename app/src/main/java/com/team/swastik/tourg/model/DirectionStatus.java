package com.team.swastik.tourg.model;

public class DirectionStatus {
    private String status;

    public DirectionStatus(String status) {
        this.status = status;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "DirectionStatus{" +
                "status='" + status + '\'' +
                '}';
    }
}
