package com.team.swastik.tourg.ui.fragment.myTrip;


import androidx.lifecycle.LiveData;

import com.team.swastik.tourg.base.BaseViewModel;
import com.team.swastik.tourg.data.database.AppDatabase;
import com.team.swastik.tourg.data.database.dao.PlaceDao;
import com.team.swastik.tourg.data.database.dao.TripDao;
import com.team.swastik.tourg.data.database.entity.TripWithPlaces;
import com.team.swastik.tourg.data.pref.WishListPreference;

import java.util.List;

import javax.inject.Inject;

public class MyTripViewModel extends BaseViewModel {
    private AppDatabase appDatabase;
    private PlaceDao placeDao;
    private TripDao tripDao;

    private WishListPreference wishListPreference;


    @Inject
    public MyTripViewModel(AppDatabase appDatabase, WishListPreference wishListPreference) {
        this.appDatabase=appDatabase;
        this.wishListPreference =wishListPreference;
        this.placeDao = appDatabase.placeDao();
        this.tripDao = appDatabase.tripDao();
    }

    public LiveData<List<TripWithPlaces>> getTripWithPlaces(){
        return tripDao.getTripWithPlaces();
    }

    public void saveTripToPref(long tripId){
        wishListPreference.setTripOnProcess(true);
        wishListPreference.setCurrentTripId(tripId);
    }


}