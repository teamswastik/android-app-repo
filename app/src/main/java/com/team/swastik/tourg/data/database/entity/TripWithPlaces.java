package com.team.swastik.tourg.data.database.entity;

import androidx.room.Embedded;
import androidx.room.Junction;
import androidx.room.Relation;

import com.team.swastik.tourg.model.Place;
import com.team.swastik.tourg.model.Trip;

import java.util.List;

public class TripWithPlaces {
    @Embedded
    public Trip trip;
    @Relation(
            parentColumn = "id",
            entityColumn = "id",
            associateBy = @Junction(
                    value = TripPlaceCrossRef.class,
                    parentColumn = "tripId",
                    entityColumn = "placeId"
            )
    )
    public List<Place> places;


}
