package com.team.swastik.tourg.ui.fragment.selectPlan;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;

import com.team.swastik.tourg.R;
import com.team.swastik.tourg.base.BaseRecyclerViewAdapter;
import com.team.swastik.tourg.databinding.ItemDistrictBinding;
import com.team.swastik.tourg.databinding.ItemHomeMainRecyclerBinding;
import com.team.swastik.tourg.model.enums.District;
import com.team.swastik.tourg.ui.fragment.home.viewholder.RecyclerViewHolder;

import java.util.List;

public class DistrictAdapter extends BaseRecyclerViewAdapter<District,DistrictViewHolder> {
    private IDistrictListener listener;

    public interface IDistrictListener {
        void onDistrictClicked(District district,int position);
    }

    public DistrictAdapter(Context context, List<District> dataSet) {
        super(context, dataSet);
    }

    @Override
    public void bindData(DistrictViewHolder holder, District model, int position) {
        holder.setDistrictName(model.getName());
        holder.getBinding().root.setOnClickListener(v -> {
            if (listener!=null){
                listener.onDistrictClicked(model,position);
            }
        });
    }

    @Override
    public DistrictViewHolder createViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        ItemDistrictBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_district, parent, false);
        return new DistrictViewHolder(binding);
    }

    @Override
    public void onItemClick(District model, int position) {

    }

    public void setListener(IDistrictListener listener) {
        this.listener = listener;
    }
}
