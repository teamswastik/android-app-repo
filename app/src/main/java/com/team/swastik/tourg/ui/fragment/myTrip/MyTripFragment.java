package com.team.swastik.tourg.ui.fragment.myTrip;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.team.swastik.tourg.R;
import com.team.swastik.tourg.base.BaseActivity;
import com.team.swastik.tourg.base.BaseFragment;
import com.team.swastik.tourg.data.database.entity.TripWithPlaces;
import com.team.swastik.tourg.databinding.FragmentMyTripBinding;
import com.team.swastik.tourg.di.ViewModelFactory;
import com.team.swastik.tourg.model.Place;
import com.team.swastik.tourg.service.SortTripService;
import com.team.swastik.tourg.service.TripForegroundService;
import com.team.swastik.tourg.ui.activity.MainViewModel;
import com.team.swastik.tourg.ui.activity.StartTrip;
import com.team.swastik.tourg.ui.fragment.plan.ShortedPlacesAdapter;

import java.util.List;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;
import timber.log.Timber;

public class MyTripFragment extends BaseFragment<FragmentMyTripBinding> implements TripWithPlacesAdapter.ITripListener {

    @Inject
    ViewModelFactory viewModelFactory;

    private MyTripViewModel mViewModel;
    private MainViewModel mMainViewModel;

    private ProgressDialog dialog;
    private BroadcastReceiver bReceiver = new BroadcastReceiver(){

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(SortTripService.SORT_TRIP_RESULT)){
                if (mViewModel!=null && intent.getExtras()!=null){
                    Bundle bundle = intent.getExtras();
                    if (bundle.getBoolean(SortTripService.KEY_SORT_SUCCESS)){
                        List<Place> parcelableArrayList = bundle.getParcelableArrayList(SortTripService.KEY_SORT_PLACE_LIST);
                        Timber.e("OnReceivedbrodcast : "+parcelableArrayList.size());
                    //    mViewModel.setSortedList(parcelableArrayList);
                    }else {
                        Toast.makeText(context, "Sorting Failed", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        }

    };

    @Override
    public int getLayoutId() {
        return R.layout.fragment_my_trip;
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        initDaggerAndViewModel();
        dialog= new ProgressDialog(getBaseActivity());
        return super.onCreateView(inflater,container,savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setListeners();
        setObservers();
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getBaseActivity()).registerReceiver(bReceiver, new IntentFilter(SortTripService.SORT_TRIP_RESULT));
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getBaseActivity()).unregisterReceiver(bReceiver);
    }

    private void setListeners(){

        getBinding().btnStartService.setOnClickListener(v -> {
            Intent intent = new Intent(getBaseActivity(), TripForegroundService.class);
            intent.setAction(TripForegroundService.ACTION_START_FOREGROUND_SERVICE);
            getBaseActivity().startService(intent);

        });

        getBinding().btnStopService.setOnClickListener(v -> {
            Intent intent = new Intent(getBaseActivity(), TripForegroundService.class);
            intent.setAction(TripForegroundService.ACTION_STOP_FOREGROUND_SERVICE);
            getBaseActivity().startService(intent);
        });

    }

    private void setObservers(){
        mViewModel.getTripWithPlaces().observe(this.getViewLifecycleOwner(), tripWithPlaces -> {
            if (tripWithPlaces!=null){
                initRecyclerView(tripWithPlaces);
            }
        });

    }

    private void initRecyclerView(List<TripWithPlaces> tripWithPlaces) {
        RecyclerView recyclerView = getBinding().recyclerViewTrips;
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity(),LinearLayoutManager.VERTICAL,false));
        TripWithPlacesAdapter adapter = new TripWithPlacesAdapter(getBaseActivity(),tripWithPlaces);
        adapter.setListener(this);
        recyclerView.setAdapter(adapter);
    }

    private void initDaggerAndViewModel() {
        AndroidSupportInjection.inject(this);
        // ViewModelProvider viewModelProvider = new ViewModelProvider(this, viewModelFactory);
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(MyTripViewModel.class);
        mMainViewModel = ViewModelProviders.of(getBaseActivity(), viewModelFactory).get(MainViewModel.class);
    }

    @Override
    public void onTripStarted(TripWithPlaces tripWithPlaces, int position) {
        mViewModel.saveTripToPref(tripWithPlaces.trip.getId());
        Intent intent = new Intent(getBaseActivity(), StartTrip.class);
        startActivity(intent);
        getBaseActivity().finishActivity();
    }
}