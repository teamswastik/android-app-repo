package com.team.swastik.tourg.ui.fragment.search;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.team.swastik.tourg.R;
import com.team.swastik.tourg.base.BaseActivity;
import com.team.swastik.tourg.databinding.FragmentMyTripBinding;
import com.team.swastik.tourg.databinding.FragmentSearchBinding;
import com.team.swastik.tourg.di.ViewModelFactory;
import com.team.swastik.tourg.ui.activity.MainViewModel;
import com.team.swastik.tourg.ui.fragment.myTrip.MyTripViewModel;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

public class SearchFragment extends Fragment {

    @Inject
    ViewModelFactory viewModelFactory;
    private BaseActivity mActivityContext;
    private SearchViewModel mSearchViewModel;
    private MainViewModel mMainViewModel;
    private FragmentSearchBinding mBinding;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mActivityContext = (BaseActivity) getActivity();
        initDaggerAndViewModel();
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_search, container, false);
        mBinding.setLifecycleOwner(this);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setListeners(view);
        setObservers();
    }

    private void setListeners(View view){

    }

    private void setObservers(){

    }

    private void initDaggerAndViewModel() {
        AndroidSupportInjection.inject(this);
        // ViewModelProvider viewModelProvider = new ViewModelProvider(this, viewModelFactory);
        mSearchViewModel = ViewModelProviders.of(this, viewModelFactory).get(SearchViewModel.class);
        mMainViewModel = ViewModelProviders.of(mActivityContext, viewModelFactory).get(MainViewModel.class);
    }
}
