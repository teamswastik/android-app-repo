package com.team.swastik.tourg.algorithm;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.team.swastik.tourg.R;
import com.team.swastik.tourg.model.Place;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Location {

    public static double[][] distanceSort(List<Place> selectedPlaces, double currentLocationLng, double currentLocationLat){

        int count = selectedPlaces.size();
        List<Place> places = new ArrayList<Place>(selectedPlaces);

        double[][] locations = new double[count+1][count+1];

        locations[0][0] = currentLocationLng;
        locations[0][1] = currentLocationLat;
        for(int i=1; i<count+1; i++){
            locations[i][0] = places.get(i-1).getLongitude();
            locations[i][1] = places.get(i-1).getLatitude();
        }

        return locations;

    }


}