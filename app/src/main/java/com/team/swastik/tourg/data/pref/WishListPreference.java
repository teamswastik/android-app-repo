package com.team.swastik.tourg.data.pref;

import java.util.Set;

public interface WishListPreference {
    String PREF_KEY_WISH_LIST = "PREF_KEY_WISH_LIST";
    String PREF_KEY_TRIP_ID="PREF_KEY_TRIP_ID";
    String PREF_KEY_TRIP_PROCESS ="PREF_KEY_TRIP_PROCESS";
    String PREF_KEY_LAT ="PREF_KEY_LAT";
    String PREF_KEY_LNG ="PREF_KEY_LNG";

    void setWishList(Set<String> wishList);
    Set<String> getWishList();
    boolean addToWishList(String placeId);
    boolean removeFromWishList(String placeId);

    void setCurrentTripId(Long tripId);
    Long getCurrentTripId();

    void setTripOnProcess(boolean onProcess);
    boolean getTripOnProcess();

    void setLat(double lat);
    double getLat();

    void setLng(double lng);
    double getLng();
}
