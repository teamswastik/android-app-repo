package com.team.swastik.tourg.ui.fragment.places;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.team.swastik.tourg.base.BaseViewModel;
import com.team.swastik.tourg.model.Place;
import com.team.swastik.tourg.utils.AppConstants;

import javax.inject.Inject;

import timber.log.Timber;

public class PlacesViewModel extends BaseViewModel {
    private MutableLiveData<Place> placeLiveData = new MutableLiveData<>();

    @Inject
    public PlacesViewModel() {

    }


    public LiveData<Place> getPlaceLiveData() {
        return placeLiveData;
    }

    public void getPlaceById(String id){
        DatabaseReference placeRef = FirebaseDatabase.getInstance().getReference(AppConstants.FB_PATH_PLACE);
        DatabaseReference touristPlaceRef = placeRef.child(AppConstants.FB_PATH_TOURIST_PLACE);
        Query query = touristPlaceRef
                .orderByChild("id")
                .equalTo(id);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                for(DataSnapshot postSnapshot: snapshot.getChildren()) {
                    Place place = postSnapshot.getValue(Place.class);
                    if (place!=null) {
                        placeLiveData.setValue(place);
                        break;
                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Timber.e( "Failed to read value."+ error.toException());
            }
        });
    }
}