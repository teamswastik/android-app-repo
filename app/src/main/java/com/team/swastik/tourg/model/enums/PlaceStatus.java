package com.team.swastik.tourg.model.enums;

public enum PlaceStatus {
    OPENED,
    CLOSED,
}
