package com.team.swastik.tourg.data.database.typeconverter;
import java.util.Date;
import androidx.room.TypeConverter;

public class DateConvertor {

    @TypeConverter
    public static Date toDate(Long timestamp) {
        return timestamp == null ? null : new Date(timestamp);
    }

    @TypeConverter
    public static Long toTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }
}
