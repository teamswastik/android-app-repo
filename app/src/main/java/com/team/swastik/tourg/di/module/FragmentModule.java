package com.team.swastik.tourg.di.module;


import com.team.swastik.tourg.ui.fragment.addplace.AddPlaceFragment;
import com.team.swastik.tourg.ui.fragment.home.HomeFragment;
import com.team.swastik.tourg.ui.fragment.myTrip.MyTripFragment;
import com.team.swastik.tourg.ui.fragment.places.PlacesFragment;
import com.team.swastik.tourg.ui.fragment.plan.PlanFragment;
import com.team.swastik.tourg.ui.fragment.search.SearchFragment;
import com.team.swastik.tourg.ui.fragment.selectPlaces.SelectPlacesFragment;
import com.team.swastik.tourg.ui.fragment.selectPlan.SelectPlanFragment;
import com.team.swastik.tourg.ui.fragment.tripBasicDetails.TripBasicDetailsFragment;
import com.team.swastik.tourg.ui.fragment.wishList.WishListFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentModule {

    @ContributesAndroidInjector
    abstract HomeFragment injectorHomeFragment();

    @ContributesAndroidInjector
    abstract MyTripFragment injectorMyTripFragment();

    @ContributesAndroidInjector
    abstract PlacesFragment injectorPlacesFragment();

    @ContributesAndroidInjector
    abstract PlanFragment injectorPlanFragment();

    @ContributesAndroidInjector
    abstract SearchFragment injectorSearchFragment();

    @ContributesAndroidInjector
    abstract SelectPlacesFragment injectorSelectPlacesFragment();

    @ContributesAndroidInjector
    abstract SelectPlanFragment injectorSelectPlanFragment();

    @ContributesAndroidInjector
    abstract TripBasicDetailsFragment injectorTripBasicDetailsFragment();

    @ContributesAndroidInjector
    abstract WishListFragment injectorWishListFragment();

    @ContributesAndroidInjector
    abstract AddPlaceFragment injectorAddPlaceFragment();
}
