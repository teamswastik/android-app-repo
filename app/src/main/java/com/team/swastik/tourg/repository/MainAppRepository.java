package com.team.swastik.tourg.repository;


import androidx.lifecycle.LiveData;

import com.team.swastik.tourg.base.BaseRepository;
import com.team.swastik.tourg.data.database.AppDatabase;
import com.team.swastik.tourg.data.pref.PreferenceHelper;
import com.team.swastik.tourg.model.Place;
import com.team.swastik.tourg.model.enums.District;
import com.team.swastik.tourg.repository.place.PlaceRepository;
import com.team.swastik.tourg.repository.user.UserRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Flowable;

public class MainAppRepository extends BaseRepository implements AppRepository {
    private UserRepository mUserRepository;
    private PlaceRepository mPlaceRepository;

    @Inject
    public MainAppRepository(AppDatabase appDatabase, PreferenceHelper preferenceHelper,UserRepository userRepository,PlaceRepository placeRepository) {
        super(appDatabase, preferenceHelper);
        this.mUserRepository= userRepository;
        this.mPlaceRepository = placeRepository;
    }

    @Override
    public boolean isUserAvailable() {
        return mUserRepository.isUserAvailable();
    }

    @Override
    public void setUserAvailable(boolean available) {
        mUserRepository.setUserAvailable(available);
    }

/*    @Override
    public Flowable<List<Place>> getAllPlaces() {
        return mPlaceRepository.getAllPlaces();
    }*/

    @Override
    public LiveData<List<Place>> getAllPlaces() {
        return mPlaceRepository.getAllPlaces();
    }

    @Override
    public Completable insertPlace(Place place) {
      return  mPlaceRepository.insertPlace(place);
    }

    @Override
    public Completable updatePlace(Place place) {
      return   mPlaceRepository.updatePlace(place);
    }

    @Override
    public Completable deletePlace(Place place) {
       return mPlaceRepository.deletePlace(place);
    }

    @Override
    public void getPlacesFromFirebase() {
        mPlaceRepository.getPlacesFromFirebase();
    }

    @Override
    public void getPlacesByRating(int limit) {
        mPlaceRepository.getPlacesByRating(limit);
    }

    @Override
    public LiveData<List<Place>> getAllTouristPlacesLiveData() {
        return mPlaceRepository.getAllTouristPlacesLiveData();
    }

    @Override
    public LiveData<List<Place>> getPlaceByDistrict(District district) {
        return mPlaceRepository.getPlaceByDistrict(district);
    }
}
