package com.team.swastik.tourg.repository.place;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.team.swastik.tourg.base.BaseRepository;
import com.team.swastik.tourg.data.database.AppDatabase;
import com.team.swastik.tourg.data.database.dao.PlaceDao;
import com.team.swastik.tourg.data.pref.PreferenceHelper;
import com.team.swastik.tourg.model.Place;
import com.team.swastik.tourg.model.enums.District;
import com.team.swastik.tourg.utils.AppConstants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class PlaceRepositoryImpl extends BaseRepository implements PlaceRepository {
    private PlaceDao placeDao;
    private Flowable<List<Place>> allPlaces;
    private LiveData<List<Place>> mAllTouristPlacesLiveData;


    @Inject
    public PlaceRepositoryImpl(AppDatabase appDatabase, PreferenceHelper preferenceHelper) {
        super(appDatabase, preferenceHelper);
        placeDao = appDatabase.placeDao();
        mAllTouristPlacesLiveData = placeDao.getAllPlaces();
    }


 /*   @Override
    public Flowable<List<Place>> getAllPlaces() {
        return allPlaces;
    }*/

    @Override
    public LiveData<List<Place>> getAllPlaces() {
        return null;
    }

    @Override
    public Completable insertPlace(Place place) {
        return placeDao.insert(place)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Completable updatePlace(Place place) {
        return placeDao.update(place)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Completable deletePlace(Place place) {
        return placeDao.delete(place)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    @Override
    public void getPlacesFromFirebase() {
        Timber.e("GetPlacesFrom Database");
        DatabaseReference placeRef = FirebaseDatabase.getInstance().getReference(AppConstants.FB_PATH_PLACE);
        DatabaseReference touristPlaceRef = placeRef.child(AppConstants.FB_PATH_TOURIST_PLACE);
        // Read from the database
        touristPlaceRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Place> places = new ArrayList<>();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Place place = postSnapshot.getValue(Place.class);
                    places.add(place);
                    Timber.e("Looping Place : " + place.getName());
                }

                CompositeDisposable mCompositeDisposable = new CompositeDisposable();
                mCompositeDisposable.add(
                        placeDao.insert(places)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(
                                () -> {
                                    Timber.e("Insert Completed");
                                    mCompositeDisposable.dispose();
                                },
                                throwable -> {
                                    Timber.e("Error Inserting");
                                    Timber.e(throwable.getMessage());
                                    throwable.printStackTrace();
                                    mCompositeDisposable.dispose();
                                }
                        )
                );


            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Timber.e("Failed to read value." + error.toException());
            }
        });

    }

    @Override
    public void getPlacesByRating(int limit) {
        Timber.e("Get Places by rating");
        DatabaseReference placeRef = FirebaseDatabase.getInstance().getReference(AppConstants.FB_PATH_PLACE);
        DatabaseReference touristPlaceRef = placeRef.child(AppConstants.FB_PATH_TOURIST_PLACE);
        Query query = touristPlaceRef
                .orderByChild("rating")
                .startAt(3.5)
                .limitToLast(limit);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Timber.e("Get Places by rating");
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    Place place = postSnapshot.getValue(Place.class);
                    Timber.e("Looping query : " + place.getName());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                // Failed to read value
                Timber.e("Failed to read value." + error.toException());
            }
        });

    }


    @Override
    public LiveData<List<Place>> getAllTouristPlacesLiveData() {
        return this.mAllTouristPlacesLiveData;
    }

    @Override
    public LiveData<List<Place>> getPlaceByDistrict(District district) {
        return placeDao.getPlacesByDistrict(district);
    }


}
