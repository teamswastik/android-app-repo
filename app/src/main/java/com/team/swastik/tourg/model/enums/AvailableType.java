package com.team.swastik.tourg.model.enums;

import java.util.HashMap;
import java.util.Map;

public enum AvailableType {
    ALL_DAY_24X7(0),
    ALL_DAY_SPECIFIC_TIME(1),
    WEEK_DAYS_24X7(2),
    WEEK_DAYS_SPECIFIC_TIME(3),
    WEEK_ENDS_24X7(4),
    WEEK_ENDS_SPECIFIC_TIME(5),
    SPECIFIC_DAYS_24X7(6),
    SPECIFIC_DAYS_SPECIFIC_TIME(6);

    private final int mValue;

    private static Map mMap = new HashMap<>();

    AvailableType(int mValue) {
        this.mValue = mValue;
    }

    static {
        for (AvailableType type  : AvailableType.values()) {
            mMap.put(type.getValue(), type);
        }
    }

    public static AvailableType valueOf(int type) {
        return (AvailableType) mMap.get(type);
    }
    public int getValue() {
        return mValue;
    }

    @Override
    public String toString() {
        return "AvailableType{" +
                "mValue=" + mValue +
                '}';
    }
}
