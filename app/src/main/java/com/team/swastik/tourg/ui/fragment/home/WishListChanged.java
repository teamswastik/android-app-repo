package com.team.swastik.tourg.ui.fragment.home;

public class WishListChanged {
    private String changedId;
    private Action action;
    private Integer placeAdapterPosition;
    private Integer mainAdapterPosition;

    public WishListChanged(String changedId, Action action) {
        this(changedId,action,null);
    }

    public WishListChanged(String changedId, Action action, Integer placeAdapterPosition) {
        this(changedId,action,placeAdapterPosition,null);
    }

    public WishListChanged(String changedId, Action action, Integer placeAdapterPosition, Integer mainAdapterPosition) {
        this.changedId = changedId;
        this.action = action;
        this.placeAdapterPosition = placeAdapterPosition;
        this.mainAdapterPosition = mainAdapterPosition;
    }

    public String getChangedId() {
        return changedId;
    }

    public Action getAction() {
        return action;
    }

    public Integer getPlaceAdapterPosition() {
        return placeAdapterPosition;
    }

    public void setPlaceAdapterPosition(Integer placeAdapterPosition) {
        this.placeAdapterPosition = placeAdapterPosition;
    }

    public Integer getMainAdapterPosition() {
        return mainAdapterPosition;
    }

    public void setMainAdapterPosition(Integer mainAdapterPosition) {
        this.mainAdapterPosition = mainAdapterPosition;
    }

    public static enum Action{
        ADDED,
        REMOVED
    }
}
