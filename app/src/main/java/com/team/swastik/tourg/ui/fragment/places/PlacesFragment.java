package com.team.swastik.tourg.ui.fragment.places;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.team.swastik.tourg.R;
import com.team.swastik.tourg.base.BaseActivity;
import com.team.swastik.tourg.base.BaseFragment;
import com.team.swastik.tourg.databinding.FragmentPlacesBinding;
import com.team.swastik.tourg.di.ViewModelFactory;
import com.team.swastik.tourg.model.Place;
import com.team.swastik.tourg.ui.activity.MainViewModel;
import com.team.swastik.tourg.ui.fragment.home.WishListChanged;
import com.team.swastik.tourg.utils.AppConstants;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;
import timber.log.Timber;

public class PlacesFragment extends BaseFragment<FragmentPlacesBinding>  {

    @Inject
    ViewModelFactory viewModelFactory;
    private MainViewModel mMainViewModel;
    private PlacesViewModel mPlacesViewModel;
    private boolean onWishList  = false;

    private ProgressDialog dialog;

    public PlacesFragment(){}

    @Override
    public int getLayoutId() {
        return  R.layout.fragment_places;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        initDaggerAndViewModel();
        dialog= new ProgressDialog(getBaseActivity());
        return super.onCreateView(inflater,container,savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dialog.show();
        if (getArguments()!=null){
           String placeId = PlacesFragmentArgs.fromBundle(getArguments()).getPlaceId();
           mPlacesViewModel.getPlaceById(placeId);
        }

        setListeners();
        setObservers();
    }

    private void setListeners(){
        getBinding().imgViewAddToWishList.setOnClickListener(v -> {
            Place place = mPlacesViewModel.getPlaceLiveData().getValue();
            if (onWishList) {
                mMainViewModel.removeFromWishList(place.getId(),-1,-1);
            } else {
                mMainViewModel.addToWishList(place.getId(),-1,-1);
            }
        });

        getBinding().imgViewBack.setOnClickListener(v -> {
            Navigation.findNavController(getBinding().getRoot()).navigateUp();
        });

    }

    private void setObservers(){
        mPlacesViewModel.getPlaceLiveData().observe(this.getViewLifecycleOwner(), place -> {
            setPlaceDetails(place);
            dialog.dismiss();

        });

        mMainViewModel.getWishListChangedMutableLiveData().observe(this.getViewLifecycleOwner(), wishListChanged -> {
            if (wishListChanged!=null){
                if (wishListChanged.getAction().equals(WishListChanged.Action.ADDED)){
                    onWishList=true;
                    getBinding().imgViewAddToWishList.setImageResource(R.drawable.ic_heart_pink_fill);
                }else {
                    onWishList=false;
                    getBinding().imgViewAddToWishList.setImageResource(R.drawable.ic_heart_black_border);
                }
            }

        });

    }

    private void setPlaceDetails(Place place){
        getBinding().txtViewPlaceName.setText(place.getName());
        String location = place.getAddress().getCity()+", "+place.getAddress().getDistrict().getName();
        getBinding().txtViewPlaceLocation.setText(location);
        if (mMainViewModel.getFavouritePlacesLiveData().getValue()!=null &&
                mMainViewModel.getFavouritePlacesLiveData().getValue().contains(place.getId())){
            onWishList= true;
            getBinding().imgViewAddToWishList.setImageResource(R.drawable.ic_heart_pink_fill);
        }else {
            onWishList= false;
            getBinding().imgViewAddToWishList.setImageResource(R.drawable.ic_heart_black_border);
        }

        Glide.with(getBaseActivity())
                .load(place.getImageUrl())
                .dontAnimate()
                .centerCrop()
                .into(getBinding().imgViewPlace);

        getBinding().textViewInformation.setText(place.getDesc());
        getBinding().textViewOpenTime.setText(String.valueOf(place.getOpeningTime()));
        getBinding().textViewCloseTime.setText(String.valueOf(place.getClosingTime()));
        getBinding().textViewAvgViewTime.setText(String.valueOf(place.getAvgViewTime()));

    }

    private void initDaggerAndViewModel() {
        AndroidSupportInjection.inject(this);
        // ViewModelProvider viewModelProvider = new ViewModelProvider(this, viewModelFactory);
        mPlacesViewModel = ViewModelProviders.of(this, viewModelFactory).get(PlacesViewModel.class);
        mMainViewModel = ViewModelProviders.of(getBaseActivity(), viewModelFactory).get(MainViewModel.class);
    }
}