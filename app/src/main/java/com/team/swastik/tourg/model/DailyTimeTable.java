package com.team.swastik.tourg.model;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = "daily_time" ,
        foreignKeys = {
        @ForeignKey(
                entity = Place.class,
                parentColumns = "id",
                childColumns = "place_id",
                onDelete = CASCADE
        )
})
public class DailyTimeTable {
    @PrimaryKey(autoGenerate = false)
    private String id;
    @ColumnInfo(name = "place_id")
    private String placeId;
    @Embedded
    private AvailableTime usual;
    @Embedded
    private AvailableTime monday;
    @Embedded
    private AvailableTime tuesday;
    @Embedded
    private AvailableTime wednesday;
    @Embedded
    private AvailableTime thursday;
    @Embedded
    private AvailableTime friday;
    @Embedded
    private AvailableTime saturday;
    @Embedded
    private AvailableTime sunday;



}
