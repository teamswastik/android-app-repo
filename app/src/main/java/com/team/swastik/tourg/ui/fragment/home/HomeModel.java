package com.team.swastik.tourg.ui.fragment.home;

import com.team.swastik.tourg.model.Place;

import java.util.List;

public class HomeModel {
    private String title;
    private List<Place> items;
    private LayoutType layoutType;
    private boolean isPlace;

    public HomeModel(String title, List<Place> items, LayoutType layoutType) {
        this.title = title;
        this.items = items;
        this.layoutType = layoutType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Place> getItems() {
        return items;
    }

    public void setItems(List<Place> items) {
        this.items = items;
    }

    public boolean isPlace() {
        return isPlace;
    }

    public void setPlace(boolean place) {
        isPlace = place;
    }

    public LayoutType getLayoutType() {
        return layoutType;
    }

    public void setLayoutType(LayoutType layoutType) {
        this.layoutType = layoutType;
    }

    @Override
    public String toString() {
        return "HomeModel{" +
                "title='" + title + '\'' +
                ", items=" + items +
                ", layoutType=" + layoutType +
                ", isPlace=" + isPlace +
                '}';
    }
}
