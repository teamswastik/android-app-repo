package com.team.swastik.tourg.model.enums;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public enum Province {
    NORTHERN(0,"NORTHERN"),
    WESTERN(1,"WESTERN"),
    EASTERN(2,"EASTERN"),
    SOUTHERN(3,"SOUTHERN"),
    NORTH_WESTERN(4,"NORTH_WESTERN"),
    NORTH_CENTRAL(5,"NORTH_CENTRAL"),
    CENTRAL(6,"CENTRAL"),
    SABARAGAMUWA(7,"SABARAGAMUWA"),
    UVA(8,"UVA");
    private final int id;
    private final String name;
    private static Map provinceMap = new HashMap<>();

    Province(int id, String name) {
        this.id = id;
        this.name = name;
    }

    static {
        for (Province province : Province.values()) {
            provinceMap.put(province.getId(),province);
        }
    }

    public static Province getProvinceById(int id) {
        return (Province) provinceMap.get(id);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @NotNull
    @Override
    public String toString() {
        return name;
    }
}
