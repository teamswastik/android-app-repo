package com.team.swastik.tourg.repository.place;

import androidx.lifecycle.LiveData;

import com.team.swastik.tourg.base.BaseRepository;
import com.team.swastik.tourg.data.database.AppDatabase;
import com.team.swastik.tourg.data.database.dao.PlaceDao;
import com.team.swastik.tourg.data.pref.PreferenceHelper;
import com.team.swastik.tourg.model.Place;
import com.team.swastik.tourg.model.enums.District;
import com.team.swastik.tourg.model.enums.PlaceCategory;
import com.team.swastik.tourg.model.enums.PlaceStatus;
import com.team.swastik.tourg.model.enums.Province;

import org.reactivestreams.Subscriber;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class PlaceTestImpl extends BaseRepository implements PlaceRepository {
    //private Flowable<List<Place>> allPlaces;
    private PlaceDao placeDao;

    @Inject
    public PlaceTestImpl(AppDatabase appDatabase, PreferenceHelper preferenceHelper) {
        super(appDatabase, preferenceHelper);
        this.placeDao = appDatabase.placeDao();

    }

 /*   @Override
    public Flowable<List<Place>> getAllPlaces() {
        List<Place> places = new ArrayList<>();
        places.add(getPlace("pl1","https://images.unsplash.com/photo-1500622944204-b135684e99fd?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=3101&q=80"));
        places.add(getPlace("pl2","https://images.unsplash.com/photo-1520962922320-2038eebab146?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2000&q=80"));
        places.add(getPlace("pl3","https://images.unsplash.com/photo-1441239372925-ac0b51c4c250?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1846&q=80"));
        places.add(getPlace("pl4","https://images.unsplash.com/photo-1500828131278-8de6878641b8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=3034&q=80"));
        places.add(getPlace("pl5","https://images.unsplash.com/photo-1519293978507-9c6bb9f82eda?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=3000&q=80"));

        return Flowable.create(new FlowableOnSubscribe<List<Place>>() {
            @Override
            public void subscribe(FlowableEmitter<List<Place>> emitter) throws Exception {
                emitter.onNext(places);
            }
        }, BackpressureStrategy.BUFFER)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }*/

    private Place getPlace(String id ,String imgUrl) {
        return new Place(
                id,
                "University of kelaniya ",
                "Green university",
                79.8732959,
                6.8936223,
                2,
                80,
                imgUrl
        );
    }

    @Override
    public LiveData<List<Place>> getAllPlaces() {
        return placeDao.getAllPlaces();
    }

    @Override
    public Completable insertPlace(Place place) {
        return null;
    }

    @Override
    public Completable updatePlace(Place place) {
        return null;
    }

    @Override
    public Completable deletePlace(Place place) {
        return null;
    }

    @Override
    public void getPlacesFromFirebase() {

    }

    @Override
    public void getPlacesByRating(int limit) {

    }

    @Override
    public LiveData<List<Place>> getAllTouristPlacesLiveData() {
        return null;
    }

    @Override
    public LiveData<List<Place>> getPlaceByDistrict(District district) {
        return null;
    }
}
