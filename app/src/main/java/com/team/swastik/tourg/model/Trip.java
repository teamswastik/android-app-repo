package com.team.swastik.tourg.model;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.List;
import java.util.Set;

@Entity
public class Trip {
    @PrimaryKey(autoGenerate = true)
    private Long id;
    private String tripTitle;
    private float budget;
    private String createdAt;
    private String updatedAt;
    private String tripStartedAt;
    private String tripEndedAt;
    private boolean tripEnded;
    @Ignore
    private Set<String> visitedPlaces;
    @Ignore
    private List<Place> currentSortedPlaces;

    public Trip() {
    }

    @Ignore
    public Trip(String tripTitle, String createdAt, String updatedAt) {
        this.tripTitle = tripTitle;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTripTitle() {
        return tripTitle;
    }

    public void setTripTitle(String tripTitle) {
        this.tripTitle = tripTitle;
    }

    public float getBudget() {
        return budget;
    }

    public void setBudget(float budget) {
        this.budget = budget;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Set<String> getVisitedPlaces() {
        return visitedPlaces;
    }

    public void setVisitedPlaces(Set<String> visitedPlaces) {
        this.visitedPlaces = visitedPlaces;
    }

    public List<Place> getCurrentSortedPlaces() {
        return currentSortedPlaces;
    }

    public void setCurrentSortedPlaces(List<Place> currentSortedPlaces) {
        this.currentSortedPlaces = currentSortedPlaces;
    }

    public String getTripStartedAt() {
        return tripStartedAt;
    }

    public void setTripStartedAt(String tripStartedAt) {
        this.tripStartedAt = tripStartedAt;
    }

    public String getTripEndedAt() {
        return tripEndedAt;
    }

    public void setTripEndedAt(String tripEndedAt) {
        this.tripEndedAt = tripEndedAt;
    }

    public boolean isTripEnded() {
        return tripEnded;
    }

    public void setTripEnded(boolean tripEnded) {
        this.tripEnded = tripEnded;
    }

    @Override
    public String toString() {
        return "Trip{" +
                "id=" + id +
                ", tripTitle='" + tripTitle + '\'' +
                ", budget=" + budget +
                ", createdAt='" + createdAt + '\'' +
                ", updatedAt='" + updatedAt + '\'' +
                '}';
    }
}

