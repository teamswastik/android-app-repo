package com.team.swastik.tourg.model.enums;

import java.util.HashMap;
import java.util.Map;

public enum PlaceCategory {
    SCHOOL(0),
    TEMPLE(1),
    HOTEL(2),
    RESTAURANT(3),
    PARK(4),
    AIRPORT(5),
    AMUSEMENT_PARK(6),
    AQURIUM(7),
    ATM(8),
    ART_GALLERY(9),
    BANK(10),
    BAKERY(11),
    BAR(12),
    BEAUTY_SALON(13),
    BOOK_SHOP(14),
    BUSSTOP(15),
    CAFE(16),
    HOSPITAL(17),
    SUPERMARKET(18),
    GYM(19),
    LIBRARY(20),
    LODGING(21),
    MUSEUM(22),
    NIGHT_CLUB(23),
    PARKING(24),
    PHARMACY(25),
    POLICE_STATION(26),
    POST_OFFICE(27),
    SHOPPING_MALL(28),
    UNIVERSITY(29),
    TRAVEL_AGENCY(30),
    ZOO(31),
    TOURIST_ATTRACTION(32),
    RAILWAYSTATION(33),
    BEACH(34),
    HISTORICAL(35),
    RELIGIOUS(36),
    STATUE(37),
    WATERFALL(38),
    NATIONAL_PARK(39);


    private final int id;
    private static Map categoryMap = new HashMap<>();

    PlaceCategory(int id) {
        this.id = id;
    }


    static {
        for (PlaceCategory placeCategory : PlaceCategory.values()) {
            categoryMap.put(placeCategory.getId(),placeCategory);
        }
    }

    public static PlaceCategory getCategoryById(int id) {
        return (PlaceCategory) categoryMap.get(id);
    }

    public int getId() {
        return id;
    }
}
