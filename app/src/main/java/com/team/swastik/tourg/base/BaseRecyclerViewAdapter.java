package com.team.swastik.tourg.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public  abstract class BaseRecyclerViewAdapter<T,VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

    private Context mContext;
    private List<T> mDataSet;

    public BaseRecyclerViewAdapter(Context context, List<T> dataSet) {
        this.mContext = context;
        this.mDataSet = dataSet;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return createViewHolder(inflater,parent,viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        T model = mDataSet.get(position);
        bindData(holder,model,position);
    }

    public abstract void bindData(VH holder,T model ,int position);

    public abstract VH createViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType);

    public abstract void onItemClick(T model, int position);


    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public List<T> getDataSet() {
        return mDataSet;
    }

    public Context getmContext() {
        return mContext;
    }
}
