package com.team.swastik.tourg.service;

import android.app.Notification;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.team.swastik.tourg.R;
import com.team.swastik.tourg.model.enums.District;

import java.util.Random;
import timber.log.Timber;

public class TourGFCMService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        Timber.e("Remote message: " + remoteMessage);
        Timber.e(remoteMessage.getMessageId());
        Timber.e(remoteMessage.getFrom());
        Timber.e(remoteMessage.getTo());
        if (remoteMessage.getNotification() != null) {
            Timber.e("Title : " + remoteMessage.getNotification().getTitle());
            Timber.e("Body : " + remoteMessage.getNotification().getBody());
            Timber.e("link : " + remoteMessage.getNotification().getLink());
            Timber.e("channel Id : " + remoteMessage.getNotification().getChannelId());
            Timber.e("Icon : " + remoteMessage.getNotification().getIcon());
            Timber.e("image url : " + remoteMessage.getNotification().getImageUrl());
            Timber.e("Tag : " + remoteMessage.getNotification().getTag());
            sendNotification(remoteMessage.getNotification());
        }

        if (remoteMessage.getData() != null && remoteMessage.getData().size() > 0) {
            Timber.e("Data : " + remoteMessage.getData().toString());
        }

        if (remoteMessage.getRawData() != null && remoteMessage.getRawData().length > 0) {
            Timber.e("Raw Data : " + remoteMessage.getRawData().toString());
        }

    }

    @Override
    public void onNewToken(@NonNull String token) {
        Timber.e("Refreshed token: " + token);
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(token);

    }


    private void sendRegistrationToServer(String token) {
    }

    private void sendNotification(RemoteMessage.Notification notification ) {

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

        if (notification.getImageUrl()== null){
            Notification createdNotification = createNotification(notification.getTitle(), notification.getBody());
            Random random = new Random();
            notificationManager.notify( random.nextInt(),createdNotification);
            return;
        }
        Handler uiHandler = new Handler(Looper.getMainLooper());
        uiHandler.post(() -> {
            Picasso.get().load(notification.getImageUrl()).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    Notification createdNotification = createNotification(notification.getTitle(), notification.getBody(),bitmap);
                    Random random = new Random();
                    notificationManager.notify( random.nextInt(),createdNotification);
                }

                @Override
                public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                    Notification createdNotification = createNotification(notification.getTitle(), notification.getBody());
                    Random random = new Random();
                    notificationManager.notify( random.nextInt(),createdNotification);
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }
            });
        });



    }

    private Notification createNotification(String title, String body) {
        return createNotification(title, body, null);

    }

    private Notification createNotification(String title, String body, Bitmap image) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(getBaseContext(), getString(R.string.channel_id_promo))
                .setSmallIcon(R.drawable.ic_menu_home)
                .setContentTitle(title)
                .setContentText(body);

        if (image != null) {
            builder.setLargeIcon(image)
                    .setStyle(
                            new NotificationCompat.BigPictureStyle()
                                    .bigPicture(image)
                                    .bigLargeIcon(null)
                    );
        }

        return builder.build();
    }
}
