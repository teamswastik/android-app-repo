package com.team.swastik.tourg.ui.activity;

import android.location.Location;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.team.swastik.tourg.base.BaseViewModel;
import com.team.swastik.tourg.data.pref.WishListPreference;
import com.team.swastik.tourg.model.Place;
import com.team.swastik.tourg.ui.fragment.home.WishListChanged;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import timber.log.Timber;

public class MainViewModel extends BaseViewModel {
    private final WishListPreference mWishListPref;
    List<Place> allTouristPlaces = new ArrayList<Place>();
    List<Place> selectedTouristPlaces = new ArrayList<Place>();
    List<Place> shortedPlaces = new ArrayList<Place>();
    int selectedDistrictId;
    String selectedDistrictName;
    double selectedDistrictlng;
    double selectedDistrictlat;
    private String planType = new String();
    private MutableLiveData<Set<String>> mFavouritePlaces = new MutableLiveData<>();
    private MutableLiveData<WishListChanged> wishListChangedMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<Location> mCurrentLocation = new MutableLiveData<>();

    // author Ramakrishnan
    private Set<Place> selectedList;

    @Inject
    public MainViewModel(WishListPreference wishListPreference) {
        this.mWishListPref = wishListPreference;
    }

    public List<Place> getAllTouristPlaces() {
        return allTouristPlaces;
    }

    public void setAllTouristPlaces(List<Place> AllTouristPlaces){
        allTouristPlaces = AllTouristPlaces;
    }

    public List<Place> getSelectedTouristPlaces() {
        return selectedTouristPlaces;
    }

    public void setSelectedTouristPlaces(List<Place> SelectedTouristPlaces){
        selectedTouristPlaces = SelectedTouristPlaces;
    }

    public int getSelectedDistrictId() {
        return selectedDistrictId;
    }

    public void setSelectedDistrictId(int SelectedDistrictId){
        selectedDistrictId = SelectedDistrictId;
    }

    public String getSelectedDistrictName() {
        return selectedDistrictName;
    }

    public void setSelectedDistrictName(String SelectedDistrictName){
        selectedDistrictName = SelectedDistrictName;
    }

    public double getSelectedDistrictlng() {
        return selectedDistrictlng;
    }

    public void setSelectedDistrictlng(double SelectedDistrictlng){
        selectedDistrictlng = SelectedDistrictlng;
    }

    public double getSelectedDistrictlat() {
        return selectedDistrictlat;
    }

    public void setSelectedDistrictlat(double SelectedDistrictlat){
        selectedDistrictlat = SelectedDistrictlat;
    }

    public String getplanType() {
        return planType;
    }

    public void setplanType(String selectedPlanType){
        planType = selectedPlanType;
    }

    public List<Place> getShortedPlaces() {
        return shortedPlaces;
    }

    public void setShortedPlaces(List<Place> ShortedPlaces){
        shortedPlaces = ShortedPlaces;
    }

    public void setPlace(Place place){

    }

    public LiveData<Set<String>> getFavouritePlacesLiveData() {
        return mFavouritePlaces;
    }

    public void addToWishList(String id,int placePosition,int mainAdapterPosition){
        boolean b = mWishListPref.addToWishList(id);
        if (b){
            Set<String> wishList = mWishListPref.getWishList();
            Timber.e("Wishlist Ids : "+ wishList.toString());
            mFavouritePlaces.setValue(wishList);
            wishListChangedMutableLiveData.setValue(new WishListChanged(id, WishListChanged.Action.ADDED,placePosition,mainAdapterPosition));
        }
    }

    public void removeFromWishList(String id,int placePosition,int mainAdapterPosition){
        boolean b = mWishListPref.removeFromWishList(id);
        if (b){
            Set<String> wishList = mWishListPref.getWishList();
            Timber.e("Wishlist Ids : "+ wishList.toString());
            mFavouritePlaces.setValue(wishList);
            wishListChangedMutableLiveData.setValue(new WishListChanged(id, WishListChanged.Action.REMOVED,placePosition,mainAdapterPosition));
        }
    }

    public void getFavouritesPlacesFromPref(){
        mFavouritePlaces.setValue(mWishListPref.getWishList());
    }

    public MutableLiveData<WishListChanged> getWishListChangedMutableLiveData() {
        return wishListChangedMutableLiveData;
    }

    public void setWishListChangedMutableLiveData(WishListChanged wishListChanged) {
        wishListChangedMutableLiveData.setValue(wishListChanged);
    }

    public LiveData<Location> getCurrentLocation() {
        return mCurrentLocation;
    }

    public void setCurrentLocation(Location currentLocation) {
        this.mCurrentLocation.setValue(currentLocation);
    }

    public Set<Place> getSelectedList() {
        return selectedList;
    }

    public void setSelectedList(Set<Place> selectedList) {
        this.selectedList = selectedList;
    }

    public boolean getTripOnProcess(){
      return   mWishListPref.getTripOnProcess();
    }
}
