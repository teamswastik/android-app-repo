package com.team.swastik.tourg.di.module;

import com.team.swastik.tourg.data.pref.AppPreferenceHelper;
import com.team.swastik.tourg.data.pref.PreferenceHelper;
import com.team.swastik.tourg.data.pref.UserPreference;
import com.team.swastik.tourg.data.pref.UserPreferenceImpl;
import com.team.swastik.tourg.data.pref.WishListPrefImpl;
import com.team.swastik.tourg.data.pref.WishListPreference;
import com.team.swastik.tourg.di.TourgAppScope;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class PreferenceModule {

    @Binds
    @TourgAppScope
    abstract PreferenceHelper providePreferenceHelper(AppPreferenceHelper appPreferenceHelper);

    @Binds
    abstract UserPreference bindUserPreference(UserPreferenceImpl userPreference);

    @Binds
    abstract WishListPreference bindWishListPreference(WishListPrefImpl wishListPref);

}
