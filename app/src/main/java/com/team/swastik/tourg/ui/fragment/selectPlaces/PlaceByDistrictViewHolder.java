package com.team.swastik.tourg.ui.fragment.selectPlaces;

import com.bumptech.glide.Glide;
import com.team.swastik.tourg.base.BaseViewHolder;
import com.team.swastik.tourg.databinding.ItemPlaceByDistrictBinding;

public class PlaceByDistrictViewHolder extends BaseViewHolder<ItemPlaceByDistrictBinding> {

    public PlaceByDistrictViewHolder(ItemPlaceByDistrictBinding binding) {
        super(binding);
    }

    public void setPlaceName(String name){
        getBinding().txtViewPlaceName.setText(name);
    }

    public void setImage(String imgUrl){

    }

}
