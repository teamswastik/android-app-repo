package com.team.swastik.tourg.di.module;

import com.team.swastik.tourg.di.TourgAppScope;
import com.team.swastik.tourg.repository.AppRepository;
import com.team.swastik.tourg.repository.MainAppRepository;
import com.team.swastik.tourg.repository.place.PlaceRepository;
import com.team.swastik.tourg.repository.place.PlaceRepositoryImpl;
import com.team.swastik.tourg.repository.place.PlaceTestImpl;
import com.team.swastik.tourg.repository.matrix.MatrixRepository;
import com.team.swastik.tourg.repository.matrix.MatrixRepositoryImpl;
import com.team.swastik.tourg.repository.user.UserRepository;
import com.team.swastik.tourg.repository.user.UserRepositoryImpl;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class RepositoryModule {

    @Binds
    @TourgAppScope
    abstract AppRepository provideAppRepo(MainAppRepository mainAppRepository);

    @Binds
    abstract UserRepository provideUserRepo(UserRepositoryImpl userRepo);

    @Binds
    abstract PlaceRepository providePlaceRepo(PlaceRepositoryImpl placeRepo);

    @Binds
    abstract MatrixRepository provideMatrixRepo(MatrixRepositoryImpl userRepo);


}
