package com.team.swastik.tourg.utils;

import android.content.Context;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

public class DateTimeUtils {
    private final SimpleDateFormat mDefaultFormatter;
    private Context mContext;


    @Inject
    public DateTimeUtils(SimpleDateFormat dateFormatter, Context mContext) {
        this.mDefaultFormatter = dateFormatter;
        this.mContext = mContext;
    }

    public String getDateInPattern(String pattern,Date date){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, Locale.ENGLISH);
       return simpleDateFormat.format(date);
    }

    public Date parseStringForPattern(String pattern,String date){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, Locale.ENGLISH);
        try {
            Toast.makeText(mContext, "Parse Error", Toast.LENGTH_SHORT).show();
            return simpleDateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Date getCurrentDateTime(){
        return new Date();
    }

    public String getCurrentDateTimeAsString(){
        return formatDateTimeToString(getCurrentDateTime());
    }

    public String formatDateTimeToString(Date date){
        return mDefaultFormatter.format(date);
    }

    public Date parseStringToDate(String date){
        try {
           return mDefaultFormatter.parse(date);
        } catch (ParseException e) {
            Toast.makeText(mContext, "Parse Error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
            return null;
        }
    }

    public int getYear(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.YEAR);
    }

    public int getYear(String date){
        Date toDate = parseStringToDate(date);
        return getYear(toDate);
    }

    public int getMonth(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
       return calendar.get(Calendar.MONTH)+1;
    }

    public int getMonth(String date){
        Date toDate = parseStringToDate(date);
       return getMonth(toDate);
    }

    public int getDayOfMonth(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    public int getDayOfMonth(String date){
        Date toDate = parseStringToDate(date);
        return getDayOfMonth(toDate);
    }

    public int getDayOfWeek(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.DAY_OF_WEEK);
    }

    public int getDayOfWeek(String date){
        Date toDate = parseStringToDate(date);
        return getDayOfMonth(toDate);
    }



}
