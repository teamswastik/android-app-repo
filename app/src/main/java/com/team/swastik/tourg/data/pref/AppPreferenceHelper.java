package com.team.swastik.tourg.data.pref;

import java.util.Set;

import javax.inject.Inject;

public class AppPreferenceHelper implements PreferenceHelper {
    private final UserPreference mUserPreference;
    private final WishListPreference mWishListPreference;

    @Inject
    public AppPreferenceHelper(UserPreference userPreference,WishListPreference wishListPreference) {
        this.mUserPreference = userPreference;
        this.mWishListPreference = wishListPreference;
    }


    @Override
    public boolean isUserAvailable() {
        return mUserPreference.isUserAvailable();
    }

    @Override
    public void setUserAvailable(boolean available) {
        mUserPreference.setUserAvailable(available);
    }

    @Override
    public void setWishList(Set<String> wishList) {
        mWishListPreference.setWishList(wishList);
    }

    @Override
    public Set<String> getWishList() {
        return mWishListPreference.getWishList();
    }

    @Override
    public boolean addToWishList(String placeId) {
        return mWishListPreference.addToWishList(placeId);
    }

    @Override
    public boolean removeFromWishList(String placeId) {
        return mWishListPreference.removeFromWishList(placeId);
    }

    @Override
    public void setCurrentTripId(Long tripId) {

    }

    @Override
    public Long getCurrentTripId() {
        return null;
    }

    @Override
    public void setTripOnProcess(boolean onProcess) {

    }

    @Override
    public boolean getTripOnProcess() {
        return false;
    }

    @Override
    public void setLat(double lat) {

    }

    @Override
    public double getLat() {
        return 0;
    }

    @Override
    public void setLng(double lng) {

    }

    @Override
    public double getLng() {
        return 0;
    }
}
