package com.team.swastik.tourg.data.database.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.team.swastik.tourg.model.Place;
import com.team.swastik.tourg.model.enums.District;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

@Dao
public interface PlaceDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable insert(Place place);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable insert(List<Place> place);

    @Update
    Completable update(Place place);

    @Delete
    Completable delete(Place place);

    @Query("SELECT * FROM place")
    LiveData<List<Place>> getAllPlaces();


    @Query("SELECT * FROM place WHERE id= :id")
    Single<Place> getPlaceById(String id);

    @Query("SELECT * FROM place WHERE id IN (:ids)")
    LiveData<List<Place>> getPlaceByIdList(List<String> ids);

    @Query("SELECT * FROM place WHERE district= :byDistrict")
    LiveData<List<Place>> getPlacesByDistrict(District byDistrict);

    @Query("SELECT * FROM place WHERE district= :byDistrict AND touristPlace = :touristPlace")
    LiveData<List<Place>> getPlacesByDistrictAndTouristPlace(District byDistrict,boolean touristPlace);

}
