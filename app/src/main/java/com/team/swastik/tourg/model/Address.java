package com.team.swastik.tourg.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;

import com.team.swastik.tourg.model.enums.District;
import com.team.swastik.tourg.model.enums.Province;

@Entity
public class Address implements Parcelable {
    private int doorNo;
    private String street;
    private String city;
    private District district;
    @Ignore
    private Province province;
    @ColumnInfo(name = "post_code")
    private int postCode;

    @Ignore
    public Address(int doorNo, String street, String city, District district) {
        this.doorNo = doorNo;
        this.street = street;
        this.city = city;
        this.district = district;
    }

    public Address(Parcel in){
        this.doorNo = in.readInt();
        this.street = in.readString();
        this.city=in.readString();
        this.district = (District) in.readSerializable();
        this.province = (Province) in.readSerializable();
        this.postCode = in.readInt();

    }

    public Address() {
    }

    public int getDoorNo() {
        return doorNo;
    }

    public void setDoorNo(int doorNo) {
        this.doorNo = doorNo;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }


    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getPostCode() {
        return postCode;
    }

    public void setPostCode(int postCode) {
        this.postCode = postCode;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }

    @Override
    public String toString() {
        return "Address{" +
                "street='" + street + '\'' +
                ", city='" + city + '\'' +
                ", district=" + district +
                ", province=" + province +
                ", postCode=" + postCode +
                ", doorN0=" + doorNo +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.doorNo);
        dest.writeString(this.street);
        dest.writeString(this.city);
        dest.writeSerializable(this.district);
        dest.writeSerializable(this.province);
        dest.writeInt(this.postCode);

    }

    public static final Parcelable.Creator<Address> CREATOR = new Parcelable.Creator<Address>()
    {
        public Address createFromParcel(Parcel in)
        {
            return new Address(in);
        }
        public Address[] newArray(int size)
        {
            return new Address[size];
        }
    };
}
