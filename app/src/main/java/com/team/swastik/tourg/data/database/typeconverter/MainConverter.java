package com.team.swastik.tourg.data.database.typeconverter;

import androidx.room.TypeConverter;

import com.team.swastik.tourg.model.enums.AccommodationType;
import com.team.swastik.tourg.model.enums.AvailableType;
import com.team.swastik.tourg.model.enums.District;
import com.team.swastik.tourg.model.enums.PlaceCategory;
import com.team.swastik.tourg.model.enums.PlaceStatus;
import com.team.swastik.tourg.model.enums.PlanType;
import com.team.swastik.tourg.model.enums.Province;
import com.team.swastik.tourg.model.enums.TripStatus;

public class MainConverter {

   // private final double latitude;
   // private final double longitude;
    @TypeConverter
    public static PlaceStatus stringToPlaceStatus(String value) {
        return PlaceStatus.valueOf(value);
    }

    @TypeConverter
    public static String placeStatusToString(PlaceStatus value) {
        return value.toString();
    }

    @TypeConverter
    public static District idToDistrict(int id) {
        return District.getDistrictById(id);
    }

    @TypeConverter
    public static int districtToId(District district) {
        return district.getId();
    }

    @TypeConverter
    public static Province idToProvince(int id) {
        return Province.getProvinceById(id);
    }

    @TypeConverter
    public static int provinceToId(Province province) {
        return province.getId();
    }

    @TypeConverter
    public static TripStatus stringToTripStatus(String value) {
        return TripStatus.valueOf(value);
    }

    @TypeConverter
    public static String tripStatusToString(TripStatus value) {
        return value.toString();
    }
    @TypeConverter
    public static AccommodationType stringToAccommodationType(String value) {
        return AccommodationType.valueOf(value);
    }

    @TypeConverter
    public static String accommodationTypeToString(AccommodationType value) {
        return value.toString();
    }
    @TypeConverter
    public static PlanType stringToPlanType(String value) {
        return PlanType.valueOf(value);
    }

    @TypeConverter
    public static String planTypeToString(PlanType value) {
        return value.toString();
    }

    @TypeConverter
    public static int availableTypeToInt(AvailableType availableType){
        return availableType.getValue();
    }

    @TypeConverter
    public static AvailableType intToAvailableType(int value){
        return AvailableType.valueOf(value);
    }


    @TypeConverter
    public static PlaceCategory idToPlaceCategory(int id){
        return PlaceCategory.getCategoryById(id);
    }

    @TypeConverter
    public static int placeCategoryToId(PlaceCategory category){
        return category.getId();
    }




}
