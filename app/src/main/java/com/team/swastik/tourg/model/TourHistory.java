package com.team.swastik.tourg.model;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.team.swastik.tourg.data.database.typeconverter.DateConvertor;

import java.util.Date;

@Entity(foreignKeys = {
 @ForeignKey(entity = Trip.class,parentColumns ="tourId",childColumns = "id")
})
public class TourHistory {
@PrimaryKey
    private String id;
    private boolean tourStatus;
    @TypeConverters(DateConvertor.class)
    private Date startDate;
    @TypeConverters(DateConvertor.class)
    private Date endDate;
    private int tripDays;
    private Trip trip;

    @Ignore
    public TourHistory(String id, boolean tourStatus, Date startDate, Date endDate, int tripDays, Trip trip) {
        this.id = id;
        this.tourStatus = tourStatus;
        this.startDate = startDate;
        this.endDate = endDate;
        this.tripDays = tripDays;
        this.trip = trip;
    }

    public TourHistory() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isTourStatus() {
        return tourStatus;
    }

    public void setTourStatus(boolean tourStatus) {
        this.tourStatus = tourStatus;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getTripDays() {
        return tripDays;
    }

    public void setTripDays(int tripDays) {
        this.tripDays = tripDays;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    @Override
    public String toString() {
        return "TourHistory{" +
                "id='" + id + '\'' +
                ", tourStatus=" + tourStatus +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", tripDays=" + tripDays +
                ", trip=" + trip +
                '}';
    }
}
