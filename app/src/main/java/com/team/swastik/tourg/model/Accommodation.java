package com.team.swastik.tourg.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.team.swastik.tourg.model.enums.District;

@Entity
public class Accommodation {
 @PrimaryKey
 @ColumnInfo(name = "accommodation_id")
 private String id;
 private String name;
 private int ratings;
 private float cost;
 private boolean parking;
 private boolean wifi;

    private Address address;
    private District district;
    private Place place;
    private String imageUrl;

    @Ignore
    public Accommodation(String id, String name, int ratings, float cost, boolean parking, boolean wifi, Address address, District district, Place place, String imageUrl) {
        this.id = id;
        this.name = name;
        this.ratings = ratings;
        this.cost = cost;
        this.parking = parking;
        this.wifi = wifi;
        this.address = address;
        this.district = district;
        this.place = place;
        this.imageUrl = imageUrl;
    }

    public Accommodation() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRatings() {
        return ratings;
    }

    public void setRatings(int ratings) {
        this.ratings = ratings;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public boolean isParking() {
        return parking;
    }

    public void setParking(boolean parking) {
        this.parking = parking;
    }

    public boolean isWifi() {
        return wifi;
    }

    public void setWifi(boolean wifi) {
        this.wifi = wifi;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Accommodation{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", ratings=" + ratings +
                ", cost=" + cost +
                ", parking=" + parking +
                ", wifi=" + wifi +
                ", address=" + address +
                ", district=" + district +
                ", place=" + place +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }
}


