package com.team.swastik.tourg.base;

import androidx.appcompat.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity {

    public void finishActivity(){
        this.finish();
    }


}
