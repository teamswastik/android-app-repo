package com.team.swastik.tourg.utils;

public class AppConstants {
    public static final String OPEN_ROUTE_SERVICE_BASE_URL = "https://api.openrouteservice.org";
    public static final String OPEN_ROUTE_SERVICE_MATRIX_URL = "https://api.openrouteservice.org/v2/matrix/driving-car";
    public static final String JSON_TEST_API_BASE_URL = "https://jsonplaceholder.typicode.com";
    public static final String OPEN_ROUTE_SERVICE_PROFILE_CAR = "driving-car";
    public static final String APP_PREF_NAME = "tour_g_pref";
    public static final String APP_DB_NAME= "tour_g.db";
    public static final String DATE_FORMAT = "yyyy-MM-dd hh:mm:ss a";
    public static final String ORS_API_KEY = "5b3ce3597851110001cf6248aede66e959bf44aabe822d108b12a882";
    public static final String FB_PATH_PLACE = "Place";
    public static final String FB_PATH_TOURIST_PLACE = "tourist_places";
    public static final String SORT_SERVICE_KEY_PLACES = "place_list";
    public static final String SORT_SERVICE_KEY_LAT="lat";
    public static final String SORT_SERVICE_KEY_LNG="lng";
    public static final String INTENT_FILTER_SORT= "SORT";
}
