package com.team.swastik.tourg.model;

import androidx.room.Entity;
import androidx.room.Ignore;

import java.sql.Time;

@Entity
public class AvailableTime {
    //Time 24 Hours
    private int openHr;
    private int closeHr;
    private int openMin;
    private int closeMin;

    @Ignore
    public AvailableTime(int openHr, int closeHr, int openMin, int closeMin) {
        this.openHr = openHr;
        this.closeHr = closeHr;
        this.openMin = openMin;
        this.closeMin = closeMin;
    }

    public AvailableTime() {
    }

    public int getOpenHr() {
        return openHr;
    }

    public void setOpenHr(int openHr) {
        this.openHr = openHr;
    }

    public int getCloseHr() {
        return closeHr;
    }

    public void setCloseHr(int closeHr) {
        this.closeHr = closeHr;
    }

    public int getOpenMin() {
        return openMin;
    }

    public void setOpenMin(int openMin) {
        this.openMin = openMin;
    }

    public int getCloseMin() {
        return closeMin;
    }

    public void setCloseMin(int closeMin) {
        this.closeMin = closeMin;
    }

    @Override
    public String toString() {
        return "OpenClose{" +
                "openHr=" + openHr +
                ", openMin=" + openMin +
                ", closeHr=" + closeHr +
                ", closeMin=" + closeMin +
                '}';
    }
}
