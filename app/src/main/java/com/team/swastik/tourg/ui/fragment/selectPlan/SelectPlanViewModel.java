package com.team.swastik.tourg.ui.fragment.selectPlan;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.team.swastik.tourg.base.BaseViewModel;

import javax.inject.Inject;

public class SelectPlanViewModel extends BaseViewModel {

    private MutableLiveData<String> mText;

    @Inject
    public SelectPlanViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is Select Plan fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}