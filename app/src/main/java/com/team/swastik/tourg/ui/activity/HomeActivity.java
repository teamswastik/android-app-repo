package com.team.swastik.tourg.ui.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.team.swastik.tourg.R;
import com.team.swastik.tourg.base.BaseActivity;
import com.team.swastik.tourg.databinding.ActivityHomeBinding;
import com.team.swastik.tourg.di.ViewModelFactory;
import com.team.swastik.tourg.service.TripForegroundService;
import com.team.swastik.tourg.ui.fragment.home.HomeViewModel;

import java.util.concurrent.atomic.AtomicReference;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import timber.log.Timber;


public class HomeActivity extends BaseActivity {

    @Inject
    ViewModelFactory viewModelFactory;
    private ActivityHomeBinding mBinding;
    private Context mContext;

    private MainViewModel mMainViewModel;

    private FusedLocationProviderClient fusedLocationClient;

    private void initDaggerAndViewModel() {
        AndroidInjection.inject((Activity) mContext);
        mMainViewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        mContext = this;
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(mBinding.bottomNavView, navController);


        navController.addOnDestinationChangedListener((controller, destination, arguments) -> {
            controlBottomNavigation(destination.getId());
            Timber.e("On Destination changed");
        });

        initDaggerAndViewModel();
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(task -> {
            if (!task.isSuccessful()) {
                Timber.e("getInstanceId failed :%s", task.getException());
                return;
            }

            // Get new Instance ID token
            String token = task.getResult().getToken();

            Timber.e("FCM token : " + token);
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        stopForeGroundService();
       checkLocationPermission();
        if (mMainViewModel.getTripOnProcess()){
            Intent intent = new Intent(this, StartTrip.class);
            startActivity(intent);
            finishActivity();
        }
    }
    private void stopForeGroundService(){
        Intent intent = new Intent(this, TripForegroundService.class);
        intent.setAction(TripForegroundService.ACTION_STOP_FOREGROUND_SERVICE);
        startService(intent);
    }

    private void controlBottomNavigation(int destination) {
        switch (destination) {
            case R.id.navigation_home:
                mBinding.bottomNavView.setVisibility(View.VISIBLE);
                break;
            case R.id.placeFragment:
            case R.id.selectPlanFragment:
            case R.id.selectPlacesFragment:
            case R.id.planFragment:
                mBinding.bottomNavView.setVisibility(View.GONE);
                break;
            default:
                mBinding.bottomNavView.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void setListeners() {



    }


    private void checkLocationPermission(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Toast.makeText(mContext, "Please enable location permission", Toast.LENGTH_SHORT).show();
            return;
        }

        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, location -> {
                    // Got last known location. In some rare situations this can be null.
                    if (location != null) {
                        // Logic to handle location object
                        mMainViewModel.setCurrentLocation(location);
                    }
                });

    }

    private void setObservers(){
        mMainViewModel.getCurrentLocation().observe(this, location -> {
            if (location!=null){
                if (mMainViewModel.getTripOnProcess()){
                    Intent intent = new Intent(this, StartTrip.class);
                    startActivity(intent);
                    finishActivity();
                }
            }
        });

    }

    private void setBottomNavListeners(BottomNavigationView bottomNav){
        bottomNav.setOnNavigationItemReselectedListener(item -> {

        });

        bottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return false;
            }
        });
    }





}

