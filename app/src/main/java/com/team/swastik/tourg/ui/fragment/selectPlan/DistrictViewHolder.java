package com.team.swastik.tourg.ui.fragment.selectPlan;

import com.team.swastik.tourg.base.BaseViewHolder;
import com.team.swastik.tourg.databinding.ItemDistrictBinding;

public class DistrictViewHolder extends BaseViewHolder<ItemDistrictBinding> {

    public DistrictViewHolder(ItemDistrictBinding binding) {
        super(binding);
    }

    public void setDistrictName(String name){
        getBinding().txtViewDistrictName.setText(name);
    }
}
