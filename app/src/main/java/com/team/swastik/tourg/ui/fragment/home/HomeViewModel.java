package com.team.swastik.tourg.ui.fragment.home;

import androidx.lifecycle.LiveData;

import com.team.swastik.tourg.base.BaseViewModel;
import com.team.swastik.tourg.model.Place;
import com.team.swastik.tourg.model.enums.District;
import com.team.swastik.tourg.repository.place.PlaceRepository;
import com.team.swastik.tourg.repository.user.UserRepository;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class HomeViewModel extends BaseViewModel {
    private PlaceRepository mPlaceRepository;
   // private MutableLiveData<List<Place>> liveDataPlace = new MutableLiveData<>();
  //  private MutableLiveData<List<HomeModel>> homeModelLiveData = new MutableLiveData<>();

    private UserRepository mUserRepository;

    @Inject
    public HomeViewModel(PlaceRepository placeRepository) {
        this.mPlaceRepository = placeRepository;
    }

    /*public void getAllPlaces() {
        getCompositeDisposable().add(
                mPlaceRepository.getAllPlaces()
                        .subscribe(places -> {
                            if (places != null) {
                                liveDataPlace.setValue(places);
                                getHomeModels(places);
                            }

                        })
        );
    }*/

    public void getHomeModels(List<Place> places) {
        List<HomeModel> homeModels = new ArrayList<>();
        homeModels.add(
                new HomeModel(
                        null,
                        places,
                        LayoutType.LIST_HORIZONTAL
                )
        );

        homeModels.add(
                new HomeModel(
                        "Recommended",
                        getPlacesByDistrict(places),
                        LayoutType.LIST_HORIZONTAL
                )
        );

     //   homeModelLiveData.setValue(homeModels);
    }

    private List<Place> getPlacesByDistrict(List<Place> places){
        List<Place> placesByDistrict = new ArrayList<>();
        for (Place place : places){
            if (place.getAddress().getDistrict().equals(District.HAMBANTOTA)){
                placesByDistrict.add(place);
            }
        }

        return placesByDistrict;
    }

    public LiveData<List<Place>> getTouristPlaceLiveData(){
        return mPlaceRepository.getAllTouristPlacesLiveData();
    }

    public void getPlacesFromFirebase(){
        mPlaceRepository.getPlacesFromFirebase();
    }

   /* public LiveData<List<Place>> getLiveDataPlace() {
        return liveDataPlace;
    }

    public LiveData<List<HomeModel>> getHomeModelLiveData() {
        return homeModelLiveData;
    }*/
}