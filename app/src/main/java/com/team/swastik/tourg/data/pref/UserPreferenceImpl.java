package com.team.swastik.tourg.data.pref;

import android.content.SharedPreferences;

import com.team.swastik.tourg.base.BasePreference;

import javax.inject.Inject;

public class UserPreferenceImpl extends BasePreference implements UserPreference {

    @Inject
    public UserPreferenceImpl(SharedPreferences mSharedPref) {
        super(mSharedPref);
    }

    @Override
    public boolean isUserAvailable() {
        return getSharedPref().getBoolean(PREF_KEY_USER_AVAILABLE,false);
    }

    @Override
    public void setUserAvailable(boolean available) {
        getSharedPref().edit().putBoolean(PREF_KEY_USER_AVAILABLE, available).apply();
    }
}
