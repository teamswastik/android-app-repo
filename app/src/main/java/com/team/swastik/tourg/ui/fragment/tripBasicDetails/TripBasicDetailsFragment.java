package com.team.swastik.tourg.ui.fragment.tripBasicDetails;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.team.swastik.tourg.R;
import com.team.swastik.tourg.base.BaseActivity;
import com.team.swastik.tourg.databinding.FragmentTripBasicDetailsBinding;
import com.team.swastik.tourg.di.ViewModelFactory;
import com.team.swastik.tourg.model.Place;
import com.team.swastik.tourg.service.OutputReceiver;
import com.team.swastik.tourg.ui.activity.MainViewModel;
import com.team.swastik.tourg.ui.fragment.plan.PlanFragment;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

public class TripBasicDetailsFragment extends Fragment {

    @Inject
    ViewModelFactory viewModelFactory;
    private TripBasicDetailsViewModel mTripBasicDetailsViewModel;
    private BaseActivity mActivityContext;
    private MainViewModel mMainViewModel;
    private FragmentTripBasicDetailsBinding mBinding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mActivityContext = (BaseActivity) getActivity();
        initDaggerAndViewModel();
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_trip_basic_details, container, false);
        mBinding.setLifecycleOwner(this);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTripBasicDetailsViewModel = ViewModelProviders.of(this).get(TripBasicDetailsViewModel.class);
        setListeners(view);
        setObservers();
    }

    private void setListeners(View view){
        final NavController navController = Navigation.findNavController(view);
        selectPlaces();
        mBinding.buttonMakePlan.setOnClickListener(v -> {
            List<Place> selectedTouristPlaces = mMainViewModel.getSelectedTouristPlaces();
                if( TextUtils.isEmpty(mBinding.editTextNumberOfDays.getText())){
                    mBinding.editTextNumberOfDays.setError( "Number of days is required!" );
                    Toast.makeText(getActivity(), "Number of days is required!", Toast.LENGTH_LONG).show();
                }else {
                    if(selectedTouristPlaces!=null && (! selectedTouristPlaces.isEmpty())) {
                      //  StartShortService();
                       // navController.navigate(R.id.action_tripBasicDetailsFragment_to_planFragment);
                    }else {
                        Toast.makeText(getActivity(), "Please waite until places get selected!", Toast.LENGTH_LONG).show();
                    }
                }{

            }
                dismissKeyboard(getActivity());
        });

        mBinding.buttonBack.setOnClickListener(v -> {
            Navigation.findNavController(mBinding.getRoot()).navigate(
                    TripBasicDetailsFragmentDirections.actionBackToNavigationHome()
            );

        });
    }

    private void setObservers(){}

    public void dismissKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (null != activity.getCurrentFocus())
            imm.hideSoftInputFromWindow(activity.getCurrentFocus()
                    .getApplicationWindowToken(), 0);
    }

    public void selectPlaces(){
        List<Place> selectedPlaces = mMainViewModel.getAllTouristPlaces();
        for (int i=0; i<selectedPlaces.size(); i++){
            for (int j=i+1; j<selectedPlaces.size(); j++){
                if(selectedPlaces.get(i).getRating()<selectedPlaces.get(j).getRating()){
                    Place temp = selectedPlaces.get(i);
                    selectedPlaces.set(i,selectedPlaces.get(j));
                    selectedPlaces.set(j,temp);
                }
            }
        }
        mMainViewModel.setSelectedTouristPlaces(selectedPlaces);
    }

  /*  public void StartShortService(){
        OutputReceiver receiver = new OutputReceiver(new PlanFragment.OutputList());
        Intent intent = new Intent(getActivity(), com.team.swastik.tourg.service.ShortService.class);
        String strSelectedTripDays = mBinding.editTextNumberOfDays.getText().toString();
        intent.putExtra("allPlaces", selectedPlacesId());
        intent.putExtra("allLocations", selectedPlacesLocation());
        intent.putExtra("allAvgViewTime", selectedPlacesAvgViewTime());
        intent.putExtra("tripDays", strSelectedTripDays);
        intent.putExtra("receiver", receiver);
        intent.putExtra("type", "PlanTripForMe");
        getActivity().startService(intent);
    }*/

    public String[] selectedPlacesId(){
        List<Place> selectedTouristPlaces = mMainViewModel.getSelectedTouristPlaces();
        String[] selectedPlacesId = new String[selectedTouristPlaces.size()+1];
        selectedPlacesId[0] = String.valueOf(mMainViewModel.getSelectedDistrictId());
        for(int i=1; i<selectedPlacesId.length; i++){
            selectedPlacesId[i]=selectedTouristPlaces.get(i-1).getId();
        }
        return selectedPlacesId;
    }

    public double[][] selectedPlacesLocation(){
        List<Place> selectedTouristPlaces = mMainViewModel.getSelectedTouristPlaces();
        double[][] selectedPlacesLocation = new double[selectedTouristPlaces.size()+1][2];
        selectedPlacesLocation[0][0] = mMainViewModel.getSelectedDistrictlng();
        selectedPlacesLocation[0][1] = mMainViewModel.getSelectedDistrictlat();
        for(int i=1; i<selectedPlacesLocation.length; i++){
            selectedPlacesLocation[i][0] = selectedTouristPlaces.get(i-1).getLongitude();
            selectedPlacesLocation[i][1] = selectedTouristPlaces.get(i-1).getLatitude();
        }
        return selectedPlacesLocation;
    }

    public int[] selectedPlacesAvgViewTime(){
        List<Place> selectedTouristPlaces = mMainViewModel.getSelectedTouristPlaces();
        int[] selectedPlacesAvgViewTime = new int[selectedTouristPlaces.size()+1];
        selectedPlacesAvgViewTime[0] = 0;
        for(int i=1; i<selectedPlacesAvgViewTime.length; i++){
            selectedPlacesAvgViewTime[i]=selectedTouristPlaces.get(i-1).getAvgViewTime();
        }
        return selectedPlacesAvgViewTime;
    }

    private void initDaggerAndViewModel() {
        AndroidSupportInjection.inject(this);
        // ViewModelProvider viewModelProvider = new ViewModelProvider(this, viewModelFactory);
        mTripBasicDetailsViewModel = ViewModelProviders.of(this, viewModelFactory).get(TripBasicDetailsViewModel.class);
        mMainViewModel = ViewModelProviders.of(mActivityContext, viewModelFactory).get(MainViewModel.class);
    }
}