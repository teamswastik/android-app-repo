package com.team.swastik.tourg.repository.place;

import androidx.lifecycle.LiveData;

import com.team.swastik.tourg.model.Place;
import com.team.swastik.tourg.model.enums.District;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;

public interface PlaceRepository {
   // Flowable<List<Place>> getAllPlaces();
    LiveData<List<Place>> getAllPlaces();
    Completable insertPlace(Place place);

    Completable updatePlace(Place place);

    Completable deletePlace(Place place);

    void getPlacesFromFirebase();

    void getPlacesByRating(int limit);

    LiveData<List<Place>> getAllTouristPlacesLiveData();

    LiveData<List<Place>> getPlaceByDistrict(District district);

}
