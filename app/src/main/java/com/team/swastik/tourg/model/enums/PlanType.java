package com.team.swastik.tourg.model.enums;

public enum PlanType {
    MANUAL_PLAN,
    AUTO_PLAN
}
