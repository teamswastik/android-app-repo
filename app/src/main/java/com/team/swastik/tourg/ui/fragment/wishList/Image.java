package com.team.swastik.tourg.ui.fragment.wishList;

public class Image {
    private int ImageId;
    private String ImageName;

    public Image(int imageId, String imageName) {
        ImageId = imageId;
        ImageName = imageName;
    }

    public int getImageId() {
        return ImageId;
    }

    public String getImageName() {
        return ImageName;
    }

    public void setImageId(int imageId) {
        ImageId = imageId;
    }

    public void setImageName(String imageName) {
        ImageName = imageName;
    }
}
