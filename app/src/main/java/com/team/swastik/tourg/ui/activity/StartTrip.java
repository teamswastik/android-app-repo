package com.team.swastik.tourg.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.team.swastik.tourg.R;
import com.team.swastik.tourg.base.BaseActivity;
import com.team.swastik.tourg.data.database.entity.TripWithPlaces;
import com.team.swastik.tourg.databinding.ActivityStartTripBinding;
import com.team.swastik.tourg.di.ViewModelFactory;
import com.team.swastik.tourg.model.Place;
import com.team.swastik.tourg.model.Trip;
import com.team.swastik.tourg.model.enums.District;
import com.team.swastik.tourg.service.SortTripService;
import com.team.swastik.tourg.service.TripForegroundService;
import com.team.swastik.tourg.utils.AppConstants;
import com.team.swastik.tourg.utils.DateTimeUtils;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import dagger.android.AndroidInjection;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class StartTrip extends BaseActivity {

    @Inject
    ViewModelFactory viewModelFactory;
    @Inject
    DateTimeUtils dateTimeUtils;
    private ActivityStartTripBinding mBinding;
    private Context mContext;
    private StartTripViewModel mStartTripViewModel;
    private ProgressDialog dialog;

    private BroadcastReceiver bReceiver = new BroadcastReceiver(){

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(SortTripService.SORT_TRIP_RESULT)){
                if (intent.getExtras()!=null){
                    Bundle bundle = intent.getExtras();
                    if (bundle.getBoolean(SortTripService.KEY_SORT_SUCCESS)){
                        List<Place> parcelableArrayList = bundle.getParcelableArrayList(SortTripService.KEY_SORT_PLACE_LIST);
                        mStartTripViewModel.setCurrentSortedList(parcelableArrayList);
                        dialog.dismiss();
                        Timber.e("OnReceivedbrodcast : "+parcelableArrayList.size());
                        //    mViewModel.setSortedList(parcelableArrayList);
                    }else {
                        Toast.makeText(context, "Sorting Failed", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_start_trip);
        dialog= new ProgressDialog(this);
        mContext = this;
        initDaggerAndViewModel();
        dialog.setMessage("Loading.!");
        dialog.show();
        setListeners(mBinding);
        setObservers(mBinding, mContext);
        mStartTripViewModel.getTripWithId(mStartTripViewModel.getCurrentTripId());
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(bReceiver, new IntentFilter(SortTripService.SORT_TRIP_RESULT));
        stopForeGroundService();
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(bReceiver);
        startForeGroundService();
    }

    @SuppressLint("CheckResult")
    private void setListeners(ActivityStartTripBinding mBinding) {
        mBinding.buttonEndTrip.setOnClickListener(view -> {
            Trip trip = mStartTripViewModel.getCurrentTripWithPlaces().getValue().trip;
            trip.setTripEnded(true);
            trip.setTripEndedAt(dateTimeUtils.getCurrentDateTimeAsString());
            mStartTripViewModel.updateTrip(trip)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            () ->{
                                Timber.e("Trip Updated");
                                mStartTripViewModel.endTrip();
                                Intent myIntent = new Intent(StartTrip.this, HomeActivity.class);
                                StartTrip.this.startActivity(myIntent);
                            },
                            throwable -> {
                                Timber.e("Error in Getting Trip");
                                throwable.printStackTrace();
                            }
                    );

        });
    }

    private void setObservers(ActivityStartTripBinding mBinding, Context mContext) {
        mStartTripViewModel.getNoNextPlace().observe(this, aBoolean -> {
            if (aBoolean!=null && aBoolean){
                mBinding.txtViewNextPlaceName.setText("No Place");
                Glide.with(mContext)
                        .load(R.drawable.ic_launcher_background)
                        .dontAnimate()
                        .centerCrop()
                        .into(mBinding.imgViewNextPlace);
            }
        });
        mStartTripViewModel.getCurrentTripWithPlaces().observe(this, tripWithPlaces -> {
            if (tripWithPlaces!=null){
                startIntentService(tripWithPlaces.places,mStartTripViewModel.getLat(),mStartTripViewModel.getLng());
            }

        });

        mStartTripViewModel.getCurrentPlaceLiveData().observe(this, place -> {
            if (place!=null){
                mBinding.txtViewCurrentPlaceName.setText(place.getName());
                Glide.with(mContext)
                        .load(place.getImageUrl())
                        .dontAnimate()
                        .centerCrop()
                        .into(mBinding.imgViewCurrentPlace);
            }

        });

        mStartTripViewModel.getNextPlaceLiveData().observe(this, place -> {
            if (place!=null){
                mBinding.txtViewNextPlaceName.setText(place.getName());
                Glide.with(mContext)
                        .load(place.getImageUrl())
                        .dontAnimate()
                        .centerCrop()
                        .into(mBinding.imgViewNextPlace);
            }
        });

        mStartTripViewModel.getAccommodationLiveData(District.COLOMBO,false).observe(this, new Observer<List<Place>>() {
            @Override
            public void onChanged(List<Place> places) {
                if (places!=null){
                    initRecyclerView(places, mContext);
                }
            }
        });

    }

    private void startIntentService(List<Place> places , double lat, double lng){
        Intent intent = new Intent(this, SortTripService.class);
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(
                AppConstants.SORT_SERVICE_KEY_PLACES,
                new ArrayList<>(places)
        );
        bundle.putDouble(AppConstants.SORT_SERVICE_KEY_LAT,lat);
        bundle.putDouble(AppConstants.SORT_SERVICE_KEY_LNG,lng);
        intent.putExtras(bundle);
        startService(intent);
    }

    private void startForeGroundService(){
        Intent intent = new Intent(this, TripForegroundService.class);
        intent.setAction(TripForegroundService.ACTION_START_FOREGROUND_SERVICE);
        startService(intent);
    }

    private void stopForeGroundService(){
        Intent intent = new Intent(this, TripForegroundService.class);
        intent.setAction(TripForegroundService.ACTION_STOP_FOREGROUND_SERVICE);
       startService(intent);
    }

    private void initRecyclerView(List<Place> places, Context mContext) {
        RecyclerView recyclerView = mBinding.recyclerViewAccommodations;
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false));
        AccommodationAdapter accommodationAdapter = new AccommodationAdapter(places,this);
        recyclerView.setAdapter(accommodationAdapter);
        accommodationAdapter.notifyDataSetChanged();
    }

    private void initDaggerAndViewModel() {
        AndroidInjection.inject((Activity) mContext);
        mStartTripViewModel = ViewModelProviders.of(this, viewModelFactory).get(StartTripViewModel.class);
    }

}
