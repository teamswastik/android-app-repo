package com.team.swastik.tourg.ui.fragment.myTrip;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.team.swastik.tourg.R;
import com.team.swastik.tourg.base.BaseRecyclerViewAdapter;
import com.team.swastik.tourg.data.database.entity.TripWithPlaces;
import com.team.swastik.tourg.databinding.ItemPlaceByDistrictBinding;
import com.team.swastik.tourg.databinding.ItemTripBinding;
import com.team.swastik.tourg.model.Place;
import com.team.swastik.tourg.ui.fragment.selectPlaces.PlaceByDistrictViewHolder;

import java.util.List;

public class TripWithPlacesAdapter extends BaseRecyclerViewAdapter<TripWithPlaces,TripViewHolder> {

    private ITripListener listener;

    public interface ITripListener{
        void onTripStarted(TripWithPlaces tripWithPlaces,int position);
    }

    public TripWithPlacesAdapter(Context context, List<TripWithPlaces> dataSet) {
        super(context, dataSet);
    }

    @Override
    public void bindData(TripViewHolder holder, TripWithPlaces model, int position) {

       /* holder.setTripId(model.trip.getId().toString());
        if (model.places!=null && model.places.size()>0)
            holder.setTripPlaces(loopPlaces(model.places));*/

        holder.getBinding().btnStartTrip.setOnClickListener(v -> {
            listener.onTripStarted(model,position);
        });

        if (model.trip.isTripEnded()){
            holder.getBinding().btnStartTrip.setVisibility(View.GONE);
        }else {
            holder.getBinding().btnStartTrip.setVisibility(View.VISIBLE);
        }

        Glide.with(getmContext())
                .load(model.places.get(0).getImageUrl())
                .dontAnimate()
                .centerCrop()
                .into(holder.getBinding().imgViewPlace);

    }

    private String loopPlaces(List<Place> places){
        StringBuilder sb = new StringBuilder();
        for (Place place:places){
            sb.append("||");
            sb.append(place.getName());
        }

       return sb.toString();
    }

    @Override
    public TripViewHolder createViewHolder(LayoutInflater inflater, ViewGroup parent, int viewType) {
        ItemTripBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_trip, parent, false);
        return new TripViewHolder(binding);
    }

    @Override
    public void onItemClick(TripWithPlaces model, int position) {

    }

    public void setListener(ITripListener listener) {
        this.listener = listener;
    }
}
