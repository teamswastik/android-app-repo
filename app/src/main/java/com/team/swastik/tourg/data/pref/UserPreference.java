package com.team.swastik.tourg.data.pref;

public interface UserPreference {
    String PREF_KEY_USER_AVAILABLE = "PREF_KEY_USER_AVAILABLE";
    boolean isUserAvailable();
    void setUserAvailable(boolean available);

}
