package com.team.swastik.tourg.model.enums;

public enum AccommodationType {
    GUEST_HOUSE,
    COTTAGE,
    SHELTER,
    HOMESTAY,
    CAMPSITE,
    MOBILE_HOME,
    HOTEL,
    APARTMENT_HOTEL,
    YOUTH_HOSTEL,
    HOSTEL
}
