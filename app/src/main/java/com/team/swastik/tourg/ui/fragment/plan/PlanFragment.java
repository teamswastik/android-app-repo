package com.team.swastik.tourg.ui.fragment.plan;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.team.swastik.tourg.R;
import com.team.swastik.tourg.base.BaseFragment;
import com.team.swastik.tourg.databinding.FragmentPlanBinding;
import com.team.swastik.tourg.di.ViewModelFactory;
import com.team.swastik.tourg.model.Place;
import com.team.swastik.tourg.model.Trip;
import com.team.swastik.tourg.service.SortTripService;
import com.team.swastik.tourg.ui.activity.MainViewModel;
import com.team.swastik.tourg.ui.fragment.selectPlaces.PlaceByDistrictAdapter;
import com.team.swastik.tourg.utils.AppConstants;
import com.team.swastik.tourg.utils.DateTimeUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class PlanFragment extends BaseFragment<FragmentPlanBinding> {

    @Inject
    ViewModelFactory viewModelFactory;
    @Inject
    DateTimeUtils dateTimeUtils;
    private static PlanViewModel mViewModel;
    private static MainViewModel mMainViewModel;

    private ProgressDialog dialog;
    private BroadcastReceiver bReceiver = new BroadcastReceiver(){

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(SortTripService.SORT_TRIP_RESULT)){
                if (mViewModel!=null && intent.getExtras()!=null){
                    Bundle bundle = intent.getExtras();
                    if (bundle.getBoolean(SortTripService.KEY_SORT_SUCCESS)){
                        List<Place> parcelableArrayList = bundle.getParcelableArrayList(SortTripService.KEY_SORT_PLACE_LIST);
                        Timber.e("OnReceivedbrodcast : "+parcelableArrayList.size());
                        mViewModel.setSortedList(parcelableArrayList);
                    }else {
                        Toast.makeText(context, "Sorting Failed", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        }

    };


    @Override
    public int getLayoutId() {
        return R.layout.fragment_plan;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        initDaggerAndViewModel();
        dialog= new ProgressDialog(getBaseActivity());
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dialog.setMessage("Planning your trip.");
        dialog.show();
        if (mMainViewModel.getSelectedList()!=null){
            List<Place> placeList = new ArrayList<Place>(mMainViewModel.getSelectedList());
            Timber.e("Total Selected : " +placeList.size());
            populateWithWishList(
                    mMainViewModel.getFavouritePlacesLiveData().getValue(),
                    placeList
            );
            Location location = mMainViewModel.getCurrentLocation().getValue();
            if (location!=null) {
                startIntentService(placeList,location.getLatitude(),location.getLongitude());
            } else {
                dialog.dismiss();
                Toast.makeText(getBaseActivity(), "Enable Location", Toast.LENGTH_SHORT).show();
                Navigation.findNavController(getBinding().getRoot()).navigateUp();
            }
        }else {
            dialog.dismiss();
            Toast.makeText(getBaseActivity(), "Select the places for the trip", Toast.LENGTH_SHORT).show();
            Navigation.findNavController(getBinding().getRoot()).navigateUp();
        }
        setListeners();
        setObservers();
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getBaseActivity()).registerReceiver(bReceiver, new IntentFilter(SortTripService.SORT_TRIP_RESULT));
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getBaseActivity()).unregisterReceiver(bReceiver);
    }

    private void startIntentService(List<Place> places , double lat, double lng){
        Intent intent = new Intent(getBaseActivity(), SortTripService.class);
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(
                AppConstants.SORT_SERVICE_KEY_PLACES,
                new ArrayList<>(places)
        );
        bundle.putDouble(AppConstants.SORT_SERVICE_KEY_LAT,lat);
        bundle.putDouble(AppConstants.SORT_SERVICE_KEY_LNG,lng);
        intent.putExtras(bundle);
        getBaseActivity().startService(intent);
    }
    public void setListeners(){
        getBinding().imgViewBack.setOnClickListener(v -> {
            Navigation.findNavController(getBinding().getRoot()).navigateUp();
        });

        getBinding().btnCreateTrip.setOnClickListener(v -> {
            if (mMainViewModel.getSelectedList()!=null){
                Trip trip = new Trip("TestTrip", dateTimeUtils.getCurrentDateTimeAsString(), dateTimeUtils.getCurrentDateTimeAsString());
                List<Place> selectedList = new ArrayList<Place>(mMainViewModel.getSelectedList());
                mViewModel.createTrip(trip,selectedList);
            }else {
                Toast.makeText(getBaseActivity(), "No selected List", Toast.LENGTH_SHORT).show();
            }
        });


    }

    public void setObservers(){
        //getShortedList();

        mViewModel.getSortedListLiveData().observe(this.getViewLifecycleOwner(), places -> {
            if (places!=null) {
                Timber.e("onLivedata Observed : "+ places.size());
                dialog.dismiss();
                initRecyclerView(places);
            }
        });

        mViewModel.getCreationSuccessful().observe(this.getViewLifecycleOwner(), aBoolean -> {
            if (aBoolean!=null && aBoolean){
                Navigation.findNavController(getBinding().getRoot()).popBackStack(R.id.navigation_home,false);
            }
        });

    }

    private void populateWithWishList(Set<String> wishList , List<Place> places){
        for (int i = 0; i < places.size(); i++) {
            if (wishList.contains(places.get(i).getId())){
                places.get(i).setOnWishList(true);
            }
        }

    }

    public void getShortedList(){
        String message = "Please wait .....";
        List<Place> messagePlaces = new ArrayList<Place>();
        messagePlaces.add(new Place("message", message, null, 0, 0,0, 0));
        if(messagePlaces!=null){
            initRecyclerView(messagePlaces);
        }
    }

    private void initRecyclerView(List<Place> places) {
        RecyclerView recyclerView = getBinding().recyclerViewPlaces;
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity(),LinearLayoutManager.VERTICAL,false));
        ShortedPlacesAdapter adapter = new ShortedPlacesAdapter(getBaseActivity(),places);
        recyclerView.setAdapter(adapter);
    }

   /* public static class OutputList {
        public void displayOutputList(int resultCode, Bundle resultData){
            String[] output = (String[]) resultData.getStringArray("ShortedList");
            List<Place> selectedPlaces = mMainViewModel.getSelectedTouristPlaces();
            List<Place> shortedPlaces = new ArrayList<Place>();
            for(int i=1; i<output.length; i++){
                String placeId = output[i];
                for(int j=0; j<selectedPlaces.size(); j++){
                    if(selectedPlaces.get(j).getId().equals(placeId)){
                        shortedPlaces.add(selectedPlaces.get(j));
                    }
                }
            }
            mMainViewModel.setShortedPlaces(shortedPlaces);
            List<Place> ShortedPlaces = mMainViewModel.getShortedPlaces();
            if (ShortedPlaces!=null){
                initRecyclerView(ShortedPlaces);
            }
            if(ShortedPlaces.size()==0){
                String message = output[0];
                List<Place> messagePlaces = new ArrayList<Place>();
                messagePlaces.add(new Place("message", message, null, 0, 0,0, 0));
                if(messagePlaces!=null){
                    initRecyclerView(messagePlaces);
                }
            }
        }
        private void initRecyclerView(List<Place> places) {
            RecyclerView recyclerView = mBinding.recyclerViewShortedPlaces;
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(mActivityContext,LinearLayoutManager.VERTICAL,false));
            ShortedPlacesAdapter shortedPlacesAdapter = new ShortedPlacesAdapter(places);
            recyclerView.setAdapter(shortedPlacesAdapter);
            shortedPlacesAdapter.notifyDataSetChanged();
        }
    }*/

    private void initDaggerAndViewModel() {
        AndroidSupportInjection.inject(this);
        // ViewModelProvider viewModelProvider = new ViewModelProvider(this, viewModelFactory);
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(PlanViewModel.class);
        mMainViewModel = ViewModelProviders.of(getBaseActivity(), viewModelFactory).get(MainViewModel.class);
    }
}